import {Dimensions} from 'react-native';
import React from 'react';

export const screenWidth = Dimensions.get('screen').width;
export const screenHeight = Dimensions.get('screen').height;
