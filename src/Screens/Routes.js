import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import {
  SplashScreen,
  OnboardAuth,
  Login,
  Register,
  Home,
  Produk,
  Toko,
  Favorit,
  Profil,
  DtlProduk,
  DtlToko,
  Notification,
  DtlArtikel,
  ListProduct,
  ListEventSharing,
  Feed,
  SearchProduct,
  ForgotPassword,
  EditProfile
} from './MainScreens';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Colors from '../Styles/Colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Badge } from '@rneui/themed';
import { screenWidth } from '../Variable';
import Font from '../Styles/Fonts';
import { color } from '@rneui/base';

const Stack = createNativeStackNavigator();
const Tab = createMaterialBottomTabNavigator();

function HomeSS() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={({ navigation }) => ({ headerShown: false })}
      />
    </Stack.Navigator>
  );
}

function ProdukSS() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Produk"
        component={Produk}
        options={({ navigation }) => ({ headerShown: false })}
      />
    </Stack.Navigator>
  );
}

function TokoSS() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Toko"
        component={Toko}
        options={({ navigation }) => ({ headerShown: false })}
      />
    </Stack.Navigator>
  );
}

function FeedSS() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Feed"
        component={Feed}
        initialParams={{ param: 'haloo' }}
        options={({ }) => ({ headerShown: false })}
      />
    </Stack.Navigator>
  );
}

function ProfilSS() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Profil"
        component={Profil}
        options={{ headerShown: false }}
      // options={{
      //   headerTitle: () => (
      //     <Text style={[Font.bold16, {color: Colors.white}]}>Profile</Text>
      //   ),
      //   headerTransparent: true,
      // }}
      />
    </Stack.Navigator>
  );
}

const titleBottom = title => (
  <Text style={{ fontSize: 11, fontWeight: '400' }}>{title}</Text>
);

const MainTabNavigation = (route) => {
  // console.log('apa ini', route.route.params)
  return (
    <Tab.Navigator
      // initialRouteName={'Akun'}
      initialRouteName={'Feed'}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconImage;

          if (route.name === 'Home') {
            iconImage = focused
              ? require('../Assets/icons/Home_fill.png')
              : require('../Assets/icons/Home.png');
          } else if (route.name === 'Produk') {
            iconImage = focused
              ? require('../Assets/icons/Plant_fill.png')
              : require('../Assets/icons/Plant.png');
          } else if (route.name === 'Feed') {
            iconImage = focused
              ? require('../Assets/icons/feed_fill.png')
              : require('../Assets/icons/feed.png');
          } else if (route.name === 'Toko') {
            iconImage = focused
              ? require('../Assets/icons/Shop_fill.png')
              : require('../Assets/icons/Shop.png');
          } else if (route.name === 'Profil') {
            iconImage = focused
              ? require('../Assets/icons/profil_fill.png')
              : require('../Assets/icons/profil.png');
          }
          return (
            <Image
              source={iconImage}
              style={{ width: 24, height: 24 }}
              resizeMode={'contain'}
            />
          );
        },
      })}
      activeColor={Colors.primary}
      inactiveColor={Colors.placeholder}
      barStyle={{
        backgroundColor: Colors.white,
      }}
      shifting={false}>
      <Tab.Screen
        name="Home"
        component={Home}
        initialParams={{ navv: route.route.params }}
        options={{ headerShown: false }}
      />
      <Tab.Screen
        name="Produk"
        component={Produk}
        initialParams={{ navv: route.route.params }}
        options={{ headerShown: false }}
      />
      <Tab.Screen
        name="Feed"
        component={Feed}
        initialParams={{ navv: route.route.params }}
        options={{ headerShown: false }}
      />
      <Tab.Screen
        name="Toko"
        component={Toko}
        initialParams={{ navv: route.route.params }}
        options={{ headerShown: false }}
      />
      <Tab.Screen
        name="Profil"
        component={Profil}
        initialParams={{ navv: route.route.params }}
        options={{ headerShown: false }}
      />
    </Tab.Navigator>
  );
};

const MainNavigation = (props) => {
  console.log(props, 'props');
  return (
    <Stack.Navigator 
    // initialRouteName={props.token == null ? "OnboardAuth" : "Home"}
    initialRouteName={'Home'}>
      <Stack.Screen
        name="OnboardAuth"
        component={OnboardAuth}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Home"
        component={MainTabNavigation}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DtlProduk"
        component={DtlProduk}
        options={({ navigation }) => ({ headerShown: false })}
      // options={{headerTintColor: Colors}}
      />
      <Stack.Screen
        name="DtlToko"
        component={DtlToko}
        options={({ navigation }) => ({ headerShown: false })}
      />
      <Stack.Screen
        name="Notification"
        component={Notification}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="DtlArtikel"
        component={DtlArtikel}
        options={({ navigation }) => ({ headerShown: false })}
      />
      <Stack.Screen
        name="ListProduct"
        component={ListProduct}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ListEventSharing"
        component={ListEventSharing}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SearchProduct"
        component={SearchProduct}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const config = {
  screens: {
    DtlProduk: {
      path: 'DtlProduk/:id',
      parse: {
        id: (id) => (id)
      },
    },
    DtlArtikel: {
      path: 'DtlArtikel/:id/:title',
      parse: {
        id: (id) => (id),
        title: (title) => (title)
      },
    },
  },
}

const linking = {
  prefixes: ['https://www.wanflora.com', 'app://wanflora'],
  // prefixes : ['app://app_bumdeskita'],
  config,
};

const Routes = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [token, setToken] = useState(null);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);

    // get data logn
    async function getDataLogin() {
      const token = await AsyncStorage.getItem('user_token')
      setToken(token)
    }
    getDataLogin()
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }
  return (
    <NavigationContainer linking={linking}>
      <MainNavigation token={token} />
    </NavigationContainer>
  );
};

export default Routes;
