import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    View,
    StatusBar,
    TouchableOpacity,
    Image,
    FlatList,
    RefreshControl,
    ScrollView,
    Animated,
    ActivityIndicator
} from 'react-native';
import React, { useState, useEffect } from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import Money from '../../Components/Money';
import MasonryList from '@react-native-seoul/masonry-list';
import { Api, Key } from '../../../Screens/Api';
import { screenWidth, screenHeight } from '../../../Variable';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceholder from 'react-native-shimmer-placeholder';
import HeaderTitleBack from '../../Components/HeaderTitleBack';
import HTMLView from 'react-native-htmlview';

const ListEventSharing = ({ navigation, route }) => {
    const [shimmer, setShimmer] = useState(false);
    const [tokens, setTokens] = useState('');
    const [refreshing, setRefreshing] = useState(false);
    const [dataEvent, setDataEvent] = useState([]);
    const [dataSharing, setDataSharing] = useState([]);
    const [tab, setTab] = useState(route?.params?.title == 'Event' ? 'Event' : (route?.params?.title == 'Sharing' ? 'Sharing' : 'Event'));
    // scrolled event
    const [nextLinkEvent, setNextLinkEvent] = useState('');
    const [hasScrolledEvent, sethasScrolledEvent] = useState(false);
    const [loadMoreEvent, setLoadMoreEvent] = useState(false);
    const [passEvent, setPassEvent] = useState(false);
    const [scrollYEvent, setScrollYEvent] = useState(new Animated.Value(0));
    // scrolled
    const [nextLink, setNextLink] = useState('');
    const [hasScrolled, sethasScrolled] = useState(false);
    const [loadMore, setLoadMore] = useState(false);
    const [pass, setPass] = useState(false);
    const [scrollY, setScrollY] = useState(new Animated.Value(0));

    useEffect(() => {
        // get data login
        async function getDataLogin() {
            try {
                const token = await AsyncStorage.getItem('user_token')
                console.log('tokne', token);
                setTokens(token)
                getDataEvent(token)
                getDataSharing(token)
            } catch (e) {
                console.log(e)
            }
        }
        getDataLogin();
    }, []);

    const onRefresh = () => {
        setRefreshing(true);
        setTimeout(() => {
            getDataEvent(tokens)
            getDataSharing(tokens)
            setRefreshing(false);
        }, 2000);
    };

    const getDataEvent = (token) => {
        setShimmer(true)
        // axios get
        axios.get(`${Api}/event`, {
            headers: {
                'Accept': 'application/json',
                'key': Key,
                'Authorization': 'Bearer ' + token,
            }
        }).then((res) => {
            console.log('result event', res.data);
            if (res.data.next_page_url === null) {
                (setPassEvent(true), sethasScrolledEvent(false))
            } else {
                sethasScrolledEvent(true)
                setNextLinkEvent(res.data.next_page_url);
            }
            setDataEvent(res.data.data)
            setShimmer(false);
        }).catch((err) => {
            setShimmer(false);
            console.log('err event', err.response)
        })
    }

    const getDataSharing = (token) => {
        setShimmer(true)
        // axios get
        axios.get(`${Api}/sharing`, {
            headers: {
                'Accept': 'application/json',
                'key': Key,
                'Authorization': 'Bearer ' + token,
            }
        }).then((res) => {
            console.log('result sharing', res.data);
            if (res.data.next_page_url === null) {
                (setPass(true), sethasScrolled(false))
            } else {
                sethasScrolled(true)
                setNextLink(res.data.next_page_url);
            }
            setDataSharing(res.data.data)
            setShimmer(false);
        }).catch((err) => {
            setShimmer(false);
            console.log('err sharing', err.response)
        })
    }

    const isCloseToBottomEvent = (data) => {
        // console.log(data, 'isssssss');
        const paddingToBottom = 20
        return data?.layoutMeasurement.height + data?.contentOffset.y >=
            data?.contentSize.height - paddingToBottom
    }

    const renderFooterEvent = () => {
        return (
            hasScrolled === true ?
                <ActivityIndicator size={'large'} animating /> : null
        )
    }

    const handleLoadMoreEvent = () => {
        if (loadMoreEvent) {
            axios.get(`${nextLinkEvent}`, {
                headers: {
                    'Accept': 'application/json',
                    'key': Key,
                    'Authorization': 'Bearer ' + tokens,
                }
            }).then(res => {
                console.log('res2', res.data.data);
                sethasScrolledEvent(true);
                setDataEvent([...dataEvent, ...res.data.data]);
                setNextLinkEvent(res.data.next_page_url);

                res.data.next_page_url === null ?
                    (sethasScrolledEvent(false), setPassEvent(true)) :
                    false;
            }).catch(err => {
                console.log(err);
            });
            return
        }
        setLoadMoreEvent(true)
    }

    const isCloseToBottom = (data) => {
        // console.log(data, 'isssssss');
        const paddingToBottom = 20
        return data?.layoutMeasurement.height + data?.contentOffset.y >=
            data?.contentSize.height - paddingToBottom
    }

    const renderFooter = () => {
        return (
            hasScrolled === true ?
                <ActivityIndicator size={'large'} animating /> : null
        )
    }

    const handleLoadMore = () => {
        if (loadMore) {
            axios.get(`${nextLink}`, {
                headers: {
                    'Accept': 'application/json',
                    'key': Key,
                    'Authorization': 'Bearer ' + tokens,
                }
            }).then(res => {
                console.log('res2', res.data.data);
                sethasScrolled(true);
                setDataSharing([...dataSharing, ...res.data.data]);
                setNextLink(res.data.next_page_url);

                res.data.next_page_url === null ?
                    (sethasScrolled(false), setPass(true)) :
                    false;
            }).catch(err => {
                console.log(err);
            });
            return
        }
        setLoadMore(true)
    }

    const renderShimmer = ({ item }) => {
        return (
            <ShimmerPlaceholder
                LinearGradient={LinearGradient}
                style={{
                    flex: 1,
                    width: screenWidth * 0.93,
                    height: 230,
                    marginHorizontal: 4,
                    borderRadius: 15,
                    marginVertical: 8,
                }}
            />
        );
    };

    const renderEvent = ({ item }) => {
        return (
            <TouchableOpacity
                style={{
                    flex: 1,
                    width: screenWidth * 0.93,
                    // height: 278,
                    marginHorizontal: 4,
                    borderRadius: 15,
                    marginVertical: 8,
                    elevation: 2,
                    backgroundColor: Colors.white,
                }}
                onPress={() => navigation.navigate('DtlArtikel', { title: 'Event', id: item.id, tab: 'header' })}>
                <Image
                    source={{ uri: item.file }}
                    style={{
                        width: screenWidth * 0.93,
                        height: 162,
                        borderRadius: 15,
                    }}
                    resizeMode={'contain'}
                />
                <View style={{ paddingVertical: 8, paddingHorizontal: 10, justifyContent: 'space-between', flex: 1 }}>
                    <Text style={Font.bold14} numberOfLines={1}>
                        {item.title}
                    </Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 8 }}>
                        <Image
                            source={require('../../../Assets/icons/date.png')}
                            style={{ width: 16, height: 16, marginRight: 8 }}
                        />
                        <Text style={[Font.caption, { color: Colors.placeholder }]}>
                            {item?.start?.date + ' - ' + item?.end?.date}
                        </Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            source={require('../../../Assets/icons/time.png')}
                            style={{ width: 16, height: 16, marginRight: 8 }}
                        />
                        <Text style={[Font.caption, { color: Colors.placeholder }]}>
                            {item?.start?.time + ' - ' + item?.end?.time}
                        </Text>
                    </View>
                    <Text
                        style={[Font.reguler12, { color: Colors.gray3, marginTop: 5 }]}
                        numberOfLines={2}>
                        {item.desc.replace(/\&nbsp;/g, '')}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };

    const renderSharing = ({ item }) => {
        return (
            <TouchableOpacity
                style={{
                    flex: 1,
                    width: screenWidth * 0.93,
                    // height: 230,
                    marginHorizontal: 4,
                    borderRadius: 15,
                    marginVertical: 8,
                    elevation: 2,
                    backgroundColor: Colors.white,
                }}
                onPress={() => navigation.navigate('DtlArtikel', { title: 'Sharing', id: item.id, tab: 'header' })}>
                <Image
                    source={{ uri: item.file }}
                    style={{
                        width: screenWidth * 0.93,
                        height: 162,
                        borderRadius: 15,
                    }}
                    resizeMode={'contain'}
                />
                <View style={{ paddingVertical: 8, paddingHorizontal: 10, }}>
                    <Text style={[Font.bold14, { marginBottom: 5 }]} numberOfLines={1}>
                        {item.title}
                    </Text>
                    <Text
                        style={[Font.reguler12, { color: Colors.gray3, }]}
                        numberOfLines={2}>
                        {item.desc.replace(/\&nbsp;/g, '')}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <StatusBar translucent backgroundColor="transparent" barStyle='light-content' />
            <HeaderTitleBack title={route?.params?.title} onPressBack={() => navigation.goBack()} />
            <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: Colors.white, elevation: 5 }}>
                <TouchableOpacity onPress={() => { setTab('Event') }} style={{ width: '50%', borderBottomWidth: 1, borderBottomColor: tab == 'Event' ? Colors.primary : 'transparent', paddingVertical: 14 }}>
                    <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 12, color: tab == 'Event' ? Colors.primary : Colors.black, textAlign: 'center' }}>Event</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { setTab('Sharing') }} style={{ width: '50%', borderBottomWidth: 1, borderBottomColor: tab == 'Sharing' ? Colors.primary : 'transparent', paddingVertical: 14 }}>
                    <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 12, color: tab == 'Sharing' ? Colors.primary : Colors.black, textAlign: 'center' }}>Sharing</Text>
                </TouchableOpacity>
            </View>
            <View style={{ alignItems: 'center', paddingVertical: 8 }}>
                {tab == 'Event' ?
                    <ScrollView showsVerticalScrollIndicator={false}
                        scrollEventThrottle={16}
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { y: scrollY } } }]
                        )}
                        onMomentumScrollEnd={({ nativeEvent }) => {
                            // console.log('eventnn',nativeEvent);
                            if (isCloseToBottom(nativeEvent)) {
                                handleLoadMore()
                            }
                        }}>
                        {shimmer == true ?
                            <View
                                style={{
                                    height: 270,
                                }}>
                                <FlatList
                                    data={[{}, {}, {}]}
                                    horizontal={true}
                                    renderItem={renderShimmer}
                                    style={{ paddingLeft: 12 }}
                                    showsHorizontalScrollIndicator={false}
                                    ListFooterComponent={<View style={{ width: 26 }} />}
                                />
                            </View> :
                            <FlatList
                                data={dataEvent}
                                renderItem={renderEvent}
                                showsVerticalScrollIndicator={false}
                                ListFooterComponent={renderFooter}
                                ListEmptyComponent={() => {
                                    return (
                                        <View style={{ alignItems: 'center', justifyContent: 'center', width: screenWidth * 1, height: screenHeight * 0.7 }}>
                                            <Image source={require('../../../Assets/images/empty.png')} style={{ width: 220, height: 144 }} />
                                            <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.gray1, marginTop: 16 }}>Tidak Ada Data</Text>
                                        </View>
                                    )
                                }}
                                contentContainerStyle={{ marginBottom: 130 }}
                            />}
                    </ScrollView> :
                    <ScrollView showsVerticalScrollIndicator={false}
                        scrollEventThrottle={16}
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { y: scrollY } } }]
                        )}
                        onMomentumScrollEnd={({ nativeEvent }) => {
                            // console.log('eventnn',nativeEvent);
                            if (isCloseToBottom(nativeEvent)) {
                                handleLoadMore()
                            }
                        }}>
                        {shimmer == true ?
                            <View
                                style={{
                                    height: 270,
                                }}>
                                <FlatList
                                    data={[{}, {}, {}]}
                                    horizontal={true}
                                    renderItem={renderShimmer}
                                    style={{ paddingLeft: 12 }}
                                    showsHorizontalScrollIndicator={false}
                                    ListFooterComponent={<View style={{ width: 26 }} />}
                                />
                            </View> :
                            <FlatList
                                data={dataSharing}
                                renderItem={renderSharing}
                                showsVerticalScrollIndicator={false}
                                ListFooterComponent={renderFooter}
                                ListEmptyComponent={() => {
                                    return (
                                        <View style={{ alignItems: 'center', justifyContent: 'center', width: screenWidth * 1, height: screenHeight * 0.7 }}>
                                            <Image source={require('../../../Assets/images/empty.png')} style={{ width: 220, height: 144 }} />
                                            <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.gray1, marginTop: 16 }}>Tidak Ada Data</Text>
                                        </View>
                                    )
                                }}
                                contentContainerStyle={{ marginBottom: 130 }}
                            />}
                    </ScrollView>}
            </View>
        </SafeAreaView>
    );
};

export default ListEventSharing;

const styles = StyleSheet.create({
    wrapFilter: {
        marginRight: 8,
        borderWidth: 1,
        borderColor: Colors.gray5,
        backgroundColor: Colors.container,
        justifyContent: 'center',
        height: 30,
        borderRadius: 100,
        flex: 1,
        marginVertical: 8,
        paddingHorizontal: 16,
    },
    wrapFilterActive: {
        marginRight: 8,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 30,
        borderRadius: 100,
        flex: 1,
        marginVertical: 8,
        paddingHorizontal: 16,
    },
    txt: { p: { fontFamily: 'Roboto-Regular', fontSize: 12, color: Colors.gray3 } }
});
