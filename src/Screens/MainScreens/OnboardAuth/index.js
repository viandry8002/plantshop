import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  ImageBackground,
  StatusBar,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import React from 'react';
import { screenHeight, screenWidth } from '../../../Variable';
import Colors from '../../../Styles/Colors';
import BtnPrimary from '../../Components/BtnPrimary';
import BtnOutline from '../../Components/BtnOutline';

const index = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <ImageBackground
          source={require('../../../Assets/images/bg3.png')}
          style={{
            width: screenWidth,
            height: screenHeight * 0.1,
          }}>
          <StatusBar
            backgroundColor="transparent"
            translucent
            barStyle="light-content"
          />
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <View style={{ alignItems: 'center' }}>
              <View style={{ height: '18%' }} />
              <Text style={{ fontWeight: '600', fontSize: 18, color: Colors.white, fontFamily: 'Roboto-Bold' }}>
                Welcome To
              </Text>
              <Text style={{ fontWeight: '700', fontSize: 33, color: Colors.white, fontFamily: 'Roboto-Bold' }}>
                WAN FLORA
              </Text>
              <View style={{ height: '11%' }} />
              <Image
                source={require('../../../Assets/icons/logo2.png')}
                style={{ width: 136, height: 92 }}
              />
              <Text
                style={{
                  fontWeight: '700',
                  fontSize: 24,
                  color: Colors.primary,
                  width: 250,
                  textAlign: 'center',
                  fontFamily: 'Roboto-Bold'
                }}>
                Sentra Tanaman Hias Terlengkap
              </Text>
              <View style={{ height: 8 }} />
              <Text style={{ width: screenWidth - 60, textAlign: 'center', fontFamily: 'Roboto-Regular', color: Colors.placeholder, fontSize: 14 }}>
                Cari Tanaman hias yang kamu sukai untuk hiasan di rumah semua ada
                disini!
              </Text>
              <View style={{ height: '12%' }} />
              <View style={{}}>
              <View style={{ width: screenWidth * 0.83 }}>
                  <BtnPrimary
                    title={'Register'}
                    pressed={() => navigation.navigate('Register')}
                  />
                </View>
                <View style={{ width: screenWidth * 0.85, marginTop: 16 }}>
                  <BtnOutline
                    title={'Login'}
                    pressed={() => navigation.navigate('Login')}
                  />
                </View>
                

                <View style={{ width: screenWidth * 0.85, marginTop: 16, alignItems: 'center' }}>
                  {/* <BtnOutline
                    title={'Lewati'}
                    pressed={() => navigation.push('Home')}
                  /> */}
                  <TouchableOpacity onPress={() => { 
                    navigation.reset({
                      index: 0,
                      routes: [{ name: 'Home', params: { navv: 'header' } }],
                    })
                    // navigation.push('Home', {nav1: true})
                   }} style={{ width: screenWidth * 0.5, alignItems: 'center', paddingVertical: 8}}>
                    <Text style={{ fontWeight: '600', fontSize: 12, color: Colors.primary, fontFamily: 'Roboto-Bold' }}>
                      Lewati
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>

              {/* <TouchableOpacity onPress={() => { navigation.push('Home') }}>
              <Text style={{ fontWeight: '600', fontSize: 12, color: Colors.primary, fontFamily: 'Roboto-Bold' }}>
                Lewati
              </Text>
            </TouchableOpacity> */}
            </View>
          </View>
        </ImageBackground>
      </ScrollView>
      <View style={{ height: 10 }} />
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({});
