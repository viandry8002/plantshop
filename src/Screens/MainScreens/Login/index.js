import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  ImageBackground,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Alert,
  Dimensions
} from 'react-native';
import React, { useState } from 'react';
import { screenHeight, screenWidth } from '../../../Variable';
import Colors from '../../../Styles/Colors';
import BtnPrimary from '../../Components/BtnPrimary';
import BtnOutline from '../../Components/BtnOutline';
import { Api, Key } from '../../../Screens/Api';
import SpinnerLoading from '../../Components/SpinnerLoading';
import { Input } from '@rneui/themed';
import Icons from 'react-native-vector-icons/Ionicons';
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';
import OneSignal from 'react-native-onesignal';

const index = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isShow, setIsShow] = useState(true);
  const [isFocused, setIsFocused] = useState(false);
  const [isFocused1, setIsFocused1] = useState(false);
  const [alertError, setAlertError] = useState(false);

  // save data login
  const saveDataLogin = async (token, role) => {
    console.log('tokee', token);
    try {
      if (token != null) {
        await AsyncStorage.setItem('user_token', token)
      }
    } catch (error) {
      console.log(error)
    }
  }

  // onLogin
  const onLogin = () => {
    if (email == '') {
      alert('Email tidak boleh kosong');
    } else if (password == '') {
      alert('Password tidak boleh kosong');
    } else {
      setLoading(true)
      // set data
      let data = {
        'email': email,
        'password': password
      }
      // axios post
      axios.post(`${Api}/login`, data, {
        headers: {
          'Accept': 'application/json',
          'key': Key,
        }
      }).then((res) => {
        console.log('result login', res.data);
        if (res.data.success == true) {
          saveDataLogin(res.data.token)
          OneSignal.setAppId("12b2d23e-304f-4f5e-87e5-8958e25972b7"); //akun wanflora1@gmail.com
          OneSignal.setLogLevel(6, 0);
          OneSignal.setExternalUserId(res.data?.data?.id + '', (results) => {
            // The results will contain push and email success statuses
            console.log('onesignal', results);
          })
          navigation.reset({
            index: 0,
            routes: [{ name: 'Home', params: { navv: 'header' } }],
          })
          setLoading(false)
        } else if (res.data.message == 'Akun tidak ditemukan!') {
          setLoading(false)
          alert('Akun belum terdaftar!');
        } else if (res.data.message == 'Akun belum diaktifkan!') {
          setLoading(false)
          alert('Akun belum diaktifkan!');
        } else if (res.data.message == 'Password tidak sesuai!') {
          setLoading(false)
          setAlertError(true)
          setTimeout(() => {
            setAlertError(false);
          }, 1500);
        }
      }).catch((err) => {
        setLoading(false)
        console.log('err login', err)
      })
    }
  }

  // Login Google
  const onLoginGoogle = async () => {
    // function
    try {
      await GoogleSignin.configure({
        offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
        webClientId: "417002044033-5o7tu51e3qt4vofkacacul2u6nf0hpft.apps.googleusercontent.com",
        androidClientId: '417002044033-0aiu8j57sep6cv1murhrhego4nirk1g5.apps.googleusercontent.com'
      });

      await GoogleSignin.hasPlayServices();
      const infoUser = await GoogleSignin.signIn();
      setLoading(true)
      const dataLogin = {
        name: infoUser.user.name,
        email: infoUser.user.email,
      }
      console.log('hasil', dataLogin);
      // await GoogleSignin.signIn().then(result => { 
      //   console.log('result google', result);
      //  });

      // axios post
      axios.post(`${Api}/google`, dataLogin, {
        headers: {
          'Accept': 'application/json',
          'key': Key,
        }
      }).then((res) => {
        console.log('result google', res.data);
        if (res.data.success == true) {
          saveDataLogin(res.data.token)
          OneSignal.setAppId("12b2d23e-304f-4f5e-87e5-8958e25972b7"); //akun wanflora1@gmail.com
          OneSignal.setLogLevel(6, 0);
          OneSignal.setExternalUserId(res.data?.data?.id + '', (results) => {
            // The results will contain push and email success statuses
            console.log('onesignal', results);
          })
          navigation.reset({
            index: 0,
            routes: [{ name: 'Home', params: { navv: 'header' } }],
          })
          setLoading(false)
        } else if (res.data.message == 'Akun tidak ditemukan!') {
          setLoading(false)
          alert('Akun belum terdaftar!');
        } else if (res.data.message == 'Akun belum diaktifkan!') {
          setLoading(false)
          alert('Akun belum diaktifkan!');
        } else if (res.data.message == 'Password tidak sesuai!') {
          setLoading(false)
          setAlertError(true)
          setTimeout(() => {
            setAlertError(false);
          }, 1500);
        }
      }).catch((err) => {
        setLoading(false)
        console.log('err google', err)
      })
      // return;

    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        alert('User cancelled the login flow !');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        alert('Signin in progress');
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        alert('Google play services not available or outdated !');
        // play services not available or outdated
      } else {
        console.log(error, 'err google')
      }
    }
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      {loading == true ? <SpinnerLoading spinner={true} /> : null}
      {/* <ScrollView showsVerticalScrollIndicator={false}> */}
      <ImageBackground source={require('../../../Assets/images/bg3.png')} style={{ width: screenWidth, height: screenHeight - 100 }}>
        <StatusBar backgroundColor="transparent" translucent barStyle="light-content" />
        <View style={{ height: '22%' }} />
        <View
          style={{
            // flex: 1,
            width: screenWidth - 32,
            backgroundColor: Colors.white,
            height: Dimensions.get('window').height * 0.8,
            alignSelf: 'center',
            borderRadius: 15,
            paddingHorizontal: 12,
            paddingVertical: 12,
            elevation: 4,
            marginBottom: 5
          }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View>
              <Text style={{ color: Colors.black, fontSize: 22, fontWeight: '700', textAlign: 'center', fontFamily: 'Roboto-Bold' }}> Login </Text>
              <Input
                onChangeText={(text) => setEmail(text)}
                placeholder={'Email'}
                onFocus={() => { setIsFocused(true) }}
                onBlur={() => { setIsFocused(false) }}
                leftIcon={
                  <Image
                    source={require('../../../Assets/icons/Message.png')}
                    style={{ width: 24, height: 24 }}
                  />
                }
                style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                containerStyle={{
                  height: 48,
                  marginTop: 16,
                  borderWidth: 1,
                  borderRadius: 100,
                  borderColor: isFocused == true ? Colors.primary : Colors.gray,
                }}
              // value={'asdasd'}
              />
              <Input
                onChangeText={(text) => setPassword(text)}
                placeholder={'Password'}
                onFocus={() => { setIsFocused1(true) }}
                onBlur={() => { setIsFocused1(false) }}
                secureTextEntry={isShow == true ? true : false}
                leftIcon={
                  <Image
                    source={require('../../../Assets/icons/Lock.png')}
                    style={{ width: 24, height: 24 }}
                  />
                }
                rightIcon={
                  <TouchableOpacity onPress={() => { setIsShow(!isShow) }}>
                    {isShow == true ?
                      <Image
                        source={require('../../../Assets/icons/hide.png')}
                        style={{ width: 24, height: 24 }}
                      /> :
                      <Icons name='eye-outline' size={24} color={Colors.black} />}
                  </TouchableOpacity>
                }
                style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                containerStyle={{
                  height: 48,
                  marginTop: 16,
                  borderWidth: 1,
                  borderRadius: 100,
                  borderColor: isFocused1 == true ? Colors.primary : Colors.gray,
                }}
              // value={'asdasd'}
              />
              <View style={{ height: 16 }} />
              <TouchableOpacity onPress={() => { navigation.navigate('ForgotPassword') }} style={{ alignSelf: 'flex-end' }}>
                <Text style={{ fontWeight: '700', fontSize: 12, color: Colors.primary, fontFamily: 'Roboto-Bold' }}> Lupa Password? </Text>
              </TouchableOpacity>
              <View style={{ height: 16 }} />
              <BtnPrimary title={'Login'} pressed={() => { onLogin() }} />
              {/* <Text style={{ textAlign: 'center', fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}> Belum Punya Akun? </Text> */}
              <View style={{ height: 16 }} />
              <View style={{}}>
                <BtnOutline title={'Register'} pressed={() => { navigation.navigate('Register') }} />
              </View>
              <View style={{ height: 16 }} />
              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <View style={{ height: 1, width: '30%', backgroundColor: Colors.container }} />
                <Text style={{ textAlign: 'center', fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}>
                  Atau masuk dengan
                </Text>
                <View style={{ height: 1, width: '30%', backgroundColor: Colors.container }} />
              </View>
              <View style={{ height: 16 }} />
              <TouchableOpacity onPress={() => { onLoginGoogle() }} style={{ flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderColor: Colors.container, borderRadius: 15, paddingVertical: 10, paddingHorizontal: 16 }}>
                <Image source={require('../../../Assets/images/google_icon.png')} style={{ width: 20, height: 20 }} />
                <Text style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black, textAlign: 'center', width: '90%' }}>Masuk Menggunakan Google</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      </ImageBackground>
      {/* </ScrollView> */}
      {alertError == true ?
        <View style={{ backgroundColor: Colors.error, borderRadius: 6, width: screenWidth * 0.85, alignSelf: 'center', paddingVertical: 8, position: 'absolute', bottom: 20 }}>
          <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 11, color: Colors.white, textAlign: 'center' }}>Password tidak sesuai!</Text>
        </View> : null}
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({});
