import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  ImageBackground,
  StatusBar,
  TouchableOpacity,
  FlatList,
  ScrollView
} from 'react-native';
import React, { useEffect, useState } from 'react';
import { screenHeight, screenWidth } from '../../../Variable';
import Colors from '../../../Styles/Colors';
import Icons from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Input } from '@rneui/themed';
import Font from '../../../Styles/Fonts';
import Horizontal from '../../Components/Horizontal';
import Money from '../../Components/Money';
import MasonryList from '@react-native-seoul/masonry-list';
import { Api, Key, AdMob } from '../../../Screens/Api';
import BeforeLogin from '../../Components/BeforeLogin';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceholder from 'react-native-shimmer-placeholder';
import { AdMobBanner } from 'react-native-admob';

const index = ({ navigation, route }) => {
  // let scrollY = new Animated.Value(0);
  const [headerShown, setHeaderShown] = useState(false);
  const [shimmer, setShimmer] = useState(false);
  const [tokens, setTokens] = useState('');
  const [dataDetail, setDataDetail] = useState('');
  const [dataProduct, setDataProduct] = useState([]);
  const [showAlert, setShowAlert] = useState(false);
  const [message, setMessage] = useState('');
  const [showLogin, setShowLogin] = useState(false);

  useEffect(() => {
    // get data login
    async function getDataLogin() {
      try {
        const token = await AsyncStorage.getItem('user_token')
        setTokens(token)
        getDetail(token)
        getDataProduct(token)
      } catch (e) {
        console.log(e)
      }
    }
    getDataLogin();

    navigation.setOptions({
      headerStyle:
        headerShown === true
          ? { backgroundColor: Colors.primary }
          : { backgroundColor: 'transparent' },
      // headerStyle: {
      //   backgroundColor: scrollY.interpolate({
      //     inputRange: [0, screenHeight / 2 - 40],
      //     outputRange: ['transparent', Colors.primary],
      //     extrapolate: 'clamp',
      //   }),
      // },
    });
    // navigation.setParams({
    //   bgColor: scrollY.interpolate({
    //     inputRange: [0, SCREEN_HEIGHT / 2 - 40],
    //     outputRange: ['transparent', 'rgb(255,0,0)'],
    //     extrapolate: 'clamp',
    //   }),
    // });
  }, []);

  console.log('params', headerShown);

  const getDetail = (token) => {
    setShimmer(true)
    // axios get
    axios.get(`${Api}/store/find/${route?.params?.id}`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      console.log('result detail', res.data);
      setDataDetail(res.data.data)
      // setShimmer(false);
    }).catch((err) => {
      setShimmer(false);
      console.log('err detail', err.response)
    })
  }

  const getDataProduct = (token) => {
    setShimmer(true)
    // axios get
    axios.get(`${Api}/product?id_store=${route?.params?.id}`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      console.log('result product', res.data);
      setDataProduct(res.data.data)
      setShimmer(false);
    }).catch((err) => {
      setShimmer(false);
      console.log('err product', err.response)
    })
  }

  const addFollow = (id) => {
    let data = {
      id_store: id
    }
    setShimmer(true)
    // axios post
    axios.post(`${Api}/store/follow`, data, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + tokens,
      }
    }).then((res) => {
      console.log('result follow', res.data);
      getDetail(tokens);
      setMessage(res.data.message)
      setShowAlert(true)
      setTimeout(() => {
        setShowAlert(false)
      }, 500);
      setShimmer(false);
    }).catch((err) => {
      setShimmer(false);
      console.log('err follow', err.response)
    })
  }

  const shimmerProduct = () => {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 8 }}>
        <View
          style={{
            flex: 0.5,
            margin: 4,
            borderRadius: 15,
            backgroundColor: Colors.white,
            height: 220
          }}>
          <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '100%', height: 166, borderRadius: 15 }} />
          <View style={{ padding: 8, justifyContent: 'space-between', flex: 1 }}>
            <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '80%', height: 16, borderRadius: 7 }} />
            <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '70%', height: 16, borderRadius: 7, marginTop: 8 }} />
          </View>
        </View>
        <View
          style={{
            flex: 0.5,
            margin: 4,
            borderRadius: 15,
            backgroundColor: Colors.white,
            height: 220,
            marginLeft: 8
          }}>
          <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '100%', height: 166, borderRadius: 15 }} />
          <View style={{ padding: 8, flex: 1, paddingVertical: 5 }}>
            <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '80%', height: 16, borderRadius: 7 }} />
            <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '70%', height: 16, borderRadius: 7, marginTop: 8 }} />
          </View>
        </View>
      </View>
    )
  }

  const renderProduct = ({ item }) => {
    return (
      <TouchableOpacity
        style={[
          {
            flex: 0.5,
            margin: 4,
            borderRadius: 15,
            backgroundColor: Colors.white,
          },
          item.discount_percent ? { height: 242 } : { height: 220 },
        ]}
        onPress={() => navigation.push('DtlProduk', { id: item.id, nav: true, tab: 'header'})}>
        <Image
          source={{ uri: item.file }}
          style={{
            width: '100%',
            height: 166,
            borderRadius: 15,
          }}
          resizeMode={'contain'}
        />
        {item.discount_percent ? (
          <View
            style={{
              position: 'absolute',
              backgroundColor: Colors.error,
              width: 70,
              alignItems: 'center',
              borderTopLeftRadius: 15,
              borderBottomRightRadius: 15,
            }}>
            <Text style={[Font.reguler12, { color: Colors.white }]}>
              {item.discount_percent} Disc
            </Text>
          </View>
        ) : (
          false
        )}
        <View style={{ padding: 8, justifyContent: 'space-between', flex: 1 }}>
          <Text style={Font.reguler12} numberOfLines={1}>
            {item.title}
          </Text>
          <Money
            value={item.price_discount}
            styling={[Font.bold14, { color: Colors.primary }]}
          />
          {item.discount_percent ? (
            <Money
              value={item.price}
              styling={[
                Font.reguler12,
                { color: Colors.placeholder, textDecorationLine: 'line-through' },
              ]}
            />
          ) : (
            false
          )}
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      {showLogin == false ?
        <>
          <StatusBar backgroundColor={'transparent'} translucent barStyle={'light-content'} />
          <ImageBackground
            source={require('../../../Assets/images/bg3.png')}
            style={{
              flex: 1,
              width: screenWidth,
              height: screenHeight - 350,
            }}>
            {/* Header */}
            <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 43, paddingHorizontal: 16, paddingBottom: 10, backgroundColor: headerShown == true ? Colors.primary : 'transparent' }}>
              <TouchableOpacity onPress={() => { navigation.goBack() }} style={{ marginRight: 20 }}>
                <Icons name={'arrow-left'} size={28} color={Colors.white} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {navigation.navigate('SearchProduct', { title: 'Store' })}}
                style={{
                  backgroundColor: Colors.white,
                  height: 35,
                  borderRadius: 100,
                  width: screenWidth - 90,
                  paddingHorizontal: 8,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Ionicons
                  name="search"
                  size={18}
                  color={Colors.placeholder}
                  style={{ marginRight: 11 }}
                  onPress={() => { }}
                />
                <Text style={Font.reguler12}>Cari “tanaman hias”</Text>
              </TouchableOpacity>
            </View>
            <ScrollView
              onScroll={event => {
                const scrolling = event.nativeEvent.contentOffset.y;
                if (scrolling > 100) {
                  setHeaderShown(true);
                } else {
                  setHeaderShown(false);
                }
              }}
              showsVerticalScrollIndicator={false}>
              <View style={{ height: 45 }} />
              {/* DtlToko */}
              <View style={{}}>
                <View style={{ alignItems: 'center' }}>
                  {shimmer == true ?
                    <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: 104, height: 104, borderRadius: 15 }} /> :
                    <Image
                      source={{ uri: dataDetail?.file }}
                      style={{
                        width: 104,
                        height: 104,
                        borderRadius: 15,
                      }}
                    />}
                </View>
                <View style={{ height: 16 }} />
                <View style={{ paddingHorizontal: 16, backgroundColor: Colors.white, paddingTop: 8 }}>
                  {/* Follow */}
                  <View
                    style={{
                      //   flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'space-between'
                    }}>
                    <View style={{ flex: 1 }}>
                      {shimmer == true ?
                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '70%', height: 16, borderRadius: 7 }} /> :
                        <Text style={Font.bold16}>{dataDetail?.name}</Text>}
                      {shimmer == true ?
                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '60%', height: 16, borderRadius: 7, marginTop: 8 }} /> :
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                          <Image
                            source={require('../../../Assets/icons/Pin_fill.png')}
                            style={{ width: 24, height: 24 }}
                          />
                          <Text style={Font.reguler12}>{dataDetail?.city?.city_name}, {dataDetail?.province?.province_name}</Text>
                        </View>}
                    </View>
                    <View style={{}}>
                      <TouchableOpacity onPress={() => { tokens !== null ? addFollow(dataDetail?.id) : setShowLogin(true) }} style={{ backgroundColor: Colors.primary, borderRadius: 20, alignItems: 'center', paddingVertical: 10, paddingHorizontal: 16 }}>
                        <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12, color: Colors.white }}>{dataDetail?.following == 1 ? 'Following' : 'Follow'}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  {/* counting */}
                  <View style={{ height: 16 }} />
                  {/* <Horizontal coloring={Colors.container} /> */}
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{ alignItems: 'center', width: 90 }}>
                      {shimmer == true ? <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '20%', height: 17, borderRadius: 7, marginTop: 8 }} /> :
                        <Text style={Font.bold24}>{dataDetail?.product_count}</Text>}
                      <Text style={[Font.reguler12, { color: Colors.placeholder }]}>
                        Produk
                      </Text>
                    </View>
                    <View
                      style={{
                        borderLeftWidth: 1,
                        borderLeftColor: Colors.container,
                        borderRightColor: Colors.container,
                        borderRightWidth: 1,
                        alignItems: 'center',
                        width: 90,
                      }}>
                      {shimmer == true ? <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '20%', height: 17, borderRadius: 7, marginTop: 8 }} /> :
                        <Text style={Font.bold24}>{dataDetail?.follower_count}</Text>}
                      <Text style={[Font.reguler12, { color: Colors.placeholder }]}>
                        Follower
                      </Text>
                    </View>
                    <View style={{ alignItems: 'center', width: 90 }}>
                      {shimmer == true ?
                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '20%', height: 17, borderRadius: 7, marginTop: 8 }} /> :
                        <Text style={Font.bold24}>{dataDetail?.product_favorit_count}</Text>}
                      <Text style={[Font.reguler12, { color: Colors.placeholder }]}>
                        Produk Favorit
                      </Text>
                    </View>
                  </View>
                  <View style={{ height: 16 }} />
                </View>
              </View>

              <View
                style={{
                  backgroundColor: Colors.container,
                  // height: screenHeight
                }}>
                {shimmer == true ? shimmerProduct() :
                  (dataProduct.length == 0 ?
                    <View style={{ alignItems: 'center', justifyContent: 'center', width: screenWidth * 1, height: screenHeight * 0.5 }}>
                      <Image source={require('../../../Assets/images/empty.png')} style={{ width: 220, height: 144 }} />
                      <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.gray1, marginTop: 16 }}>Tidak Ada Data</Text>
                    </View> :
                    <MasonryList
                      data={dataProduct}
                      numColumns={2}
                      renderItem={renderProduct}
                      style={{ padding: 4 }}
                      showsVerticalScrollIndicator={false}
                      ListFooterComponent={<View style={{ height: 10 }} />}
                    />)}
              </View>
            </ScrollView>
            {/* admob */}
            <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
              <AdMobBanner
                adSize={'banner'}
                adUnitID={AdMob}
                testDevices={[AdMobBanner.simulatorId]}
                onAdFailedToLoad={error => console.error(error, 'err admob')}
              />
            </View>
            {showAlert == true ?
              <View style={{ backgroundColor: Colors.white, elevation: 5, borderRadius: 30, paddingVertical: 10, position: 'absolute', bottom: '10%', width: screenWidth * 0.6, alignSelf: 'center', alignItems: 'center' }}>
                <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 13, color: Colors.black }}>{message}</Text>
              </View> : null}
          </ImageBackground>
        </>
        :
        <>
          <StatusBar backgroundColor={Colors.primary} barStyle={'light-content'} />
          <View style={{ backgroundColor: Colors.primary, width: screenWidth, height: 75 }}>
            <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 16, color: Colors.white, marginLeft: 25, paddingTop: 43 }}>Toko</Text>
          </View>
          <BeforeLogin navigation={navigation} />
        </>}
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({});
