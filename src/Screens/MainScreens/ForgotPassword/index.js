import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    Image,
    ImageBackground,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    Alert,
    Modal
} from 'react-native';
import React, { useState } from 'react';
import { screenHeight, screenWidth } from '../../../Variable';
import Colors from '../../../Styles/Colors';
import BtnPrimary from '../../Components/BtnPrimary';
import BtnOutline from '../../Components/BtnOutline';
import { Api, Key } from '../../../Screens/Api';
import SpinnerLoading from '../../Components/SpinnerLoading';
import { Input } from '@rneui/themed';
import Icons from 'react-native-vector-icons/Ionicons';

const ForgotPassword = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [isFocused, setIsFocused] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);

    // sendEmail
    const sendEmail = () => {
        if (email == '') {
            alert('Email tidak boleh kosong');
        } else {
            setLoading(true)
            // set data
            let data = {
                'email': email
            }
            // axios post
            axios.post(`${Api}/reset-password`, data, {
                headers: {
                    'Accept': 'application/json',
                    'key': Key,
                }
            }).then((res) => {
                console.log('result reset', res.data);
                if (res.data.success == true) {
                    setLoading(false)
                    setModalVisible(true)
                } else if (res.data.message == 'Akun tidak ditemukan!') {
                    setLoading(false)
                    alert('Akun belum terdaftar!');
                }else if(res.data.success == false){
                    setLoading(false)
                }

            }).catch((err) => {
                setLoading(false)
                console.log('err reset', err)
            })
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
            {loading == true ? <SpinnerLoading spinner={true} /> : null}
            <ScrollView showsVerticalScrollIndicator={false}>
                <ImageBackground
                    source={require('../../../Assets/images/bg3.png')}
                    style={{
                        width: screenWidth,
                        height: screenHeight - 100,
                    }}>
                    <StatusBar
                        backgroundColor="transparent"
                        translucent
                        barStyle="light-content"
                    />
                    <View style={{ height: '23%' }} />
                    <View
                        style={{
                            // flex: 1,
                            width: screenWidth - 32,
                            backgroundColor: Colors.white,
                            alignSelf: 'center',
                            borderRadius: 15,
                            padding: 15,
                            elevation: 4,
                        }}>
                        <Text
                            style={{
                                color: Colors.black,
                                fontSize: 24,
                                fontWeight: '700',
                                textAlign: 'center',
                                fontFamily: 'Roboto-Bold'
                            }}>
                            Reset Password
                        </Text>
                        <Input
                            onChangeText={(text) => setEmail(text)}
                            placeholder={'Email'}
                            onFocus={() => { setIsFocused(true) }}
                            onBlur={() => { setIsFocused(false) }}
                            leftIcon={
                                <Image
                                    source={require('../../../Assets/icons/Message.png')}
                                    style={{ width: 24, height: 24 }}
                                />
                            }
                            style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{
                                height: 48,
                                marginTop: 16,
                                borderWidth: 1,
                                borderRadius: 100,
                                borderColor: isFocused == true ? Colors.primary : Colors.gray,
                            }}
                        />
                        <View style={{ height: 16 }} />
                        <BtnPrimary title={'Reset'} pressed={() => { sendEmail() }} />
                    </View>
                </ImageBackground>
            </ScrollView>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Image source={require('../../../Assets/images/success.png')} style={{ width: 90, height: 90 }} />
                        <Text style={styles.titlemodal}>Berhasil</Text>
                        <Text style={styles.descmodal}>Silahkan cek email untuk melanjutkan aktivasi</Text>
                        {/* button */}
                        <View style={styles.containerbtnmodal}>
                            <TouchableOpacity onPress={() => { setModalVisible(false), navigation.navigate('Login') }} style={styles.btnmodal}>
                                <Text style={styles.textmodal}>Ok</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        </SafeAreaView>
    );
};

export default ForgotPassword;

const styles = StyleSheet.create({
    // modal
    centeredView: { flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: '#00000070' },
    modalView: { backgroundColor: Colors.white, borderRadius: 15, width: screenWidth * 0.8, shadowColor: "#000", shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.25, shadowRadius: 4, elevation: 5, paddingVertical: 16, paddingHorizontal: 16, alignItems: 'center' },
    titlemodal: { fontSize: 15, fontFamily: 'Roboto-Medium', lineHeight: 21, color: Colors.black, textAlign: 'center', marginTop: 5 },
    descmodal: { fontSize: 13, fontFamily: 'Roboto-Regular', lineHeight: 18, color: Colors.black, textAlign: 'center', marginTop: 5 },
    containerbtnmodal: { flexDirection: 'row', justifyContent: 'space-between', marginTop: 16 },
    btnmodal: { width: '90%', height: 36, borderWidth: 1, borderColor: Colors.primary, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.primary },
    textmodal: { fontSize: 12, fontFamily: 'Roboto-Medium', lineHeight: 18, color: Colors.white },

});
