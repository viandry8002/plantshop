import {
    SafeAreaView,
    StyleSheet,
    Text,
    View,
    FlatList,
    TouchableOpacity,
    Image,
    RefreshControl,
    ScrollView,
    StatusBar,
    ActivityIndicator,
    Linking,
    Animated,
    Share
} from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState, useEffect } from 'react';
import Colors from '../../../Styles/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Font from '../../../Styles/Fonts';
import { screenWidth, screenHeight } from '../../../Variable';
import { Api, Key, AdMob } from '../../../Screens/Api';
import BeforeLogin from '../../Components/BeforeLogin';
import ReadMore from '@fawazahmed/react-native-read-more';
import clip from "text-clipper";
import { SliderBox } from 'react-native-image-slider-box';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceholder from 'react-native-shimmer-placeholder';
import { AdMobBanner } from 'react-native-admob';
import HTMLView from 'react-native-htmlview';
import { Badge } from '@rneui/themed';
import Octicons from 'react-native-vector-icons/Octicons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SpinnerLoading from '../../Components/SpinnerLoading';

const dataSharing = [
    require('../../../Assets/images/sharing.png'),
    require('../../../Assets/images/sharing.png'),
    require('../../../Assets/images/sharing.png'),
];

const index = ({ navigation, route }) => {
    const [refreshing, setRefreshing] = useState(false);
    const [shimmer, setShimmer] = useState(true);
    const [tokens, setTokens] = useState('');
    const [listData, setListData] = useState([]);
    const [showAlert, setShowAlert] = useState(false);
    const [message, setMessage] = useState('');
    const [showLogin, setShowLogin] = useState(false);
    const [lines, setLines] = useState(3);
    const [countNotif, setCountNotif] = useState(0);

    // scrolled
    const [nextLink, setNextLink] = useState('');
    const [hasScrolled, sethasScrolled] = useState(false);
    const [loadMore, setLoadMore] = useState(false);
    const [pass, setPass] = useState(false);
    const [scrollY, setScrollY] = useState(new Animated.Value(0));

    const onRefresh = () => {
        setRefreshing(true);
        setTimeout(() => {
            setShimmer(true)
            getListFeed(tokens)
            setRefreshing(false);
        }, 2000);
    };

    useEffect(() => {
        Linking.addEventListener('url', event => handleDeeplink(event.url));
        // get data login
        async function getDataLogin() {
            try {
                const token = await AsyncStorage.getItem('user_token')
                setTokens(token)
                getCountNotif(token)
                getListFeed(token)
            } catch (e) {
                console.log(e)
            }
        }
        getDataLogin();
    }, []);
    console.log('routee feed', route.params.navv);

    async function handleDeeplink(url) {
        // setTypeLink('')
        console.log('url', url);
        try {
            // const url = await Linking.getInitialURL();
            // const url2 = await Linking.addEventListener('url', event => { return event });
            // console.log(url2, 'detailllllllllll', url);
            if (url) {
                let newUrl = `${url}`;

                newUrl = newUrl.slice(newUrl.indexOf('://') + 3);

                if (newUrl.indexOf('www.wanflora.com') >= 0) {
                    newUrl = newUrl.slice(newUrl.indexOf('www.wanflora.com/') + 'www.wanflora.com/'.length);
                }

                const [rawUri, param = ""] = newUrl.split('?');
                const [uriType, uriId, uriIv, uriKey] = rawUri.split('/');
                const uri = {
                    type: uriType || '',
                    param: uriId || '',
                    paramKey: uriKey || '',
                    paramIv: uriIv || '',
                    query: param
                        .split('&')
                        .reduce((all, d) => {

                            const [key, value] = d.split('=');

                            all[key] = value;

                            return all;
                        }, {})
                };

                return;
            } else {
                getDataLogin(null, '');
            }
        } catch (err) {
            console.error('An error occurred on get deeplink', err);
        }
    }

    // on share
    const onShare = async (data) => {
        try {
            // const urlLinking = await Linking.getInitialURL(data.id);
            // handleDeeplink(url);
            const result = await Share.share({
                message: `Beli ${data?.title} di ${data?.store?.name} dengan harga Rp${data?.price}, Sekarang! https://www.wanflora.com/DtlProduk/${data?.id}`,
                url: `https://www.wanflora.com/DtlProduk/${data?.id}`
            });
            return result
        } catch (error) {
            alert(error.message);
        }
    };

    const getListFeed = (token, type) => {
        // setShimmer(true)
        // axios get
        axios.get(`${Api}/feed`, {
            headers: {
                'Accept': 'application/json',
                'key': Key,
                'Authorization': 'Bearer ' + token,
            }
        }).then((res) => {
            console.log('result list', res.data);
            if (res.data.next_page_url === null) {
                (setPass(true), sethasScrolled(false))
            } else {
                sethasScrolled(true)
                setNextLink(res.data.next_page_url);
            }
            setListData(res.data.data)
            setShimmer(false);
            // setIsLoading(false)
        }).catch((err) => {
            setShimmer(false);
            // setIsLoading(false)
            console.log('err list', err)
        })
    }

    const getCountNotif = (token) => {
        // setShimmer(true)
        // axios get
        axios.get(`${Api}/notifications/count`, {
            headers: {
                'Accept': 'application/json',
                'key': Key,
                'Authorization': 'Bearer ' + token,
            }
        }).then((res) => {
            console.log('result count', res.data);
            setCountNotif(res.data.count)
        }).catch((err) => {
            setShimmer(false);
            console.log('err count', err)
        })
    }

    const addFavorite = (id) => {
        console.log('id fa', id);
        let data = {
            id_product: id
        }
        // setShimmer(true)
        // axios post
        axios.post(`${Api}/product/favorit`, data, {
            headers: {
                'Accept': 'application/json',
                'key': Key,
                'Authorization': 'Bearer ' + tokens,
            }
        }).then((res) => {
            console.log('result favorite', res.data);
            getListFeed(tokens);
            setMessage(res.data.message)
            setShowAlert(true)
            setTimeout(() => {
                setShowAlert(false)
            }, 500);
            setShimmer(false);
        }).catch((err) => {
            setShimmer(false);
            console.log('err favorite', err.response)
        })
    }

    const seen = new Set();
    const filteredArr = listData?.filter(el => {
        const duplicate = seen.has(el.id);
        seen.add(el.id);
        return !duplicate;
    });

    const isCloseToBottom = (data) => {
        // console.log(data, 'isssssss');
        const paddingToBottom = 20
        return data?.layoutMeasurement.height + data?.contentOffset.y >=
            data?.contentSize.height - paddingToBottom
    }

    const renderFooter = () => {
        return (
            hasScrolled === true ?
                <ActivityIndicator size={'large'} animating /> : null
        )
    }

    const handleLoadMore = () => {
        if (loadMore) {
            axios.get(`${nextLink}`, {
                headers: {
                    'Accept': 'application/json',
                    'key': Key,
                    'Authorization': 'Bearer ' + tokens,
                }
            }).then(res => {
                console.log('res2', res.data.data);
                sethasScrolled(true);
                setListData([...listData, ...res.data.data]);
                setNextLink(res.data.next_page_url);

                res.data.next_page_url === null ?
                    (sethasScrolled(false), setPass(true)) :
                    false;
            }).catch(err => {
                console.log(err);
            });
            return
        }
        setLoadMore(true)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
            <StatusBar backgroundColor={Colors.primary} barStyle="light-content" />
            {/* header */}
            <View style={{ backgroundColor: Colors.primary, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 14, height: route.params.navv !== undefined ? 60 : (route.params.nav1 !== undefined ? 85 : (route.params == undefined ? 80 : 60)), paddingTop: route.params.navv !== undefined ? screenHeight * 0.01 : (route?.params?.nav1 !== undefined ? screenHeight * 0.035 : (route?.params == undefined ? screenHeight * 0.03 : screenHeight * 0.01)) }}>
                <TouchableOpacity
                    onPress={() => { navigation.navigate('SearchProduct', { title: 'Product' }) }}
                    style={{
                        backgroundColor: Colors.white,
                        height: 35,
                        borderRadius: 100,
                        width: screenWidth - 90,
                        paddingHorizontal: 8,
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                    <Ionicons
                        name="search"
                        size={18}
                        color={Colors.placeholder}
                        style={{ marginRight: 11 }}
                        onPress={() => { }}
                    />
                    <Text style={Font.reguler12}>Cari “tanaman hias”</Text>
                </TouchableOpacity>
                <View style={{ marginRight: 16 }}>
                    <Octicons
                        name="bell-fill"
                        size={20}
                        color={Colors.white}
                        // style={{marginLeft: 16}}
                        onPress={() => navigation.navigate('Notification')}
                    />
                    {countNotif == 0 ? null :
                        <Badge
                            value={countNotif}
                            containerStyle={{ position: 'absolute', top: -9, left: 10 }}
                            badgeStyle={{
                                backgroundColor: Colors.error,
                                borderColor: Colors.white,
                                borderWidth: 1,
                            }}
                        />}
                </View>
            </View>
            {/* body */}
            {showLogin == false ?
                <>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        refreshControl={
                            <RefreshControl
                                refreshing={refreshing}
                                onRefresh={onRefresh}
                                colors={[Colors.primary]}
                            />
                        }
                        scrollEventThrottle={16}
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { y: scrollY } } }]
                        )}
                        onMomentumScrollEnd={({ nativeEvent }) => {
                            // console.log('eventnn',nativeEvent);
                            if (isCloseToBottom(nativeEvent)) {
                                handleLoadMore()
                            }
                        }}>
                        <View style={{ flex: 1 }}>
                            {shimmer == true ?
                                <View style={{ paddingVertical: 8 }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 16 }}>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Image source={require('../../../Assets/images/toko2.png')} style={{ width: 36, height: 36, borderRadius: 8 }} />
                                            <View style={{ marginLeft: 8 }}>
                                                <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.5, height: 16, marginVertical: 8, borderRadius: 7 }} />
                                                <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.4, height: 16, marginVertical: 8, borderRadius: 7 }} />
                                            </View>
                                        </View>
                                        <TouchableOpacity onPress={() => { }} style={styles.btn}>
                                            <Text style={styles.txtBtn}>Cek Produk</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth, height: 375, marginVertical: 8 }} />
                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 16 }}>
                                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.3, height: 16, marginVertical: 8, borderRadius: 7 }} />
                                        <TouchableOpacity onPress={() => { }}>
                                            <AntDesign
                                                name="sharealt"
                                                size={20}
                                                color={Colors.black}
                                                onPress={() => { }}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ paddingHorizontal: 16, paddingVertical: 8 }}>
                                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.5, height: 15, marginVertical: 4, borderRadius: 7 }} />
                                        <>
                                            <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.9, height: 14, marginVertical: 4, borderRadius: 7 }} />
                                            <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.4, height: 14, marginVertical: 4, borderRadius: 7 }} />
                                        </>
                                    </View>
                                </View> :
                                <FlatList
                                    refreshControl={
                                        <RefreshControl
                                            refreshing={refreshing}
                                            onRefresh={onRefresh}
                                            colors={[Colors.primary]}
                                        />
                                    }
                                    data={listData}
                                    renderItem={({ item, index }) => {
                                        // console.log('preeeem', item, index);
                                        const sliders = item?.files?.map((a) => {
                                            return (a.file)
                                        })
                                        return (
                                            <>
                                                {index % 6 == 0 ?
                                                    <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
                                                        <AdMobBanner
                                                            adSize={'banner'}
                                                            adUnitID={AdMob}
                                                            testDevices={[AdMobBanner.simulatorId]}
                                                            onAdFailedToLoad={error => console.error(error, 'err admob')}
                                                        />
                                                    </View> : null}
                                                <View style={{ paddingVertical: 8 }}>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 16 }}>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                            <Image source={require('../../../Assets/images/toko2.png')} style={{ width: 36, height: 36, borderRadius: 8 }} />
                                                            <View style={{ marginLeft: 8 }}>
                                                                <Text style={styles.txtTitle}>{item?.title}</Text>
                                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 3 }}>
                                                                    <Image source={require('../../../Assets/icons/Pin_fill.png')} style={{ width: 20, height: 20 }} />
                                                                    <Text style={Font.reguler12}>{item?.store?.city?.city_name}, {item?.store?.province?.province_name}</Text>
                                                                </View>
                                                            </View>
                                                        </View>
                                                        <TouchableOpacity onPress={() => { navigation.navigate('DtlProduk', { id: item?.id, nav: true }) }} style={styles.btn}>
                                                            <Text style={styles.txtBtn}>Cek Produk</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    {/* slider */}
                                                    <SliderBox
                                                        // autoplay
                                                        circleLoop
                                                        images={sliders}
                                                        ImageComponentStyle={{
                                                            height: 375,
                                                            width: screenWidth,
                                                            marginVertical: 10
                                                        }}
                                                        paginationBoxStyle={{
                                                            alignSelf: 'center',
                                                        }}
                                                        dotStyle={{
                                                            width: 8,
                                                            height: 8,
                                                            marginHorizontal: -20,
                                                            marginBottom: 10
                                                        }}
                                                        dotColor={Colors.primary}
                                                        inactiveDotColor={Colors.container}
                                                        parentWidth={screenWidth}
                                                    />
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 16 }}>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                            {item?.favorit == 0 ?
                                                                <TouchableOpacity onPress={() => { tokens !== null ? addFavorite(item?.id) : setShowLogin(true) }}>
                                                                    <AntDesign
                                                                        name="hearto"
                                                                        size={20}
                                                                        color={Colors.black}
                                                                        style={{ marginRight: 8 }}
                                                                    />
                                                                </TouchableOpacity> :
                                                                <TouchableOpacity onPress={() => { addFavorite(item?.id) }}>
                                                                    <AntDesign
                                                                        name="heart"
                                                                        size={20}
                                                                        color={Colors.primary}
                                                                        style={{ marginRight: 8 }}
                                                                    />
                                                                </TouchableOpacity>}
                                                            <Text style={styles.txtLike}>{item?.favorit_count} Suka</Text>
                                                        </View>
                                                        <TouchableOpacity onPress={() => { onShare(item) }}>
                                                            <AntDesign
                                                                name="sharealt"
                                                                size={20}
                                                                color={Colors.black}
                                                            />
                                                        </TouchableOpacity>
                                                    </View>
                                                    <View style={{ paddingHorizontal: 16, paddingVertical: 8 }}>
                                                        <Text style={styles.txtTitle}>{item?.title}</Text>
                                                        <ReadMore numberOfLines={3} style={styles.txtDesc} seeMoreText={'Baca Selengkapnya'} seeMoreStyle={styles.txtRead} seeLessText={'Sembunyikan'} seeLessStyle={styles.txtRead}>
                                                            {item?.desc}
                                                        </ReadMore>
                                                        {/* <HTMLView
                                                            value={"<div>" + item?.desc + "</div>"}
                                                            stylesheet={styles.txt}
                                                            numberOfLines={0}
                                                        /> */}
                                                        {/* {lines == 3 ?
                                                            <TouchableOpacity onPress={() => { setLines(0), getListFeed(tokens, 'type') }} style={{ paddingVertical: 3 }}>
                                                                <Text style={styles.txtRead}>Baca Selengkapnya</Text>
                                                            </TouchableOpacity> :
                                                            <TouchableOpacity onPress={() => { setLines(3), getListFeed(tokens, 'type') }} style={{ paddingVertical: 3 }}>
                                                                <Text style={styles.txtRead}>Sembunyikan</Text>
                                                            </TouchableOpacity>} */}
                                                    </View>
                                                </View>
                                                <View style={{ borderWidth: 0.5, borderColor: Colors.container }} />
                                            </>
                                        )
                                    }}
                                    keyExtractor={(item, index) => index.toString()}
                                    showsVerticalScrollIndicator={false}
                                    ListFooterComponent={renderFooter}
                                    ListEmptyComponent={() => {
                                        return (
                                            <View style={{ alignItems: 'center', justifyContent: 'center', width: screenWidth * 1, height: screenHeight * 0.7 }}>
                                                <Image source={require('../../../Assets/images/empty.png')} style={{ width: 220, height: 144 }} />
                                                <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.gray1, marginTop: 16 }}>Tidak Ada Data</Text>
                                            </View>
                                        )
                                    }}
                                />}
                        </View>
                    </ScrollView>
                    <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
                        <AdMobBanner
                            adSize={'banner'}
                            adUnitID={AdMob}
                            testDevices={[AdMobBanner.simulatorId]}
                            onAdFailedToLoad={error => console.error(error, 'err admob')}
                        />
                    </View>
                </> :
                <View style={{ flex: 1 }}>
                    <BeforeLogin navigation={navigation} />
                </View>}
            {showAlert == true ?
                <View style={{ backgroundColor: Colors.white, elevation: 5, borderRadius: 30, paddingVertical: 10, position: 'absolute', bottom: '10%', width: screenWidth * 0.6, alignSelf: 'center', alignItems: 'center' }}>
                    <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 13, color: Colors.black }}>{message}</Text>
                </View> : null}
        </SafeAreaView>
    );
};

export default index;

const styles = StyleSheet.create({
    txtTitle: { fontFamily: 'Roboto-Bold', fontSize: 12, color: Colors.black },
    btn: { borderRadius: 30, paddingVertical: 8, paddingHorizontal: 16, borderWidth: 1, borderColor: Colors.primary },
    txtBtn: { fontFamily: 'Roboto-Bold', fontSize: 12, color: Colors.primary },
    txtLike: { fontFamily: 'Roboto-Bold', fontSize: 12, color: Colors.gray1, marginLeft: 3 },
    txtDesc: { fontFamily: 'Roboto-Regular', fontSize: 12, color: Colors.black, marginTop: 5 },
    txtRead: { fontFamily: 'Roboto-Regular', fontSize: 12, color: Colors.placeholder },
    txt: { p: { fontFamily: 'Roboto-Regular', fontSize: 13, color: Colors.black } }
});
