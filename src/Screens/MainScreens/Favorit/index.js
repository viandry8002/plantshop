import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  RefreshControl,
  ScrollView,
} from 'react-native';
import React, {useState} from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import Money from '../../Components/Money';
import MasonryList from '@react-native-seoul/masonry-list';

const dataProduct = [
  {
    id: 1,
    image: require('../../../Assets/images/tanaman.png'),
    title: 'Calathea Orbifolia',
    price: '20000',
  },
  {
    id: 2,
    image: require('../../../Assets/images/tanaman.png'),
    title: 'Calathea Orbifolia',
    price: '20000',
    disc: '50%',
    priceFrom: '40000',
  },
  {
    id: 3,
    image: require('../../../Assets/images/tanaman.png'),
    title: 'Calathea Orbifolia',
    price: '20000',
  },
  {
    id: 4,
    image: require('../../../Assets/images/tanaman.png'),
    title: 'Calathea Orbifolia',
    price: '20000',
  },
  {
    id: 5,
    image: require('../../../Assets/images/tanaman.png'),
    title: 'Calathea Orbifolia',
    price: '20000',
    disc: '50%',
    priceFrom: '40000',
  },
  {
    id: 7,
    image: require('../../../Assets/images/tanaman.png'),
    title: 'Calathea Orbifolia',
    price: '20000',
  },
  {
    id: 8,
    image: require('../../../Assets/images/tanaman.png'),
    title: 'Calathea Orbifolia',
    price: '20000',
  },
  {
    id: 9,
    image: require('../../../Assets/images/tanaman.png'),
    title: 'Calathea Orbifolia',
    price: '20000',
  },
  {
    id: 10,
    image: require('../../../Assets/images/tanaman.png'),
    title: 'Calathea Orbifolia',
    price: '20000',
  },
];

const Favorite = () => {
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const renderProduct = ({item}) => {
    return (
      <TouchableOpacity
        style={[
          {
            flex: 0.5,
            margin: 4,
            borderRadius: 15,
            backgroundColor: Colors.white,
          },
          item.disc ? {height: 242} : {height: 220},
        ]}
        onPress={() => {}}>
        <Image
          source={item.image}
          style={{
            width: '100%',
            height: 166,
            borderRadius: 15,
          }}
          resizeMode={'contain'}
        />
        {item.disc ? (
          <View
            style={{
              position: 'absolute',
              backgroundColor: Colors.error,
              width: 70,
              alignItems: 'center',
              borderTopLeftRadius: 15,
              borderBottomRightRadius: 15,
            }}>
            <Text style={[Font.reguler12, {color: Colors.white}]}>
              {item.disc} Disc
            </Text>
          </View>
        ) : (
          false
        )}
        <View style={{padding: 8, justifyContent: 'space-between', flex: 1}}>
          <Text style={Font.reguler12} numberOfLines={1}>
            {item.title}
          </Text>
          <Money
            value={item.price}
            styling={[Font.bold14, {color: Colors.primary}]}
          />
          {item.disc ? (
            <Money
              value={item.priceFrom}
              styling={[
                Font.reguler12,
                {color: Colors.placeholder, textDecorationLine: 'line-through'},
              ]}
            />
          ) : (
            false
          )}
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            colors={[Colors.primary]}
          />
        }>
        <MasonryList
          data={dataProduct}
          numColumns={2}
          renderItem={renderProduct}
          style={{padding: 4}}
          showsVerticalScrollIndicator={false}
          ListFooterComponent={<View style={{height: 26}} />}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default Favorite;

const styles = StyleSheet.create({});
