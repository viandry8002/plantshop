import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    View,
    StatusBar,
    TouchableOpacity,
    Image,
    FlatList,
    RefreshControl,
    ScrollView,
    TextInput
} from 'react-native';
import React, { useState, useEffect } from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import Money from '../../Components/Money';
import MasonryList from '@react-native-seoul/masonry-list';
import { Api, Key } from '../../../Screens/Api';
import { screenWidth, screenHeight } from '../../../Variable';
import Icons from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceholder from 'react-native-shimmer-placeholder';
import HeaderTitleBack from '../../Components/HeaderTitleBack';

const SearchProduct = ({ navigation, route }) => {
    const [shimmer, setShimmer] = useState(false);
    const [tokens, setTokens] = useState('');
    const [refreshing, setRefreshing] = useState(false);
    const [dataProduct, setDataProduct] = useState([]);
    const [dataStore, setDataStore] = useState([]);
    const [tab, setTab] = useState(route?.params?.title == 'Product' ? 'Product' : 'Store');
    const [search, setSearch] = useState('');

    useEffect(() => {
        // get data login
        async function getDataLogin() {
            try {
                const token = await AsyncStorage.getItem('user_token')
                console.log('tokne', token);
                setTokens(token)
                getDataProduct(token)
                getDataStore(token)
            } catch (e) {
                console.log(e)
            }
        }
        getDataLogin();
    }, []);

    const onRefresh = () => {
        setRefreshing(true);
        setTimeout(() => {
            getDataProduct(tokens)
            getDataStore(tokens)
            setRefreshing(false);
        }, 2000);
    };

    const getDataProduct = (token, cari) => {
        setShimmer(true)
        // axios get
        axios.get(cari == undefined ? `${Api}/product` : `${Api}/product?title=${cari}`, {
            headers: {
                'Accept': 'application/json',
                'key': Key,
                'Authorization': 'Bearer ' + token,
            }
        }).then((res) => {
            console.log('result product', res.data);
            setDataProduct(res.data.data)
            setShimmer(false);
        }).catch((err) => {
            setShimmer(false);
            console.log('err product', err.response)
        })
    }

    const getDataStore = (token, cari) => {
        setShimmer(true)
        // axios get
        axios.get(cari == undefined ? `${Api}/store` : `${Api}/store?title=${cari}`, {
            headers: {
                'Accept': 'application/json',
                'key': Key,
                'Authorization': 'Bearer ' + token,
            }
        }).then((res) => {
            console.log('result Store', res.data);
            setDataStore(res.data.data)
            setShimmer(false);
        }).catch((err) => {
            setShimmer(false);
            console.log('err Store', err.response)
        })
    }

    const shimmerStore = () => {
        return (
            <View style={{ flexDirection: 'row', padding: 14 }}>
                <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: 80, height: 80, borderRadius: 15, marginRight: 8 }} />
                <View style={{ justifyContent: 'center' }}>
                    <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.6, height: 20, borderRadius: 7 }} />
                    <View style={{ borderBottomColor: Colors.container, borderBottomWidth: 1, marginVertical: 8 }} />
                    <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.4, height: 17, borderRadius: 7 }} />
                </View>
            </View>
        )
    }

    const shimmerProduct = () => {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 8 }}>
                <View
                    style={{
                        flex: 0.5,
                        margin: 4,
                        borderRadius: 15,
                        backgroundColor: Colors.white,
                        height: 220
                    }}>
                    <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '100%', height: 166, borderRadius: 15 }} />
                    <View style={{ padding: 8, justifyContent: 'space-between', flex: 1 }}>
                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '80%', height: 16, borderRadius: 7 }} />
                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '70%', height: 16, borderRadius: 7, marginTop: 8 }} />
                    </View>
                </View>
                <View
                    style={{
                        flex: 0.5,
                        margin: 4,
                        borderRadius: 15,
                        backgroundColor: Colors.white,
                        height: 220,
                        marginLeft: 8
                    }}>
                    <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '100%', height: 166, borderRadius: 15 }} />
                    <View style={{ padding: 8, flex: 1, paddingVertical: 5 }}>
                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '80%', height: 16, borderRadius: 7 }} />
                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '70%', height: 16, borderRadius: 7, marginTop: 8 }} />
                    </View>
                </View>
            </View>
        )
    }

    const renderProduct = ({ item }) => {
        return (
            <TouchableOpacity
                style={[
                    {
                        flex: 0.5,
                        margin: 4,
                        borderRadius: 15,
                        backgroundColor: Colors.white,
                        elevation: 5
                    },
                    item.discount_percent ? { height: 242 } : { height: 220 },
                ]}
                onPress={() => navigation.push('DtlProduk', { id: item.id, nav: true, tab: 'header' })}>
                <Image
                    source={item.file == null ? require('../../../Assets/images/tanaman.png') : { uri: item.file }}
                    style={{
                        width: '100%',
                        height: 166,
                        borderRadius: 15,
                    }}
                    resizeMode={'contain'}
                />
                {item.discount_percent ? (
                    <View
                        style={{
                            position: 'absolute',
                            backgroundColor: Colors.error,
                            width: 70,
                            alignItems: 'center',
                            borderTopLeftRadius: 15,
                            borderBottomRightRadius: 15,
                        }}>
                        <Text style={[Font.reguler12, { color: Colors.white }]}>
                            {item.discount_percent} Disc
                        </Text>
                    </View>
                ) : (
                    false
                )}
                <View style={{ padding: 8, justifyContent: 'space-between', flex: 1 }}>
                    <Text style={Font.reguler12} numberOfLines={1}>
                        {item.title}
                    </Text>
                    <Money
                        value={item.price_discount}
                        styling={[Font.bold14, { color: Colors.primary }]}
                    />
                    {item.discount_percent ? (
                        <Money
                            value={item.price}
                            styling={[
                                Font.reguler12,
                                { color: Colors.placeholder, textDecorationLine: 'line-through' },
                            ]}
                        />
                    ) : (
                        false
                    )}
                </View>
            </TouchableOpacity>
        );
    };

    const renderStore = ({ item }) => {
        return (
            <TouchableOpacity
                style={{
                    flex: 1,
                    flexDirection: 'row',
                    paddingHorizontal: 16,
                    paddingVertical: 12
                }}
                onPress={() => navigation.navigate('DtlToko', { id: item.id })}>
                <Image source={{ uri: item.file }} style={{ width: 80, height: 80, borderRadius: 15, marginRight: 8 }} />
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={Font.bold14}>{item.name}</Text>
                    <View
                        style={{
                            borderBottomColor: Colors.container,
                            borderBottomWidth: 1,
                            marginVertical: 8,
                        }}
                    />
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            source={require('../../../Assets/icons/Pin_fill.png')}
                            style={{ width: 24, height: 24 }}
                        />
                        <Text style={Font.reguler12}>{item?.city?.city_name}, {item?.province?.province_name}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
            <StatusBar translucent backgroundColor="transparent" barStyle='light-content' />
            {/* header */}
            <View style={styles.header}>
                <TouchableOpacity onPress={() => { navigation.goBack() }} style={{ marginRight: 20 }}>
                    <Icons name={'arrow-left'} size={28} color={Colors.white} />
                </TouchableOpacity>
                <View style={{
                    backgroundColor: Colors.white,
                    height: 40,
                    borderRadius: 100,
                    width: screenWidth - 90,
                    paddingHorizontal: 8,
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                    <Ionicons
                        name="search"
                        size={18}
                        color={Colors.placeholder}
                        style={{ marginRight: 8 }}
                        onPress={() => { }}
                    />
                    <TextInput value={search} onChangeText={(txt)=>{setSearch(txt), getDataProduct(tokens, txt), getDataStore(tokens, txt)}} placeholder='Cari “tanaman hias”' placeholderTextColor={Colors.placeholder} style={styles.input} />
                </View>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: Colors.white, elevation: 5 }}>
                <TouchableOpacity onPress={() => { setTab('Product') }} style={{ width: '50%', borderBottomWidth: 1, borderBottomColor: tab == 'Product' ? Colors.primary : 'transparent', paddingVertical: 14 }}>
                    <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 12, color: tab == 'Product' ? Colors.primary : Colors.black, textAlign: 'center' }}>Tanaman</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { setTab('Store') }} style={{ width: '50%', borderBottomWidth: 1, borderBottomColor: tab == 'Store' ? Colors.primary : 'transparent', paddingVertical: 14 }}>
                    <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 12, color: tab == 'Store' ? Colors.primary : Colors.black, textAlign: 'center' }}>Toko</Text>
                </TouchableOpacity>
            </View>
            <View style={{ paddingVertical: 8 }}>
                {tab == 'Product' ?
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        refreshControl={
                            <RefreshControl
                                refreshing={refreshing}
                                onRefresh={onRefresh}
                                colors={[Colors.primary]}
                            />
                        }>
                        {shimmer == true ? shimmerProduct() :
                            (dataProduct.length == 0 ?
                                <View style={{ alignItems: 'center', justifyContent: 'center', width: screenWidth * 1, height: screenHeight * 0.7 }}>
                                    <Image source={require('../../../Assets/images/empty.png')} style={{ width: 220, height: 144 }} />
                                    <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.gray1, marginTop: 16 }}>Tidak Ada Data</Text>
                                </View> :
                                <MasonryList
                                    data={dataProduct}
                                    numColumns={2}
                                    renderItem={renderProduct}
                                    style={{ padding: 8 }}
                                    showsVerticalScrollIndicator={false}
                                    ListFooterComponent={<View style={{ height: 26 }} />}
                                />)}
                    </ScrollView> :
                    shimmer == true ? shimmerStore() :
                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={refreshing}
                                    onRefresh={onRefresh}
                                    colors={[Colors.primary]}
                                />
                            }
                            data={dataStore}
                            renderItem={renderStore}
                            showsVerticalScrollIndicator={false}
                            ListEmptyComponent={() => {
                                return (
                                    <View style={{ alignItems: 'center', justifyContent: 'center', width: screenWidth * 1, height: screenHeight * 0.7 }}>
                                        <Image source={require('../../../Assets/images/empty.png')} style={{ width: 220, height: 144 }} />
                                        <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.gray1, marginTop: 16 }}>Tidak Ada Data</Text>
                                    </View>
                                )
                            }}
                        />}
            </View>
        </SafeAreaView>
    );
};

export default SearchProduct;

const styles = StyleSheet.create({
    header: {
        backgroundColor: Colors.primary, flexDirection: 'row', alignItems: 'center', height: 86, paddingTop: screenHeight * 0.033, paddingHorizontal: 16
    },
    input: { flex: 1, fontFamily: 'Roboto-Regular', fontSize: 12, color: Colors.black }
});
