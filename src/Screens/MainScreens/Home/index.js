import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState, useEffect } from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  RefreshControl,
} from 'react-native';
import { SliderBox } from 'react-native-image-slider-box';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import { screenWidth } from '../../../Variable';
import Money from '../../Components/Money';
import { Api, Key, AdMob } from '../../../Screens/Api';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import LinearGradient from 'react-native-linear-gradient';
import { useIsFocused } from "@react-navigation/native";
import { AdMobBanner } from 'react-native-admob';
import HTMLView from 'react-native-htmlview';
import { Badge } from '@rneui/themed';
import { screenHeight } from '../../../Variable';
import Octicons from 'react-native-vector-icons/Octicons';
import Ionicons from 'react-native-vector-icons/Ionicons';

const dataMenu = [

  {
    id: 2,
    image: require('../../../Assets/images/discount.png'),
    title: 'Produk Promo',
  },
  {
    id: 3,
    image: require('../../../Assets/images/favorit.png'),
    title: 'Terfavorit',
  },
  {
    id: 4,
    image: require('../../../Assets/images/rating.png'),
    title: 'Terlaris',
  },
  {
    id: 5,
    image: require('../../../Assets/images/plant.png'),
    title: 'Produk',
  },
  {
    id: 6,
    image: require('../../../Assets/images/news.png'),
    title: 'Sharing & Event',
  },
];

const dataTokoPilihan = [
  {
    id: 1,
    image: require('../../../Assets/images/tanaman.png'),
    logo: require('../../../Assets/images/toko.png'),
    title: 'Nama Toko dita dwi cahyani',
  },
  {
    id: 2,
    image: require('../../../Assets/images/tanaman.png'),
    logo: require('../../../Assets/images/toko.png'),
    title: 'Nama Toko',
  },
  {
    id: 3,
    image: require('../../../Assets/images/tanaman.png'),
    logo: require('../../../Assets/images/toko.png'),
    title: 'Nama Toko',
  },
  {
    id: 4,
    image: require('../../../Assets/images/tanaman.png'),
    logo: require('../../../Assets/images/toko.png'),
    title: 'Nama Toko',
  },
  {
    id: 5,
    image: require('../../../Assets/images/tanaman.png'),
    logo: require('../../../Assets/images/toko.png'),
    title: 'Nama Toko',
  },
];

const dataIklan = [
  {
    id: 1,
    image: require('../../../Assets/images/iklan.png'),
  },
  {
    id: 2,
    image: require('../../../Assets/images/iklan.png'),
  },
  {
    id: 3,
    image: require('../../../Assets/images/iklan.png'),
  },
  {
    id: 4,
    image: require('../../../Assets/images/iklan.png'),
  },
  {
    id: 5,
    image: require('../../../Assets/images/iklan.png'),
  },
];

const index = ({ navigation, route }) => {
  const [shimmer, setShimmer] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [tokens, setTokens] = useState('');
  const [dataTerlaris, setTerlaris] = useState([]);
  const [dataFavorit, setFavorit] = useState([]);
  const [dataEvent, setEvent] = useState([]);
  const [dataSharing, setSharing] = useState([]);
  const [sliders, setSliders] = useState([]);
  const [dataStore, setStore] = useState([]);
  const [countNotif, setCountNotif] = useState(0);
  const isFocused = useIsFocused();

  useEffect(() => {
    // get data login
    async function getDataLogin() {
      try {
        const token = await AsyncStorage.getItem('user_token')
        setTokens(token)
        getCountNotif(token)
        getHome(token)
      } catch (e) {
        console.log(e)
      }
    }
    getDataLogin();
    if (isFocused) {
      getHome(tokens)
    }
  }, [isFocused]);

  // console.log('routee1', route?.params);

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setShimmer(true)
      getHome(tokens)
      setRefreshing(false);
    }, 2000);
  };

  const getCountNotif = (token) => {
    // setShimmer(true)
    // axios get
    axios.get(`${Api}/notifications/count`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      console.log('result count', res.data);
      setCountNotif(res.data.count)
    }).catch((err) => {
      setShimmer(false);
      console.log('err count', err)
    })
  }

  const getHome = (token) => {
    // setShimmer(true)
    // axios get
    axios.get(`${Api}/home`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      console.log('result home', res.data);
      const dataSliders = res.data.slider.map((a) => {
        return (a.file)
      })
      setSliders(dataSliders)
      setStore(res.data.store)
      setTerlaris(res.data.terlaris)
      setFavorit(res.data.favorit)
      setEvent(res.data.event)
      setSharing(res.data.sharing)
      setShimmer(false);
    }).catch((err) => {
      setShimmer(false);
      console.log('err home', err.response)
    })
  }

  const renderMenu = ({ item }) => {
    return (
      <TouchableOpacity
        style={{
          // flex: 1,
          // width: screenWidth / 3,
          // height: 68,
          // marginVertical: 16,
          // alignItems: 'center',
          // backgroundColor: 'pink'
          marginVertical: 8, alignItems: 'center', width: '33.3%'
        }}
        onPress={() => { item.title == 'Terlaris' ? navigation.navigate('ListProduct', { title: 'Terlaris' }) : (item.title == 'Terfavorit' ? navigation.navigate('ListProduct', { title: 'Favorit' }) : (item.title == 'Sharing & Event' ? navigation.navigate('ListEventSharing', { title: 'Event & Sharing' }) : (item.title == 'Produk Promo' ? navigation.navigate('ListProduct', { title: 'Promo' }) : navigation.navigate('Produk')))) }}>
        <View
          style={{
            width: 48,
            height: 48,
            borderRadius: 15,
            backgroundColor: Colors.lightPrimary,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={item.image}
            style={{
              width: 36,
              height: 36,
            }}
          />
        </View>
        <View style={{ height: 8 }} />
        <Text style={[Font.caption, { textAlign: 'center' }]}>{item.title}</Text>
      </TouchableOpacity>
    );
  };

  const renderMenuShimmer = ({ item }) => {
    return (
      <View
        style={{
          flex: 1,
          width: screenWidth / 3,
          height: 68,
          marginVertical: 16,
          alignItems: 'center',
        }}>
        <ShimmerPlaceHolder
          LinearGradient={LinearGradient}
          style={{
            width: 48,
            height: 48,
            borderRadius: 15,
          }}
        />
        <View style={{ height: 8 }} />
      </View>
    );
  };

  const renderToko = ({ item }) => {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          width: 125,
          height: 179,
          marginHorizontal: 4,
          borderRadius: 15,
          marginVertical: 8,
          elevation: 2,
          backgroundColor: Colors.white,
        }}
        onPress={() => navigation.navigate('DtlToko', { id: item.id })}>
        <Image
          source={require('../../../Assets/images/tanaman.png')}
          style={{
            width: '100%',
            height: 105,
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
          }}
        />
        <Image
          source={{ uri: item.file }}
          style={{
            width: 56,
            height: 56,
            borderRadius: 100,
            position: 'absolute',
            alignSelf: 'center',
            top: 75,
          }}
        />
        <View style={{ marginTop: 40 }}>
          <Text style={[Font.bold12, { textAlign: 'center', marginHorizontal: 8 }]} numberOfLines={1}>
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const renderTokoShimmer = ({ item }) => {
    return (
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        style={{
          flex: 1,
          width: 125,
          height: 179,
          marginHorizontal: 4,
          borderRadius: 15,
          marginVertical: 8,
        }}
      />
    );
  };

  const renderIklan = ({ item }) => {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          width: 343,
          height: 125,
          marginHorizontal: 4,
          borderRadius: 15,
          marginVertical: 8,
          elevation: 2,
          backgroundColor: Colors.white,
        }}
        onPress={() => { }}>
        <Image
          source={item.image}
          style={{
            width: 343,
            height: 125,
            borderRadius: 15,
          }}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    );
  };

  const renderIklanShimmer = ({ item }) => {
    return (
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        style={{
          flex: 1,
          width: 343,
          height: 125,
          marginHorizontal: 4,
          borderRadius: 15,
          marginVertical: 8,
        }}
      />
    );
  };

  const renderFavorite = ({ item }) => {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          width: 166,
          height: 220,
          marginHorizontal: 4,
          borderRadius: 15,
          marginVertical: 8,
          elevation: 2,
          backgroundColor: Colors.white,
        }}
        onPress={() => {
          navigation.navigate('DtlProduk', { id: item.id, nav: true });
        }}>
        <Image
          source={{ uri: item.file }}
          style={{
            width: 166,
            height: 166,
            borderRadius: 15,
          }}
          resizeMode={'contain'}
        />
        <View style={{ padding: 8 }}>
          <Text style={Font.reguler12} numberOfLines={1}>
            {item.title}
          </Text>
          <Money
            value={item.price}
            styling={[Font.bold16, { color: Colors.primary }]}
          />
        </View>
      </TouchableOpacity>
    );
  };

  const renderTerlaris = ({ item }) => {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          width: 166,
          height: 220,
          marginHorizontal: 4,
          borderRadius: 15,
          marginVertical: 8,
          elevation: 2,
          backgroundColor: Colors.white,
        }}
        onPress={() => {
          navigation.navigate('DtlProduk', { id: item.id, nav: true });
        }}>
        <Image
          source={{ uri: item.file }}
          style={{
            width: 166,
            height: 166,
            borderRadius: 15,
          }}
          resizeMode={'contain'}
        />
        <View style={{ padding: 8 }}>
          <Text style={Font.reguler12} numberOfLines={1}>
            {item.title}
          </Text>
          <Money
            value={item.price}
            styling={[Font.bold16, { color: Colors.primary }]}
          />
        </View>
      </TouchableOpacity>
    );
  };

  const renderProductShimmer = ({ item }) => {
    return (
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        style={{
          flex: 1,
          width: 166,
          height: 220,
          marginHorizontal: 4,
          borderRadius: 15,
          marginVertical: 8,
        }}
      />
    );
  };

  const renderEvent = ({ item }) => {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          width: 289,
          height: 278,
          marginHorizontal: 4,
          borderRadius: 15,
          marginVertical: 8,
          elevation: 2,
          backgroundColor: Colors.white,
        }}
        onPress={() => navigation.navigate('DtlArtikel', { title: 'Event', id: item.id })}>
        <Image
          source={{ uri: item.file }}
          style={{
            width: 289,
            height: 162,
            borderRadius: 15,
          }}
          resizeMode={'contain'}
        />
        <View style={{ padding: 8, justifyContent: 'space-between', flex: 1 }}>
          <Text style={Font.bold14} numberOfLines={1}>
            {item.title}
          </Text>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image
              source={require('../../../Assets/icons/date.png')}
              style={{ width: 16, height: 16, marginRight: 8 }}
            />
            <Text style={[Font.caption, { color: Colors.placeholder }]}>
              {item?.start?.date + ' - ' + item?.end?.date}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image
              source={require('../../../Assets/icons/time.png')}
              style={{ width: 16, height: 16, marginRight: 8 }}
            />
            <Text style={[Font.caption, { color: Colors.placeholder }]}>
              {item?.start?.time + ' - ' + item?.end?.time}
            </Text>
          </View>
          <Text
            style={[Font.reguler12, { color: Colors.gray3 }]}
            numberOfLines={2}>
            {item.desc.replace(/\&nbsp;/g, '')}
          </Text>
          {/* <HTMLView
            value={"<div>" + item?.desc + "</div>"}
            stylesheet={styles.txt}
            numberOfLines={2}
          /> */}
        </View>
      </TouchableOpacity>
    );
  };

  const renderEventShimmer = ({ item }) => {
    return (
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        style={{
          flex: 1,
          width: 289,
          height: 278,
          marginHorizontal: 4,
          borderRadius: 15,
          marginVertical: 8,
        }}
      />
    );
  };

  const renderSharing = ({ item }) => {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          width: 289,
          height: 230,
          marginHorizontal: 4,
          borderRadius: 15,
          marginVertical: 8,
          elevation: 2,
          backgroundColor: Colors.white,
        }}
        onPress={() => navigation.navigate('DtlArtikel', { title: 'Sharing', id: item.id })}>
        <Image
          source={{ uri: item.file }}
          style={{
            width: 289,
            height: 162,
            borderRadius: 15,
          }}
          resizeMode={'contain'}
        />
        <View style={{ padding: 8 }}>
          <Text style={Font.bold14} numberOfLines={1}>
            {item.title}
          </Text>
          <Text
            style={[Font.reguler12, { color: Colors.gray3, marginTop: 5 }]}
            numberOfLines={2}>
            {item.desc.replace(/\&nbsp;/g, '')}
          </Text>
          {/* <HTMLView
            value={"<div>" + item?.desc + "</div>"}
            stylesheet={styles.txt}
            numberOfLines={2}
          /> */}
        </View>
      </TouchableOpacity>
    );
  };

  const renderSharingShimmer = ({ item }) => {
    return (
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        style={{
          flex: 1,
          width: 289,
          height: 230,
          marginHorizontal: 4,
          borderRadius: 15,
          marginVertical: 8,
        }}
      />
    );
  };

  const title = title => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginHorizontal: 16,
          marginVertical: 8,
        }}>
        <Text style={Font.bold16}>{title}</Text>
        {title == 'Toko Pilihan Untuk Kamu' ? null :
          <TouchableOpacity onPress={() => { title == 'Tanaman Terlaris' ? navigation.navigate('ListProduct', { title: 'Terlaris' }) : (title == 'Tanaman Favorite' ? navigation.navigate('ListProduct', { title: 'Favorit' }) : (title == 'Event Wan Flora' ? navigation.navigate('ListEventSharing', { title: 'Event' }) : navigation.navigate('ListEventSharing', { title: 'Sharing' }))) }} style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text style={[Font.bold12, { color: Colors.primary, marginRight: 6 }]}>
              Lihat Lainnya
            </Text>
            <AntDesign
              name="right"
              size={10}
              color={Colors.primary}
              onPress={() => { }}
            />
          </TouchableOpacity>}
      </View>
    );
  };

  const titleShimmer = () => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginHorizontal: 16,
          marginVertical: 8,
        }}>
        <ShimmerPlaceHolder
          LinearGradient={LinearGradient}
          style={{ width: 90, borderRadius: 100 }}
        />
        <ShimmerPlaceHolder
          LinearGradient={LinearGradient}
          style={{ width: 90, borderRadius: 100 }}
        />
      </View>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      <StatusBar backgroundColor={Colors.primary} barStyle="light-content" />
      {/* header */}
      <View style={{ backgroundColor: Colors.primary, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 14, height: route.params.navv !== undefined ? 60 : (route.params.nav1 !== undefined ? 80 : (route.params == undefined ? 80 : 60)), paddingTop: route.params.navv !== undefined ? screenHeight * 0.01 : (route?.params?.nav1 !== undefined ? screenHeight * 0.03 : (route?.params == undefined ? screenHeight * 0.03 : screenHeight * 0.01)) }}>
        <TouchableOpacity
          onPress={() => { navigation.navigate('SearchProduct', { title: 'Product' }) }}
          style={{
            backgroundColor: Colors.white,
            height: 35,
            borderRadius: 100,
            width: screenWidth - 90,
            paddingHorizontal: 8,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Ionicons
            name="search"
            size={18}
            color={Colors.placeholder}
            style={{ marginRight: 11 }}
            onPress={() => { }}
          />
          <Text style={Font.reguler12}>Cari “tanaman hias”</Text>
        </TouchableOpacity>
        <View style={{ marginRight: 16 }}>
          <Octicons
            name="bell-fill"
            size={20}
            color={Colors.white}
            // style={{marginLeft: 16}}
            onPress={() => navigation.navigate('Notification')}
          />
          {countNotif == 0 ? null :
            <Badge
              value={countNotif}
              containerStyle={{ position: 'absolute', top: -9, left: 10 }}
              badgeStyle={{
                backgroundColor: Colors.error,
                borderColor: Colors.white,
                borderWidth: 1,
              }}
            />}
        </View>
      </View>
      {/* body   */}
      {shimmer === true ? (
        <ScrollView showsVerticalScrollIndicator={false}>
          {/* slider */}
          <View style={{ alignItems: 'center' }}>
            <View style={{ paddingHorizontal: 16, height: 220, marginTop: 16 }}>
              <ShimmerPlaceHolder
                LinearGradient={LinearGradient}
                style={{
                  width: screenWidth - 32,
                  height: 194,
                  borderRadius: 15,
                }}
              />
            </View>
          </View>
          {/* menu */}
          <View style={{ height: 210 }}>
            <FlatList
              data={dataMenu}
              renderItem={renderMenuShimmer}
              numColumns={3}
              contentContainerStyle={{
                  // alignItems: 'center',
                  paddingHorizontal: 3,
                }}
            />
          </View>
          {/* toko pilihan */}
          {titleShimmer()}
          <View
            style={{
              height: 200,
            }}>
            <FlatList
              data={[{}, {}, {}, {}]}
              horizontal={true}
              renderItem={renderTokoShimmer}
              style={{ paddingLeft: 12 }}
              showsHorizontalScrollIndicator={false}
              ListFooterComponent={<View style={{ width: 26 }} />}
            />
          </View>
          {/* iklan */}
          <View
            style={{
              height: 150,
            }}>
            <FlatList
              data={[{}, {}, {}]}
              horizontal={true}
              renderItem={renderIklanShimmer}
              style={{ paddingLeft: 12 }}
              showsHorizontalScrollIndicator={false}
              ListFooterComponent={<View style={{ width: 26 }} />}
            />
          </View>
          {/* tanaman terlaris */}
          {titleShimmer()}
          <View
            style={{
              height: 240,
              backgroundColor: Colors.primary,
            }}>
            <FlatList
              data={[{}, {}, {}]}
              horizontal={true}
              renderItem={renderProductShimmer}
              style={{ paddingLeft: 12 }}
              showsHorizontalScrollIndicator={false}
              ListFooterComponent={<View style={{ width: 26 }} />}
            />
          </View>
          {/* Tanaman Favorite*/}
          {titleShimmer()}
          <View
            style={{
              height: 240,
            }}>
            <FlatList
              data={[{}, {}, {}]}
              horizontal={true}
              renderItem={renderProductShimmer}
              style={{ paddingLeft: 12 }}
              showsHorizontalScrollIndicator={false}
              ListFooterComponent={<View style={{ width: 26 }} />}
            />
          </View>
          {/* iklan */}
          <View
            style={{
              height: 150,
            }}>
            <FlatList
              data={[{}, {}, {}]}
              horizontal={true}
              renderItem={renderIklanShimmer}
              style={{ paddingLeft: 12 }}
              showsHorizontalScrollIndicator={false}
              ListFooterComponent={<View style={{ width: 26 }} />}
            />
          </View>
          {/* Event Plant Shop */}
          {titleShimmer()}
          <View
            style={{
              height: 300,
            }}>
            <FlatList
              data={[{}, {}, {}]}
              horizontal={true}
              renderItem={renderEventShimmer}
              style={{ paddingLeft: 12 }}
              showsHorizontalScrollIndicator={false}
              ListFooterComponent={<View style={{ width: 26 }} />}
            />
          </View>
          {/* Sharing */}
          {titleShimmer()}
          <View
            style={{
              height: 270,
            }}>
            <FlatList
              data={[{}, {}, {}]}
              horizontal={true}
              renderItem={renderSharingShimmer}
              style={{ paddingLeft: 12 }}
              showsHorizontalScrollIndicator={false}
              ListFooterComponent={<View style={{ width: 26 }} />}
            />
          </View>
        </ScrollView>
      ) : (
        <>
          <ScrollView
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
                colors={[Colors.primary]}
              />
            }>
            {/* slider */}
            <View style={{ alignItems: 'center' }}>
              <View style={{ paddingHorizontal: 16, height: 220, marginTop: 16 }}>
                <SliderBox
                  autoplay
                  circleLoop
                  images={sliders}
                  ImageComponentStyle={{
                    borderRadius: 15,
                    height: 194,
                    width: screenWidth - 32,
                  }}
                  paginationBoxStyle={{
                    alignSelf: 'flex-start',
                  }}
                  dotStyle={{
                    width: 8,
                    height: 8,
                    marginHorizontal: -20,
                    left: -20,
                  }}
                  dotColor={Colors.primary}
                  inactiveDotColor={Colors.container}
                  parentWidth={screenWidth - 32}
                />
              </View>
            </View>
            {/* menu */}
            <View style={{}}>
              <FlatList
                data={dataMenu}
                renderItem={renderMenu}
                numColumns={3}
                contentContainerStyle={{
                  // alignItems: 'center',
                  paddingHorizontal: 3,
                }} />
            </View>
            {/* admob */}
            <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
              <AdMobBanner
                adSize={'banner'}
                adUnitID={AdMob}
                testDevices={[AdMobBanner.simulatorId]}
                onAdFailedToLoad={error => console.error(error, 'err admob')}
              />
            </View>
            {/* toko pilihan */}
            {dataStore.length == 0 ? null :
              <>
                {title('Toko Pilihan Untuk Kamu')}
                <View
                  style={{
                    height: 200,
                  }}>
                  <FlatList
                    data={dataStore}
                    horizontal={true}
                    renderItem={renderToko}
                    style={{ paddingLeft: 12 }}
                    showsHorizontalScrollIndicator={false}
                    ListFooterComponent={<View style={{ width: 26 }} />}
                  />
                </View>
              </>}
            {/* iklan */}
            {/* {dataIklan.length == 0 ? null :
              <View
                style={{
                  height: 150,
                }}>
                <FlatList
                  data={dataIklan}
                  horizontal={true}
                  renderItem={renderIklan}
                  style={{ paddingLeft: 12 }}
                  showsHorizontalScrollIndicator={false}
                  ListFooterComponent={<View style={{ width: 26 }} />}
                />
              </View>} */}
            {/* tanaman terlaris */}
            {dataTerlaris.length == 0 ? null :
              <>
                {title('Tanaman Terlaris')}
                <View
                  style={{
                    height: 240,
                    backgroundColor: Colors.primary,
                  }}>
                  <FlatList
                    data={dataTerlaris}
                    horizontal={true}
                    renderItem={renderTerlaris}
                    style={{ paddingLeft: 12 }}
                    showsHorizontalScrollIndicator={false}
                    ListFooterComponent={<View style={{ width: 26 }} />}
                  />
                </View>
              </>}
            {/* Tanaman Favorite*/}
            {dataFavorit.length == 0 ? null :
              <>
                {title('Tanaman Favorite')}
                <View
                  style={{
                    height: 240,
                  }}>
                  <FlatList
                    data={dataFavorit}
                    horizontal={true}
                    renderItem={renderFavorite}
                    style={{ paddingLeft: 12 }}
                    showsHorizontalScrollIndicator={false}
                    ListFooterComponent={<View style={{ width: 26 }} />}
                  />
                </View>
              </>}

            {/* admob */}
            <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
              <AdMobBanner
                adSize={'banner'}
                adUnitID={AdMob}
                testDevices={[AdMobBanner.simulatorId]}
                onAdFailedToLoad={error => console.error(error, 'err admob')}
              />
            </View>
            {/* iklan */}
            {/* {dataIklan.length == 0 ? null :
              <View
                style={{
                  height: 150,
                }}>
                <FlatList
                  data={dataIklan}
                  horizontal={true}
                  renderItem={renderIklan}
                  style={{ paddingLeft: 12 }}
                  showsHorizontalScrollIndicator={false}
                  ListFooterComponent={<View style={{ width: 26 }} />}
                />
              </View>} */}
            {/* Event Plant Shop */}
            {dataEvent.length == 0 ? null :
              <>
                {title('Event Wan Flora')}
                <View
                  style={{
                    height: 300,
                  }}>
                  <FlatList
                    data={dataEvent}
                    horizontal={true}
                    renderItem={renderEvent}
                    style={{ paddingLeft: 12 }}
                    showsHorizontalScrollIndicator={false}
                    ListFooterComponent={<View style={{ width: 26 }} />}
                  />
                </View>
              </>}
            {/* Sharing */}
            {dataSharing.length == 0 ? null :
              <>
                {title('Sharing')}
                <View
                  style={{
                    height: 270,
                  }}>
                  <FlatList
                    data={dataSharing}
                    horizontal={true}
                    renderItem={renderSharing}
                    style={{ paddingLeft: 12 }}
                    showsHorizontalScrollIndicator={false}
                    ListFooterComponent={<View style={{ width: 26 }} />}
                  />
                </View>
              </>}
          </ScrollView>
          {/* admob */}
          <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
            <AdMobBanner
              adSize={'banner'}
              adUnitID={AdMob}
              testDevices={[AdMobBanner.simulatorId]}
              onAdFailedToLoad={error => console.error(error, 'err admob')}
            />
          </View>
        </>
      )}
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  txt: { p: { fontFamily: 'Roboto-Regular', fontSize: 12, color: Colors.gray3 } }
});
