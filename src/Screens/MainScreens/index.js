import SplashScreen from './SplashScreen';
import OnboardAuth from './OnboardAuth';
import Login from './Login';
import Register from './Register';
import Home from './Home';
import Produk from './Produk';
import Toko from './Toko';
import Feed from './Feed';
import Favorit from './Favorit';
import Profil from './Profil';
import DtlProduk from './/DtlProduk';
import DtlToko from './DtlToko';
import Notification from './Notification';
import DtlArtikel from './DtlArtikel';
import ListProduct from './ListProduct';
import ListEventSharing from './ListEventSharing';
import SearchProduct from './SearchProduct';
import ForgotPassword from './ForgotPassword';
import EditProfile from './Profil/EditProfile';

export {
  SplashScreen,
  OnboardAuth,
  Login,
  Register,
  Home,
  Produk,
  Toko,
  Feed,
  Favorit,
  Profil,
  DtlProduk,
  DtlToko,
  Notification,
  DtlArtikel,
  ListProduct,
  ListEventSharing,
  SearchProduct,
  ForgotPassword,
  EditProfile
};
