import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  RefreshControl,
  ScrollView,
  StatusBar,
  ActivityIndicator,
  Animated
} from 'react-native';
import React, { useState, useEffect } from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import Money from '../../Components/Money';
import MasonryList from '@react-native-seoul/masonry-list';
import { Api, Key, AdMob } from '../../../Screens/Api';
import { screenWidth, screenHeight } from '../../../Variable';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceholder from 'react-native-shimmer-placeholder';
import { Badge } from '@rneui/themed';
import Octicons from 'react-native-vector-icons/Octicons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { AdMobBanner } from 'react-native-admob';

const index = ({ navigation, route }) => {
  const [shimmer, setShimmer] = useState(false);
  const [tokens, setTokens] = useState('');
  const [chose, setChose] = useState('all');
  const [refreshing, setRefreshing] = useState(false);
  const [dataCategory, setDataCategory] = useState([]);
  const [dataProduct, setDataProduct] = useState([]);
  const [selectCategory, setSelectCategory] = useState('');
  const [countNotif, setCountNotif] = useState(0);
  // scrolled
  const [nextLink, setNextLink] = useState('');
  const [hasScrolled, sethasScrolled] = useState(false);
  const [loadMore, setLoadMore] = useState(false);
  const [pass, setPass] = useState(false);
  const [scrollY, setScrollY] = useState(new Animated.Value(0));

  useEffect(() => {
    // get data login
    async function getDataLogin() {
      try {
        const token = await AsyncStorage.getItem('user_token');
        setTokens(token)
        getCategory(token)
        getCountNotif(token)
        getDataProduct(token, selectCategory)
      } catch (e) {
        console.log(e)
      }
    }
    getDataLogin();
  }, []);

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setShimmer(true)
      getCategory(tokens)
      setSelectCategory('')
      setChose('all')
      getDataProduct(tokens)
      setRefreshing(false);
    }, 2000);
  };

  const getCategory = (token) => {
    setShimmer(true)
    // axios get
    axios.get(`${Api}/product/category`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      // console.log('result category', res.data);
      setDataCategory(res.data.data)
      // setShimmer(false);
    }).catch((err) => {
      setShimmer(false);
      console.log('err category', err.response)
    })
  }

  const getCountNotif = (token) => {
    // setShimmer(true)
    // axios get
    axios.get(`${Api}/notifications/count`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      console.log('result count', res.data);
      setCountNotif(res.data.count)
    }).catch((err) => {
      setShimmer(false);
      console.log('err count', err)
    })
  }

  const getDataProduct = (token, id) => {
    setShimmer(true)
    // axios get
    axios.get(id == undefined || selectCategory == '' ? `${Api}/product` : `${Api}/product?id_product_category=${id == undefined ? selectCategory : id}`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      console.log('result product', res.data);
      if (res.data.next_page_url === null) {
        (setPass(true), sethasScrolled(false))
      } else {
        sethasScrolled(true)
        setNextLink(res.data.next_page_url);
      }
      setDataProduct(res.data.data)
      setShimmer(false);
      // setIsLoading(false)
    }).catch((err) => {
      setShimmer(false);
      // setIsLoading(false)
      console.log('err product', err.response)
    })
  }

  const seen = new Set();
  const filteredArr = dataProduct?.filter(el => {
    const duplicate = seen.has(el.id);
    seen.add(el.id);
    return !duplicate;
  });

  const isCloseToBottom = (data) => {
    // console.log(data, 'isssssss');
    const paddingToBottom = 20
    return data?.layoutMeasurement.height + data?.contentOffset.y >=
      data?.contentSize.height - paddingToBottom
  }

  const renderFooter = () => {
    return (
      hasScrolled === true ?
        <ActivityIndicator size={'large'} animating /> : null
    )
  }

  const handleLoadMore = () => {
    if (loadMore) {
      axios.get(`${nextLink}`, {
        headers: {
          'Accept': 'application/json',
          'key': Key,
          'Authorization': 'Bearer ' + tokens,
        }
      }).then(res => {
        console.log('res2', res.data.data);
        sethasScrolled(true);
        setDataProduct([...dataProduct, ...res.data.data]);
        setNextLink(res.data.next_page_url);

        res.data.next_page_url === null ?
          (sethasScrolled(false), setPass(true)) :
          false;
      }).catch(err => {
        console.log(err);
      });
      return
    }
    setLoadMore(true)
  }


  const shimmerFilter = () => {
    return (
      <View style={{ justifyContent: 'center', height: 45, marginLeft: 8 }}>
        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.18, height: 30, borderRadius: 30 }} />
      </View>
    )
  }

  const shimmerProduct = () => {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 8 }}>
        <View
          style={{
            flex: 0.5,
            margin: 4,
            borderRadius: 15,
            backgroundColor: Colors.white,
            height: 220
          }}>
          <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '100%', height: 166, borderRadius: 15 }} />
          <View style={{ padding: 8, justifyContent: 'space-between', flex: 1 }}>
            <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '80%', height: 16, borderRadius: 7 }} />
            <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '70%', height: 16, borderRadius: 7, marginTop: 8 }} />
          </View>
        </View>
        <View
          style={{
            flex: 0.5,
            margin: 4,
            borderRadius: 15,
            backgroundColor: Colors.white,
            height: 220,
            marginLeft: 8
          }}>
          <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '100%', height: 166, borderRadius: 15 }} />
          <View style={{ padding: 8, flex: 1, paddingVertical: 5 }}>
            <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '80%', height: 16, borderRadius: 7 }} />
            <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '70%', height: 16, borderRadius: 7, marginTop: 8 }} />
          </View>
        </View>
      </View>
    )
  }

  const renderFilter = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => { setChose(item.title), setSelectCategory(item.id), getDataProduct(tokens, item.id) }}
        style={chose === item.title ? styles.wrapFilterActive : styles.wrapFilter}>
        {chose === item.title ? (
          <Text style={[Font.reguler12, { color: Colors.white }]}>
            {item.title}
          </Text>
        ) : (
          <Text style={Font.reguler12}>{item.title}</Text>
        )}
      </TouchableOpacity>
    );
  };

  const renderProduct = ({ item }) => {
    return (
      <TouchableOpacity
        style={[
          {
            flex: 0.5,
            margin: 4,
            borderRadius: 15,
            backgroundColor: Colors.white,
          },
          item.discount_percent ? { height: 242 } : { height: 220 },
        ]}
        onPress={() => navigation.navigate('DtlProduk', { id: item.id, nav: true })}>
        <Image
          source={item.file == null ? require('../../../Assets/images/tanaman.png') : { uri: item.file }}
          style={{
            width: '100%',
            height: 166,
            borderRadius: 15,
          }}
          resizeMode={'contain'}
        />
        {item.discount_percent ? (
          <View
            style={{
              position: 'absolute',
              backgroundColor: Colors.error,
              width: 70,
              alignItems: 'center',
              borderTopLeftRadius: 15,
              borderBottomRightRadius: 15,
            }}>
            <Text style={[Font.reguler12, { color: Colors.white }]}>
              {item.discount_percent}% Disc
            </Text>
          </View>
        ) : (
          false
        )}
        <View style={{ padding: 8, justifyContent: 'space-between', flex: 1 }}>
          <Text style={Font.reguler12} numberOfLines={1}>
            {item.title}
          </Text>
          <Money
            value={item.price_discount == '0' ? item.price : item.price_discount}
            styling={[Font.bold14, { color: Colors.primary }]}
          />
          {item.discount_percent ? (
            <Money
              value={item.price}
              styling={[
                Font.reguler12,
                { color: Colors.placeholder, textDecorationLine: 'line-through' },
              ]}
            />
          ) : (
            false
          )}
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StatusBar backgroundColor={Colors.primary} barStyle="light-content" />
      {/* header */}
      <View style={{ backgroundColor: Colors.primary, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 14, height: route.params.navv !== undefined ? 60 : (route.params.nav1 !== undefined ? 80 : (route.params == undefined ? 80 : 60)), paddingTop: route.params.navv !== undefined ? screenHeight * 0.01 : (route?.params?.nav1 !== undefined ? screenHeight * 0.03 : (route?.params == undefined ? screenHeight * 0.03 : screenHeight * 0.01)) }}>
        <TouchableOpacity
          onPress={() => { navigation.navigate('SearchProduct', { title: 'Product' }) }}
          style={{
            backgroundColor: Colors.white,
            height: 35,
            borderRadius: 100,
            width: screenWidth - 90,
            paddingHorizontal: 8,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Ionicons
            name="search"
            size={18}
            color={Colors.placeholder}
            style={{ marginRight: 11 }}
            onPress={() => { }}
          />
          <Text style={Font.reguler12}>Cari “tanaman hias”</Text>
        </TouchableOpacity>
        <View style={{ marginRight: 16 }}>
          <Octicons
            name="bell-fill"
            size={20}
            color={Colors.white}
            // style={{marginLeft: 16}}
            onPress={() => navigation.navigate('Notification')}
          />
          {countNotif == 0 ? null :
            <Badge
              value={countNotif}
              containerStyle={{ position: 'absolute', top: -9, left: 10 }}
              badgeStyle={{
                backgroundColor: Colors.error,
                borderColor: Colors.white,
                borderWidth: 1,
              }}
            />}
        </View>
      </View>
      {/* body */}
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            colors={[Colors.primary]}
          />
        }
        scrollEventThrottle={16}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }]
        )}
        onMomentumScrollEnd={({ nativeEvent }) => {
          // console.log('eventnn',nativeEvent);
          if (isCloseToBottom(nativeEvent)) {
            handleLoadMore()
          }
        }}>
        <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
          <AdMobBanner
            adSize={'banner'}
            adUnitID={AdMob}
            testDevices={[AdMobBanner.simulatorId]}
            onAdFailedToLoad={error => console.error(error, 'err admob')}
          />
        </View>
        <View
          style={{
            height: 46,
            backgroundColor: Colors.white,
            elevation: 3,
          }}>
          {shimmer == true ? shimmerFilter() :
            <FlatList
              data={dataCategory}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              renderItem={renderFilter}
              style={{ paddingLeft: 8 }}
              ListHeaderComponent={
                <TouchableOpacity
                  onPress={() => { setChose('all'), getDataProduct(tokens) }}
                  style={chose === 'all' ? styles.wrapFilterActive : styles.wrapFilter}>
                  {chose === 'all' ? (
                    <Text style={[Font.reguler12, { color: Colors.white }]}>
                      {'Semua'}
                    </Text>
                  ) : (
                    <Text style={Font.reguler12}>{'Semua'}</Text>
                  )}
                </TouchableOpacity>}
              ListFooterComponent={<View style={{ width: 8 }} />}
            />}
        </View>
        {shimmer == true ? shimmerProduct() :
          (dataProduct.length == 0 ?
            <View style={{ alignItems: 'center', justifyContent: 'center', width: screenWidth * 1, height: screenHeight * 0.7 }}>
              <Image source={require('../../../Assets/images/empty.png')} style={{ width: 220, height: 144 }} />
              <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.gray1, marginTop: 16 }}>Tidak Ada Data</Text>
            </View> :
            <MasonryList
              data={dataProduct}
              numColumns={2}
              renderItem={renderProduct}
              keyExtractor={(item, index) => index.toString()}
              style={{ padding: 4 }}
              showsVerticalScrollIndicator={false}
              ListFooterComponent={renderFooter}
            // ListFooterComponent={<View style={{ height: 26 }} />}
            />)}
      </ScrollView>
      <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
        <AdMobBanner
          adSize={'banner'}
          adUnitID={AdMob}
          testDevices={[AdMobBanner.simulatorId]}
          onAdFailedToLoad={error => console.error(error, 'err admob')}
        />
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  wrapFilter: {
    marginRight: 8,
    borderWidth: 1,
    borderColor: Colors.gray5,
    backgroundColor: Colors.container,
    justifyContent: 'center',
    height: 30,
    borderRadius: 100,
    flex: 1,
    marginVertical: 8,
    paddingHorizontal: 16,
  },
  wrapFilterActive: {
    marginRight: 8,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    height: 30,
    borderRadius: 100,
    flex: 1,
    marginVertical: 8,
    paddingHorizontal: 16,
  },
});
