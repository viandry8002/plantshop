import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, { useState, useEffect, useRef } from 'react';
import {
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    Image,
    ImageBackground,
    StatusBar,
    TouchableOpacity,
    Modal,
    ScrollView
} from 'react-native';
import { screenHeight, screenWidth } from '../../../Variable';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import BtnPrimary from '../../Components/BtnPrimary';
import SpinnerLoading from '../../Components/SpinnerLoading';
import Camera from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';
import Icons from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/Ionicons';
import { Input } from '@rneui/themed';
import { launchImageLibrary, launchCamera } from 'react-native-image-picker';
import { Modalize } from 'react-native-modalize';
import { Api, Key } from '../../Api';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import LinearGradient from 'react-native-linear-gradient';

const EditProfile = ({ navigation }) => {
    const [tokens, setTokens] = useState('');
    const [shimmer, setShimmer] = useState(false);
    const [loading, setLoading] = useState(false);
    const [detail, setDetail] = useState('');
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('password');
    const [confirmPass, setConfirmPass] = useState('password');
    //state image
    const [uploadImages, setUploadImages] = useState('');
    const [isFocused, setIsFocused] = useState(false);
    const [isFocused1, setIsFocused1] = useState(false);
    const [isFocused2, setIsFocused2] = useState(false);
    const [isFocused3, setIsFocused3] = useState(false);
    //modal
    const modalUpload = useRef(null);
    const [modalVisible, setModalVisible] = useState(false);

    useEffect(() => {
        // get data login
        async function getDataLogin() {
            try {
                const token = await AsyncStorage.getItem('user_token')
                console.log('tokne', token);
                setTokens(token)
                getData(token)
            } catch (e) {
                console.log(e)
            }
        }
        getDataLogin();
    }, []);

    //get data
    const getData = (token) => {
        setShimmer(true)
        // axios get
        axios.get(`${Api}/profile`, {
            headers: {
                'Accept': 'application/json',
                'key': Key,
                'Authorization': 'Bearer ' + token,
            }
        }).then((res) => {
            console.log('result detail', res.data);
            setDetail(res.data.data)
            setUsername(res.data.data.name)
            setEmail(res.data.data.email)
            setShimmer(false);
        }).catch((err) => {
            setShimmer(false);
            console.log('err detail', err)
        })
    }

    // onEdit
    const onEdit = () => {
        setLoading(true)
        // set form data
        let data = new FormData();
        data.append('name', username);
        data.append('email', email);
        data.append('password', pass == 'password' ? '' : pass);
        data.append('password_confirmation', confirmPass == 'password' ? '' : confirmPass);
        data.append('file', uploadImages == '' ? '' :
            {
                uri: uploadImages,
                name: 'file.jpg',
                type: 'image/jpg'
            })
        console.log(data, 'data');
        // axios post
        return axios.post(`${Api}/profile/edit`, data, {
            headers: {
                'Accept': 'application/json',
                'key': Key,
                'Authorization': 'Bearer ' + tokens,
                'Content-Type': 'multipart/form-data',
            }
        }).then((res) => {
            console.log('result edit', res.data);
            if (res.data.success == true) {
                setLoading(false)
                setModalVisible(true)
            } else if (res.data.message == 'Maksimal size gambar 2228kb!') {
                setLoading(false)
                alert('Maksimal size gambar 2228kb!');
            }

        }).catch((err) => {
            setLoading(false)
            console.log('err edit', err)
        })

    }

    // pick images
    const pickImageCamera = () => {
        let options = {
            title: 'Select Image',
            customButtons: [
                {
                    name: 'customOptionKey',
                    title: 'Choose Photo from Custom Option'
                },
            ],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        launchCamera(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log(
                    'User tapped custom button: ',
                    response.customButton
                );
                alert(response.customButton);
            } else {
                console.log(response.assets[0].uri)
                // imageProduct.push(response.assets[0].uri)
                setUploadImages(response.assets[0].uri)
            }
        });
    };

    // pick images galery
    const pickImageGalery = () => {
        let options = {
            title: 'Select Image',
            customButtons: [
                {
                    name: 'customOptionKey',
                    title: 'Choose Photo from Custom Option'
                },
            ],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log(
                    'User tapped custom button: ',
                    response.customButton
                );
                alert(response.customButton);
            } else {
                console.log(response.assets[0].uri)
                // imageProduct.push(response.assets[0].uri)
                setUploadImages(response.assets[0].uri)
            }
        });
    };

    const validation = () => {
        if(username == detail?.name && email == detail?.email && pass == 'password' && confirmPass == 'password' && uploadImages == ''){
            return true
        }else{
            return false
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
            <StatusBar backgroundColor={'transparent'} translucent barStyle={'light-content'} />
            {loading == true ? <SpinnerLoading spinner={true} /> : null}
            <ScrollView showsVerticalScrollIndicator={false}>
                <ImageBackground
                    source={require('../../../Assets/images/bg_profile.png')}
                    style={{
                        width: screenWidth,
                        height: screenHeight,
                    }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', top: '10%', left: '2%' }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }}>
                            <Icons name={'arrow-left'} size={25} color={Colors.white} />
                        </TouchableOpacity>
                        <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 16, color: Colors.white, marginLeft: 20 }}>Edit Profil</Text>
                    </View>
                    <View style={{ height: '15%' }} />
                    <View style={{ alignItems: 'center' }}>
                        {shimmer == true ? <ShimmerPlaceHolder LinearGradient={LinearGradient}
                            style={{
                                width: 104,
                                height: 104,
                                borderRadius: 15,
                            }}
                        /> : <Image
                            source={{ uri: uploadImages == '' ? detail.file : uploadImages }}
                            style={{
                                width: 104,
                                height: 104,
                                borderRadius: 15,
                            }}
                        />}
                        <View style={{ height: 10 }} />
                        <TouchableOpacity onPress={() => { modalUpload.current.open() }} style={{ marginLeft: 5 }}>
                            <Text style={[Font.reguler12, { color: Colors.primary }]}>Upload Foto </Text>
                        </TouchableOpacity>
                    </View>
                    {/* body */}
                    <View style={{ height: 10 }} />
                    <View style={{ paddingHorizontal: 16 }}>
                        <Input
                            value={username}
                            onChangeText={(text) => setUsername(text)}
                            placeholder={'Full Name'}
                            onFocus={() => { setIsFocused(true) }}
                            onBlur={() => { setIsFocused(false) }}
                            leftIcon={
                                <Image
                                    source={require('../../../Assets/icons/User.png')}
                                    style={{ width: 24, height: 24 }}
                                />
                            }
                            style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{
                                height: 48,
                                marginTop: 16,
                                borderWidth: 1,
                                borderRadius: 100,
                                borderColor: isFocused == true ? Colors.primary : Colors.gray,
                            }}
                        />
                        <Input
                            value={email}
                            onChangeText={(text) => setEmail(text)}
                            placeholder={'Email'}
                            keyboardType='email-address'
                            autoCapitalize='none'
                            onFocus={() => { setIsFocused1(true) }}
                            onBlur={() => { setIsFocused1(false) }}
                            leftIcon={
                                <Image
                                    source={require('../../../Assets/icons/Message.png')}
                                    style={{ width: 24, height: 24 }}
                                />
                            }
                            style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{
                                height: 48,
                                marginTop: 16,
                                borderWidth: 1,
                                borderRadius: 100,
                                borderColor: isFocused1 == true ? Colors.primary : Colors.gray,
                            }}
                        />
                        <Input
                            value={pass}
                            onChangeText={(text) => setPass(text)}
                            keyboardType='default'
                            placeholder={'Password'}
                            onFocus={() => { setIsFocused2(true) }}
                            onBlur={() => { setIsFocused2(false) }}
                            secureTextEntry={true}
                            leftIcon={
                                <Image
                                    source={require('../../../Assets/icons/Lock.png')}
                                    style={{ width: 24, height: 24 }}
                                />
                            }
                            style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{
                                height: 48,
                                marginTop: 16,
                                borderWidth: 1,
                                borderRadius: 100,
                                borderColor: isFocused2 == true ? Colors.primary : Colors.gray,
                            }}
                        />
                        <Input
                            value={confirmPass}
                            onChangeText={(text) => setConfirmPass(text)}
                            keyboardType='default'
                            placeholder={'Konfirmasi Password'}
                            onFocus={() => { setIsFocused3(true) }}
                            onBlur={() => { setIsFocused3(false) }}
                            secureTextEntry={true}
                            leftIcon={
                                <Image
                                    source={require('../../../Assets/icons/Lock.png')}
                                    style={{ width: 24, height: 24 }}
                                />
                            }
                            style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{
                                height: 48,
                                marginTop: 16,
                                borderWidth: 1,
                                borderRadius: 100,
                                borderColor: isFocused3 == true ? Colors.primary : Colors.gray,
                            }}
                        />
                        <View style={{ height: 16 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <Icon name='information-circle-outline' size={16} color={Colors.black} />
                            <Text
                                style={{
                                    fontWeight: '400',
                                    fontSize: 11,
                                    color: Colors.placeholder,
                                    marginLeft: 8,
                                    fontFamily: 'Roboto-Regular'
                                }}>
                                Password setidaknya mengandung 8 karakter
                            </Text>
                        </View>
                        <View style={{ height: 16 }} />
                        <BtnPrimary title={'Simpan Perubahan'} pressed={() => { onEdit() }} disabled={validation()} />
                    </View>
                </ImageBackground>
            </ScrollView>
            {/* modal success */}
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Image source={require('../../../Assets/images/success.png')} style={{ width: 90, height: 90 }} />
                        <Text style={styles.titlemodal}>Berhasil</Text>
                        <Text style={styles.descmodal}>Profil anda berhasil di edit</Text>
                        {/* button */}
                        <View style={styles.containerbtnmodal}>
                            <TouchableOpacity onPress={() => { setModalVisible(false), navigation.goBack() }} style={styles.btnmodal}>
                                <Text style={styles.textmodal}>Ok</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
            {/* modal upload */}
            <Modalize
                modalHeight={140}
                modalStyle={styles.modalstyle}
                handleStyle={styles.handlestyle}
                closeOnOverlayTap={true}
                ref={modalUpload}>
                <View style={styles.containermodal}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                        <TouchableOpacity onPress={() => { modalUpload.current.close() }}>
                            <Icons name={'x'} size={24} color={Colors.gray2} style={{ marginBottom: 8, marginRight: 5 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => { pickImageCamera(), modalUpload.current.close() }} style={{ borderWidth: 1, borderColor: Colors.gray5, borderRadius: 8, paddingVertical: 5, width: '27%', marginRight: 16, alignItems: 'center' }}>
                            <Camera name='camera-retro' color={Colors.gray3} size={24} />
                            <Text style={{ fontFamily: 'Nunito-SemiBold', fontSize: 12, color: Colors.black, marginTop: 5 }}>Kamera</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { pickImageGalery(), modalUpload.current.close() }} style={{ borderWidth: 1, borderColor: Colors.gray5, borderRadius: 8, paddingVertical: 5, width: '27%', marginRight: 16, alignItems: 'center' }}>
                            <Entypo name='folder-images' color={Colors.gray3} size={24} />
                            <Text style={{ fontFamily: 'Nunito-SemiBold', fontSize: 12, color: Colors.black, marginTop: 5 }}>Galery</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modalize>

        </SafeAreaView>
    );
};

export default EditProfile;

const styles = StyleSheet.create({
    // modal
    centeredView: { flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: '#00000070' },
    modalView: { backgroundColor: Colors.white, borderRadius: 15, height: '28%', width: screenWidth * 0.8, shadowColor: "#000", shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.25, shadowRadius: 4, elevation: 5, paddingVertical: 16, paddingHorizontal: 16, alignItems: 'center' },
    titlemodal: { fontSize: 15, fontFamily: 'Roboto-Medium', lineHeight: 21, color: Colors.black, textAlign: 'center', marginTop: 5 },
    descmodal: { fontSize: 13, fontFamily: 'Roboto-Regular', lineHeight: 18, color: Colors.black, textAlign: 'center', marginTop: 5 },
    containerbtnmodal: { flexDirection: 'row', justifyContent: 'space-between', marginTop: 16 },
    btnmodal: { width: '90%', height: 36, borderWidth: 1, borderColor: Colors.primary, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.primary },
    textmodal: { fontSize: 12, fontFamily: 'Roboto-Medium', lineHeight: 18, color: Colors.white },
    // modalize
    modalstyle: { borderTopLeftRadius: 20, borderTopRightRadius: 20 },
    handlestyle: { backgroundColor: Colors.white, marginTop: 20, width: 40 },
    containermodal: { paddingHorizontal: 16, marginTop: 15 },
});
