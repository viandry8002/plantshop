import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  ImageBackground,
  StatusBar,
  TouchableOpacity,
  Modal,
  ScrollView
} from 'react-native';
import { Api, Key, AdMob } from '../../Api';
import { screenHeight, screenWidth } from '../../../Variable';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import SpinnerLoading from '../../Components/SpinnerLoading';
import BeforeLogin from '../../Components/BeforeLogin';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import LinearGradient from 'react-native-linear-gradient';
import { useIsFocused } from "@react-navigation/native";
import { AdMobBanner } from 'react-native-admob';
import OneSignal from 'react-native-onesignal';
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';

const index = ({ navigation }) => {
  const [tokens, setTokens] = useState('');
  const [loading, setLoading] = useState(false);
  const [shimmer, setShimmer] = useState(false);
  const [detail, setDetail] = useState('');
  const [modalLogoutVisible, setModalLogoutVisible] = useState(false);
  const isFocused = useIsFocused();

  useEffect(() => {
    // get data login
    async function getDataLogin() {
      try {
        const token = await AsyncStorage.getItem('user_token')
        console.log('token', token);
        setTokens(token)
        getData(token)
      } catch (e) {
        console.log(e)
      }
    }
    getDataLogin();
    if (isFocused) {
      getData(tokens)
    }
  }, [isFocused]);

  //get data
  const getData = (token) => {
    setShimmer(true)
    // axios get
    axios.get(`${Api}/profile`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      console.log('result detail', res.data);
      setDetail(res.data.data)
      setShimmer(false);
    }).catch((err) => {
      setShimmer(false);
      console.log('err detail', err)
    })
  }

  const onLogout = async () => {
    await GoogleSignin.configure({
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      webClientId: '354674280310-d86ufhpq1vmh740kns01vus97spfsgnp.apps.googleusercontent.com',
      androidClientId: '354674280310-dfg6ddv820r0fim9drkbsqpq9dvd6unv.apps.googleusercontent.com'
    });
    setLoading(true)
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      await AsyncStorage.removeItem('user_token')
      OneSignal.removeExternalUserId((results) => {
        // The results will contain push and email success statuses
        console.log('Results of removing external user id');
        console.log(results);
      })
      setLoading(false)
      setModalLogoutVisible(false)
      // reset navigation
      navigation.reset({
        index: 0,
        routes: [{ name: 'Login' }]
      })
    } catch (e) {
      try {
        await AsyncStorage.removeItem('user_token')
        OneSignal.removeExternalUserId((results) => {
          // The results will contain push and email success statuses
          console.log('Results of removing external user id');
          console.log(results);
        })
        setLoading(false)
        setModalLogoutVisible(false)
        // reset navigation
        navigation.reset({
          index: 0,
          routes: [{ name: 'Login' }]
        })
      } catch (e) {
        setLoading(false)
        setModalLogoutVisible(false)
        console.log(e)
      }
      setLoading(false)
    }
    setLoading(false)
  }

  const menu = (title, icon) => {
    return (
      <View style={{ paddingHorizontal: 16, paddingVertical: 8, backgroundColor: Colors.white }}>
        <TouchableOpacity onPress={() => { title == 'Log Out' ? setModalLogoutVisible(true) : (title == 'Favorit Saya' ? navigation.navigate('ListProduct', { title: 'Favorit' }) : null) }} style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image
            source={icon}
            style={{ width: 24, height: 24, marginRight: 8 }}
          />
          <Text style={Font.reguler12}>{title}</Text>
        </TouchableOpacity>
        <View
          style={{
            borderBottomColor: Colors.container,
            borderBottomWidth: 1,
            marginVertical: 8,
          }}
        />
      </View>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      <StatusBar backgroundColor={Colors.primary} barStyle="light-content" />
      {loading == true ? <SpinnerLoading spinner={true} /> : null}
      {tokens !== null ?
        <>
          <ScrollView showsVerticalScrollIndicator={false}>
            <ImageBackground
              source={require('../../../Assets/images/bg_profile.png')}
              style={{
                width: screenWidth,
                height: screenHeight,
              }}>
              <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 16, color: Colors.white, top: '5%', left: '5%' }}>Profil</Text>
              <View style={{ height: '15%' }} />
              <View style={{ alignItems: 'center' }}>
                {shimmer == true ? <ShimmerPlaceHolder LinearGradient={LinearGradient}
                  style={{
                    width: 104,
                    height: 104,
                    borderRadius: 15,
                  }}
                /> :
                  <Image
                    source={{ uri: detail?.file }}
                    style={{
                      width: 104,
                      height: 104,
                      borderRadius: 15,
                    }}
                  />}
                <View style={{ height: 10 }} />
                <TouchableOpacity onPress={() => { navigation.navigate('EditProfile') }} style={{ flexDirection: 'row' }}>
                  <Image
                    source={require('../../../Assets/icons/Edit_fill.png')}
                    style={{ width: 16, height: 16, marginRight: 8 }}
                  />
                  <Text style={[Font.reguler12, { color: Colors.primary }]}>
                    Edit Profile
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8, marginTop: 5 }}>
                <AdMobBanner
                  adSize={'banner'}
                  adUnitID={AdMob}
                  testDevices={[AdMobBanner.simulatorId]}
                  onAdFailedToLoad={error => console.error(error, 'err admob')}
                />
              </View>
              {/* body */}
              {/* <View style={{ height: 5 }} /> */}
              {menu('Favorit Saya', require('../../../Assets/icons/Favorite_fill.png'))}
              {/* {menu('Tentang Kami', require('../../../Assets/icons/people_fill.png'))} */}
              {/* {menu(
                'Hubungi Kami',
                require('../../../Assets/icons/Message_fill.png'),
              )} */}
              {/* {menu(
                'Syarat & Ketentuan',
                require('../../../Assets/icons/File_dock_fill.png'),
              )} */}
              {menu(
                'Kebijakan Privasi',
                require('../../../Assets/icons/Chield_check_fill.png'),
              )}
              {menu('Log Out', require('../../../Assets/icons/Sign_out_fill.png'))}

              <Modal
                animationType="slide"
                transparent={true}
                visible={modalLogoutVisible}
                onRequestClose={() => {
                  Alert.alert("Modal has been closed.");
                  setModalLogoutVisible(!modalLogoutVisible);
                }}>
                <View style={styles.centeredView}>
                  <View style={styles.modalView}>
                    <Text style={styles.titlemodal}>Yakin Keluar ?</Text>
                    <Text style={styles.descmodal}>Anda akan keluar dari aplikasi</Text>
                    {/* button */}
                    <View style={styles.containerbtnmodal}>
                      <TouchableOpacity onPress={() => setModalLogoutVisible(false)} style={styles.btncancelmodal}>
                        <Text style={styles.textcancelmodal}>Batal</Text>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => onLogout()} style={styles.btnyesmodal}>
                        <Text style={styles.textyesmodal}>Ya</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Modal>
            </ImageBackground>
          </ScrollView>
          <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
            <AdMobBanner
              adSize={'banner'}
              adUnitID={AdMob}
              testDevices={[AdMobBanner.simulatorId]}
              onAdFailedToLoad={error => console.error(error, 'err admob')}
            />
          </View>
        </> :
        <>
          <View style={{ backgroundColor: Colors.primary, width: screenWidth, height: 58 }}>
            <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 16, color: Colors.white, marginLeft: 25, paddingTop: 30 }}>Profil</Text>
          </View>
          <BeforeLogin navigation={navigation} />
        </>}

    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  // modal
  centeredView: { flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: '#00000070' },
  modalView: { backgroundColor: Colors.white, borderRadius: 15, height: 157, width: screenWidth * 0.7, shadowColor: "#000", shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.25, shadowRadius: 4, elevation: 5, paddingVertical: 16, paddingHorizontal: 16 },
  titlemodal: { fontSize: 14, fontFamily: 'Roboto-Medium', lineHeight: 21, color: Colors.black, textAlign: 'center' },
  descmodal: { fontSize: 12, fontFamily: 'Roboto-Regular', lineHeight: 18, color: Colors.black, textAlign: 'center', marginTop: 16 },
  containerbtnmodal: { flexDirection: 'row', justifyContent: 'space-between', marginTop: 16 },
  btncancelmodal: { width: 100, height: 36, borderWidth: 1, borderColor: Colors.error, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.white },
  textcancelmodal: { fontSize: 12, fontFamily: 'Roboto-Medium', lineHeight: 18, color: Colors.error },
  btnyesmodal: { width: 100, height: 36, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.primary },
  textyesmodal: { fontSize: 12, fontFamily: 'Roboto-Medium', lineHeight: 18, color: Colors.white },
});
