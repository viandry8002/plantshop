import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    View,
    StatusBar,
    TouchableOpacity,
    Image,
    RefreshControl,
    ScrollView,
    Animated,
    ActivityIndicator
} from 'react-native';
import React, { useState, useEffect } from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import Money from '../../Components/Money';
import MasonryList from '@react-native-seoul/masonry-list';
import { Api, Key, AdMob } from '../../../Screens/Api';
import { screenWidth, screenHeight } from '../../../Variable';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceholder from 'react-native-shimmer-placeholder';
import HeaderTitleBack from '../../Components/HeaderTitleBack';
import { AdMobBanner } from 'react-native-admob';

const ListProduct = ({ navigation, route }) => {
    const [shimmer, setShimmer] = useState(true);
    const [tokens, setTokens] = useState('');
    const [chose, setChose] = useState('all');
    const [refreshing, setRefreshing] = useState(false);
    const [dataCategory, setDataCategory] = useState([]);
    const [dataProduct, setDataProduct] = useState([]);
    // scrolled
    const [nextLink, setNextLink] = useState('');
    const [hasScrolled, sethasScrolled] = useState(false);
    const [loadMore, setLoadMore] = useState(false);
    const [pass, setPass] = useState(false);
    const [scrollY, setScrollY] = useState(new Animated.Value(0));

    useEffect(() => {
        // get data login
        async function getDataLogin() {
            try {
                const token = await AsyncStorage.getItem('user_token')
                console.log('tokne', token);
                setTokens(token)
                getDataProduct(token)
            } catch (e) {
                console.log(e)
            }
        }
        getDataLogin();
    }, []);

    const onRefresh = () => {
        setRefreshing(true);
        setTimeout(() => {
            setShimmer(true)
            getDataProduct(tokens)
            setRefreshing(false);
        }, 2000);
    };

    const getDataProduct = (token) => {
        // setShimmer(true)
        // axios get
        axios.get(route?.params?.title == 'Terlaris' ? `${Api}/product?terlaris=1` : (route?.params?.title == 'Favorit' ? `${Api}/product?favorit=1` : `${Api}/product?discount=1`), {
            headers: {
                'Accept': 'application/json',
                'key': Key,
                'Authorization': 'Bearer ' + token,
            }
        }).then((res) => {
            console.log('result product', res.data);
            if (res.data.next_page_url === null) {
                (setPass(true), sethasScrolled(false))
            } else {
                sethasScrolled(true)
                setNextLink(res.data.next_page_url);
            }
            setDataProduct(res.data.data)
            setShimmer(false);
        }).catch((err) => {
            setShimmer(false);
            console.log('err product', err.response)
        })
    }

    const isCloseToBottom = (data) => {
        // console.log(data, 'isssssss');
        const paddingToBottom = 20
        return data?.layoutMeasurement.height + data?.contentOffset.y >=
            data?.contentSize.height - paddingToBottom
    }

    const renderFooter = () => {
        return (
            hasScrolled === true ?
                <ActivityIndicator size={'large'} animating /> : null
        )
    }

    const handleLoadMore = () => {
        if (loadMore) {
            axios.get(`${nextLink}`, {
                headers: {
                    'Accept': 'application/json',
                    'key': Key,
                    'Authorization': 'Bearer ' + tokens,
                }
            }).then(res => {
                console.log('res2', res.data.data);
                sethasScrolled(true);
                setDataProduct([...dataProduct, ...res.data.data]);
                setNextLink(res.data.next_page_url);

                res.data.next_page_url === null ?
                    (sethasScrolled(false), setPass(true)) :
                    false;
            }).catch(err => {
                console.log(err);
            });
            return
        }
        setLoadMore(true)
    }

    const shimmerProduct = () => {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 8 }}>
                <View
                    style={{
                        flex: 0.5,
                        margin: 4,
                        borderRadius: 15,
                        backgroundColor: Colors.white,
                        height: 220
                    }}>
                    <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '100%', height: 166, borderRadius: 15 }} />
                    <View style={{ padding: 8, justifyContent: 'space-between', flex: 1 }}>
                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '80%', height: 16, borderRadius: 7 }} />
                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '70%', height: 16, borderRadius: 7, marginTop: 8 }} />
                    </View>
                </View>
                <View
                    style={{
                        flex: 0.5,
                        margin: 4,
                        borderRadius: 15,
                        backgroundColor: Colors.white,
                        height: 220,
                        marginLeft: 8
                    }}>
                    <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '100%', height: 166, borderRadius: 15 }} />
                    <View style={{ padding: 8, flex: 1, paddingVertical: 5 }}>
                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '80%', height: 16, borderRadius: 7 }} />
                        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: '70%', height: 16, borderRadius: 7, marginTop: 8 }} />
                    </View>
                </View>
            </View>
        )
    }

    const renderProduct = ({ item }) => {
        return (
            <TouchableOpacity
                style={[
                    {
                        flex: 0.5,
                        margin: 4,
                        borderRadius: 15,
                        backgroundColor: Colors.white,
                        elevation: 5
                    },
                    item.discount_percent ? { height: 242 } : { height: 220 },
                ]}
                onPress={() => navigation.navigate('DtlProduk', { id: item.id, nav: true, tab: 'header' })}>
                <Image
                    source={item.file == null ? require('../../../Assets/images/tanaman.png') : { uri: item.file }}
                    style={{
                        width: '100%',
                        height: 166,
                        borderRadius: 15,
                    }}
                    resizeMode={'contain'}
                />
                {item.discount_percent ? (
                    <View
                        style={{
                            position: 'absolute',
                            backgroundColor: Colors.error,
                            width: 70,
                            alignItems: 'center',
                            borderTopLeftRadius: 15,
                            borderBottomRightRadius: 15,
                        }}>
                        <Text style={[Font.reguler12, { color: Colors.white }]}>
                            {item.discount_percent} Disc
                        </Text>
                    </View>
                ) : (
                    false
                )}
                <View style={{ padding: 8, justifyContent: 'space-between', flex: 1 }}>
                    <Text style={Font.reguler12} numberOfLines={1}>
                        {item.title}
                    </Text>
                    <Money
                        value={item.price_discount}
                        styling={[Font.bold14, { color: Colors.primary }]}
                    />
                    {item.discount_percent ? (
                        <Money
                            value={item.price}
                            styling={[
                                Font.reguler12,
                                { color: Colors.placeholder, textDecorationLine: 'line-through' },
                            ]}
                        />
                    ) : (
                        false
                    )}
                </View>
            </TouchableOpacity>
        );
    };

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
            <StatusBar translucent backgroundColor="transparent" barStyle='light-content' />
            <HeaderTitleBack title={route?.params?.title == 'Terlaris' ? 'Produk Terlaris' : (route?.params?.title == 'Favorit' ? 'Produk Favorit' : 'Produk Promo')} onPressBack={() => navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}
                scrollEventThrottle={16}
                onScroll={Animated.event(
                    [{ nativeEvent: { contentOffset: { y: scrollY } } }]
                )}
                onMomentumScrollEnd={({ nativeEvent }) => {
                    // console.log('eventnn',nativeEvent);
                    if (isCloseToBottom(nativeEvent)) {
                        handleLoadMore()
                    }
                }}>
                <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
                    <AdMobBanner
                        adSize={'banner'}
                        adUnitID={AdMob}
                        testDevices={[AdMobBanner.simulatorId]}
                        onAdFailedToLoad={error => console.error(error, 'err admob')}
                    />
                </View>
                {shimmer == true ? shimmerProduct() :
                    (dataProduct?.length == 0 ?
                        <View style={{ alignItems: 'center', justifyContent: 'center', width: screenWidth * 1, height: screenHeight * 0.7 }}>
                            <Image source={require('../../../Assets/images/empty.png')} style={{ width: 220, height: 144 }} />
                            <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.gray1, marginTop: 16 }}>Tidak Ada Data</Text>
                        </View> :
                        <MasonryList
                            data={dataProduct}
                            numColumns={2}
                            renderItem={renderProduct}
                            style={{ padding: 4 }}
                            showsVerticalScrollIndicator={false}
                            // ListFooterComponent={<View style={{ height: 26 }} />}
                            ListFooterComponent={renderFooter}
                        />)}
            </ScrollView>
            <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
                <AdMobBanner
                    adSize={'banner'}
                    adUnitID={AdMob}
                    testDevices={[AdMobBanner.simulatorId]}
                    onAdFailedToLoad={error => console.error(error, 'err admob')}
                />
            </View>
        </SafeAreaView>
    );
};

export default ListProduct;

const styles = StyleSheet.create({
    wrapFilter: {
        marginRight: 8,
        borderWidth: 1,
        borderColor: Colors.gray5,
        backgroundColor: Colors.container,
        justifyContent: 'center',
        height: 30,
        borderRadius: 100,
        flex: 1,
        marginVertical: 8,
        paddingHorizontal: 16,
    },
    wrapFilterActive: {
        marginRight: 8,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 30,
        borderRadius: 100,
        flex: 1,
        marginVertical: 8,
        paddingHorizontal: 16,
    },
});
