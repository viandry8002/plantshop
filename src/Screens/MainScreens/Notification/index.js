import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  RefreshControl,
  BackHandler,
  StatusBar
} from 'react-native';
import React, { useState, useEffect } from 'react';
import { Api, Key, AdMob } from '../../../Screens/Api';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import Horizontal from '../../Components/Horizontal';
import BeforeLogin from '../../Components/BeforeLogin';
import { screenWidth, screenHeight } from '../../../Variable';
import HeaderTitleBack from '../../Components/HeaderTitleBack';
import SpinnerLoading from '../../Components/SpinnerLoading';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceholder from 'react-native-shimmer-placeholder';
import { AdMobBanner } from 'react-native-admob';
import AntDesign from 'react-native-vector-icons/AntDesign';


const dataMenu = [
  {
    id: 1,
    image: require('../../../Assets/images/bell.png'),
    title: 'Promo Akhir Tahun',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Purus nibh malesuada mauris arcu, ut turpis ultrices. Dolor urna bibendum eget ut feugiat. Ligula est ullamcorper neque.',
    time: '1 Jam Lalu',
  },
  {
    id: 1,
    image: require('../../../Assets/images/logo_tanaman.png'),
    title: 'Toko Florist - Produk Baru!',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Purus nibh malesuada mauris arcu, ut turpis ultrices. Dolor urna bibendum eget ut feugiat. Ligula est ullamcorper neque.',
    time: '1 Jam Lalu',
  },
];

const index = ({ navigation }) => {
  const [refreshing, setRefreshing] = useState(false);
  const [shimmer, setShimmer] = useState(true);
  const [loading, setLoading] = useState(false);
  const [tokens, setTokens] = useState('');
  const [listData, setListData] = useState([]);
  // scrolled
  const [nextLink, setNextLink] = useState('');
  const [hasScrolled, sethasScrolled] = useState(false);
  const [pass, setPass] = useState(false);

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setShimmer(true)
      getNotif(tokens)
      setRefreshing(false);
    }, 2000);
  };

  useEffect(() => {
    // get data login
    async function getDataLogin() {
      try {
        const token = await AsyncStorage.getItem('user_token')
        console.log('tokne', token);
        setTokens(token)
        getNotif(token)
      } catch (e) {
        console.log(e)
      }
    }
    getDataLogin();
    const backAction = () => {
      navigation.goBack()
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, []);

  const getNotif = (token) => {
    // setShimmer(true)
    // axios get
    axios.get(`${Api}/notifications`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      console.log('result notif', res.data);
      if (res.data.next_page_url === null) {
        (setPass(true), sethasScrolled(false))
      } else {
        setNextLink(res.data.next_page_url);
      }
      setListData(res.data.data)
      setShimmer(false);
    }).catch((err) => {
      setShimmer(false);
      console.log('err notif', err.response)
    })
  }

  const onRead = (id, type, id_type) => {
    let data = {
      id_notification: id
    }
    setLoading(true)
    // axios post
    axios.post(`${Api}/notifications/read`, data, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + tokens,
      }
    }).then((res) => {
      console.log('result read', res.data);
      if (type == 'product') {
        navigation.navigate('DtlProduk', { id: id_type, notif: true, nav: true })
      } else if (type == 'events') {
        navigation.navigate('DtlArtikel', { title: 'Event', id: id_type, notif: true })
      } else {
        navigation.navigate('DtlArtikel', { title: 'Sharing', id: id_type, notif: true })
      }
      setLoading(false)
    }).catch((err) => {
      setLoading(false)
      console.log('err read', err.response)
    })
  }

  const seen = new Set();
  const filteredArr = listData?.filter(el => {
    const duplicate = seen.has(el.id);
    seen.add(el.id);
    return !duplicate;
  });

  // console.log(filteredArr?.filter(a => a.read == false && a.notification !== null), 'aaaaa');

  const renderFooter = () => {
    return (
      hasScrolled == true ?
        <ActivityIndicator size={'large'} animating /> : null
    )
  }

  // pass scrolled
  const onScroll = () => {
    sethasScrolled(true);
  };

  const handleLoadMore = () => {
    axios.get(`${nextLink}`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + tokens,
      }
    }).then(res => {
      console.log('res2', res.data.data);
      sethasScrolled(!hasScrolled);
      setListData([...listData, ...res.data.data]);
      setNextLink(res.data.next_page_url);

      res.data.next_page_url === null
        ? (sethasScrolled(!hasScrolled), setPass(true))
        : false;
    })
      .catch(err => {
        console.log(err, 'err next');
      });
  }

  const shimmerLoading = () => {
    return (
      <View style={{}}>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            padding: 16,
            alignItems: 'center'
          }}
          onPress={() => { }}>
          <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: 40, height: 40, borderRadius: 100 }} />

          <View style={{ flex: 1, justifyContent: 'center', marginLeft: 5 }}>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.5, height: 14, borderRadius: 7 }} />
              <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.2, height: 13, borderRadius: 7 }} />

            </View>
            <View style={{ height: 8 }} />
            <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.8, height: 14, borderRadius: 7, marginTop: 8 }} />
            <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.55, height: 14, borderRadius: 7, marginTop: 5 }} />
          </View>
        </TouchableOpacity>
        <Horizontal coloring={Colors.gray} />
      </View>
    )
  }

  const renderMenu = ({ item }) => {
    return (
      item?.notification == null ? null :
      <View style={{ flex: 1, backgroundColor: item?.read == false ? Colors.lightPrimary : Colors.white }}>
        <TouchableOpacity
          style={{
            flex: 1,
            flexDirection: 'row',
            padding: 16,
          }}
          onPress={() => { onRead(item?.id, item?.type, item?.id_type) }}>
          <Image
            source={item.type == 'product' ? { uri: item?.notification?.file } : require('../../../Assets/images/bell.png')}
            style={{ width: 40, height: 40, borderRadius: 100, marginRight: 8 }}
          />
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={[Font.bold12, { width: '80%' }]} numberOfLines={1}>{item?.notification?.title}</Text>
              <Text style={[Font.caption, { color: Colors.placeholder }]}>
                {item.time}
              </Text>
            </View>
            <View style={{ height: 8 }} />
            <Text style={Font.caption}>{item?.notification?.content.replace(/(<([^>]+)>)/ig, '')}</Text>
          </View>
        </TouchableOpacity>
        <Horizontal coloring={Colors.gray} />
      </View>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      <StatusBar backgroundColor={Colors.primary} barStyle={'light-content'}/>
      {/* header */}
      {/* <HeaderTitleBack title={'Notifikasi'} onPressBack={() =>{navigation.push('Home')}}/> */}
      <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: Colors.primary, paddingHorizontal: 16, width: screenWidth, height: 55, paddingTop: 8 }}>
        <TouchableOpacity onPress={() => { navigation.push('Home') }}>
          <AntDesign name='arrowleft' size={26} color={Colors.white} />
        </TouchableOpacity>
        <Text style={[Font.bold16, { color: Colors.white, marginLeft: 20 }]}>Notifikasi</Text>
      </View>
      {loading == true ? <SpinnerLoading spinner={true} /> : null}
      {tokens !== null ?
        (shimmer == true ? shimmerLoading() :
          <>
            <FlatList
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={onRefresh}
                  colors={[Colors.primary]}
                />
              }
              data={filteredArr}
              renderItem={renderMenu}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              onScroll={() => (pass === false ? onScroll() : false)}
              onEndReached={() => (nextLink === null ? false : handleLoadMore())}
              ListEmptyComponent={() => {
                return (
                  <View style={{ alignItems: 'center', justifyContent: 'center', width: screenWidth * 1, height: screenHeight * 0.7 }}>
                    <Image source={require('../../../Assets/images/empty.png')} style={{ width: 220, height: 144 }} />
                    <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.gray1, marginTop: 16 }}>Tidak Ada Data</Text>
                  </View>
                )
              }}
            />
            {/* admob */}
            <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
              <AdMobBanner
                adSize={'banner'}
                adUnitID={AdMob}
                testDevices={[AdMobBanner.simulatorId]}
                onAdFailedToLoad={error => console.error(error, 'err admob')}
              />
            </View>
          </>) :<BeforeLogin navigation={navigation} />}
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({});
