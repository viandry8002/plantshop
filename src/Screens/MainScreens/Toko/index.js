import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  ActivityIndicator,
  TextInput,
  Animated
} from 'react-native';
import React, { useState, useEffect, useRef } from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import { Api, Key, AdMob } from '../../../Screens/Api';
import { screenWidth, screenHeight } from '../../../Variable';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceholder from 'react-native-shimmer-placeholder';
import { AdMobBanner } from 'react-native-admob';
import Icons from 'react-native-vector-icons/Feather';
import { Modalize } from 'react-native-modalize';
import { Badge } from '@rneui/themed';
import Octicons from 'react-native-vector-icons/Octicons';
import Ionicons from 'react-native-vector-icons/Ionicons';

const datafilter = [
  {
    id: 1,
    title: 'Following',
  },
  {
    id: 2,
    title: 'Pilih Kota',
  },
];


const index = ({ navigation, route }) => {
  const [shimmer, setShimmer] = useState(false);
  const [tokens, setTokens] = useState('');
  const [chose, setChose] = useState('');
  const [refreshing, setRefreshing] = useState(false);
  const [dataStore, setDataStore] = useState([]);
  const modalFilter = useRef(null);
  const [dataFilter, setDataFilter] = useState([]);
  const [cari, setCari] = useState('');
  const [idCity, setIdCity] = useState(0);
  const [nameCity, setNameCity] = useState('');
  const [countNotif, setCountNotif] = useState(0);
  // scrolled
  const [nextLink, setNextLink] = useState('');
  const [hasScrolled, sethasScrolled] = useState(false);
  const [loadMore, setLoadMore] = useState(false);
  const [pass, setPass] = useState(false);
  const [scrollY, setScrollY] = useState(new Animated.Value(0));

  useEffect(() => {
    // get data login
    async function getDataLogin() {
      try {
        const token = await AsyncStorage.getItem('user_token')
        console.log('tokne', token);
        setTokens(token)
        getFilter(token)
        getCountNotif(token)
        getDataStore(token)
      } catch (e) {
        console.log(e)
      }
    }
    getDataLogin();
  }, []);

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getFilter()
      setNameCity('')
      // setIdCity(0)
      setCari('')
      getDataStore(tokens, chose)
      setRefreshing(false);
    }, 1000);
  };

  const getFilter = (name) => {
    if (name == undefined || cari == '') {
      // axios get
      axios.get(`${Api}/city`, {
        headers: {
          'Accept': 'application/json',
          'key': Key,
        }
      }).then((res) => {
        console.log('result city', res.data);
        setDataFilter(res.data.data)
        // setShimmer(false);
      }).catch((err) => {
        // setShimmer(false);
        console.log('err city', err.response)
      })
    } else {
      // axios get
      axios.get(`${Api}/city?city_name=${name == undefined ? cari : name}`, {
        headers: {
          'Accept': 'application/json',
          'key': Key,
        }
      }).then((res) => {
        console.log('result city', res.data);
        setDataFilter(res.data.data)
        // setShimmer(false);
      }).catch((err) => {
        // setShimmer(false);
        console.log('err city', err.response)
      })
    }
  }

  const getCountNotif = (token) => {
    // setShimmer(true)
    // axios get
    axios.get(`${Api}/notifications/count`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      console.log('result count', res.data);
      setCountNotif(res.data.count)
    }).catch((err) => {
      setShimmer(false);
      console.log('err count', err)
    })
  }

  const getDataStore = (token, params, id) => {
    console.log('looo', `/store?id_city=${id == undefined ? idCity : id}`);
    setShimmer(true)
    // axios get
    axios.get(params == undefined ? `${Api}/store?id_city=${id == undefined ? idCity : id}` : `${Api}/store?following=yes?id_city=${id == undefined ? idCity : id}`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      console.log('result store', res.data);
      if (res.data.next_page_url === null) {
        (setPass(true), sethasScrolled(false))
      } else {
        sethasScrolled(true)
        setNextLink(res.data.next_page_url);
      }
      setDataStore(res.data.data)
      setShimmer(false);
      setIsLoading(false)
    }).catch((err) => {
      setShimmer(false);
      setIsLoading(false)
      console.log('err store', err.response)
    })
  }

  const isCloseToBottom = (data) => {
    // console.log(data, 'isssssss');
    const paddingToBottom = 20
    return data?.layoutMeasurement.height + data?.contentOffset.y >=
      data?.contentSize.height - paddingToBottom
  }

  const renderFooter = () => {
    return (
      hasScrolled === true ?
        <ActivityIndicator size={'large'} animating /> : null
    )
  }

  const handleLoadMore = () => {
    if (loadMore) {
      axios.get(`${nextLink}`, {
        headers: {
          'Accept': 'application/json',
          'key': Key,
          'Authorization': 'Bearer ' + tokens,
        }
      }).then(res => {
        console.log('res2', res.data.data);
        sethasScrolled(true);
        setDataStore([...dataStore, ...res.data.data]);
        setNextLink(res.data.next_page_url);

        res.data.next_page_url === null ?
          (sethasScrolled(false), setPass(true)) :
          false;
      }).catch(err => {
        console.log(err);
      });
      return
    }
    setLoadMore(true)
  }

  const shimmerLoading = () => {
    return (
      <View style={{ flexDirection: 'row', padding: 16 }}>
        <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: 80, height: 80, borderRadius: 15, marginRight: 8 }} />
        <View style={{ justifyContent: 'center' }}>
          <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.6, height: 20, borderRadius: 7 }} />
          <View style={{ borderBottomColor: Colors.container, borderBottomWidth: 1, marginVertical: 8 }} />
          <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.4, height: 17, borderRadius: 7 }} />
        </View>
      </View>
    )
  }

  const renderFilter = ({ item }) => {
    return (
      item.title == 'Following' ?
        <TouchableOpacity
          onPress={() => {
            if (chose == 0) {
              chose === item.id ? setChose('') : setChose(item.id);
              getDataStore(tokens, item.id)
            } else {
              chose === item.id ? setChose('') : setChose(item.id);
              getDataStore(tokens)
            }
          }}
          style={chose === item.id ? styles.wrapFilterActive : styles.wrapFilter}>
          {chose === item.id ? (
            <Text style={[Font.reguler12, { color: Colors.white }]}>
              {item.title}
            </Text>
          ) : (
            <Text style={Font.reguler12}>{item.title}</Text>
          )}
        </TouchableOpacity> :
        <TouchableOpacity
          onPress={() => { modalFilter.current.open() }}
          style={[styles.wrapFilter, { flexDirection: 'row', alignItems: 'center' }]}>
          <Text style={Font.reguler12}>{nameCity == '' ? item.title : nameCity}</Text>
          <Icons name='chevron-down' size={20} color={Colors.gray1} style={{ marginLeft: 8 }} />
        </TouchableOpacity>
    );
  };

  const renderMenu = ({ item }) => {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          flexDirection: 'row',
          padding: 16,
        }}
        onPress={() => navigation.navigate('DtlToko', { id: item.id })}>
        <Image source={{ uri: item.file }} style={{ width: 80, height: 80, borderRadius: 15, marginRight: 8 }} />
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Text style={Font.bold14}>{item.name}</Text>
          <View
            style={{
              borderBottomColor: Colors.container,
              borderBottomWidth: 1,
              marginVertical: 8,
            }}
          />
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image
              source={require('../../../Assets/icons/Pin_fill.png')}
              style={{ width: 24, height: 24 }}
            />
            <Text style={Font.reguler12}>{item?.city?.city_name}, {item?.province?.province_name}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const renderViewFilter = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => { setIdCity(item.city_id), setNameCity(item.city_name), getDataStore(tokens, chose == '' ? null : chose, item.city_id), modalFilter.current.close() }} style={styles.cardFilter}>
        <Text style={styles.txtFilter}>{item.city_name}</Text>
      </TouchableOpacity>
    )
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      <StatusBar backgroundColor={Colors.primary} barStyle="light-content" />
      <>
        {/* header */}
        <View style={{ backgroundColor: Colors.primary, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 14, height: route.params.navv !== undefined ? 60 : (route.params.nav1 !== undefined ? 80 : (route.params == undefined ? 80 : 60)), paddingTop: route.params.navv !== undefined ? screenHeight * 0.01 : (route?.params?.nav1 !== undefined ? screenHeight * 0.03 : (route?.params == undefined ? screenHeight * 0.03 : screenHeight * 0.01)) }}>
          <TouchableOpacity
            onPress={() => { navigation.navigate('SearchProduct', { title: 'Store' }) }}
            style={{
              backgroundColor: Colors.white,
              height: 35,
              borderRadius: 100,
              width: screenWidth - 90,
              paddingHorizontal: 8,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Ionicons
              name="search"
              size={18}
              color={Colors.placeholder}
              style={{ marginRight: 11 }}
              onPress={() => { }}
            />
            <Text style={Font.reguler12}>Cari “tanaman hias”</Text>
          </TouchableOpacity>
          <View style={{ marginRight: 16 }}>
            <Octicons
              name="bell-fill"
              size={20}
              color={Colors.white}
              // style={{marginLeft: 16}}
              onPress={() => navigation.navigate('Notification')}
            />
            {countNotif == 0 ? null :
              <Badge
                value={countNotif}
                containerStyle={{ position: 'absolute', top: -9, left: 10 }}
                badgeStyle={{
                  backgroundColor: Colors.error,
                  borderColor: Colors.white,
                  borderWidth: 1,
                }}
              />}
          </View>
        </View>
        {/* body */}
        <ScrollView
          showsVerticalScrollIndicator={false}
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: scrollY } } }]
          )}
          onMomentumScrollEnd={({ nativeEvent }) => {
            // console.log('eventnn',nativeEvent);
            if (isCloseToBottom(nativeEvent)) {
              handleLoadMore()
            }
          }}>
          <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
            <AdMobBanner
              adSize={'banner'}
              adUnitID={AdMob}
              testDevices={[AdMobBanner.simulatorId]}
              onAdFailedToLoad={error => console.error(error, 'err admob')}
            />
          </View>
          <View
            style={{
              height: 46,
              backgroundColor: Colors.white,
              // elevation: 4,
            }}>
            <FlatList
              data={datafilter}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              renderItem={renderFilter}
              style={{ paddingLeft: 8 }}
              ListFooterComponent={<View style={{ width: 8 }} />}
            />
          </View>
          {shimmer == true ? shimmerLoading() :
            <View style={{ flex: 1 }}>
              <FlatList
                data={dataStore}
                renderItem={renderMenu}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                ListFooterComponent={renderFooter}
                ListEmptyComponent={() => {
                  return (
                    <View style={{ alignItems: 'center', justifyContent: 'center', width: screenWidth * 1, height: screenHeight * 0.7 }}>
                      <Image source={require('../../../Assets/images/empty.png')} style={{ width: 220, height: 144 }} />
                      <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.gray1, marginTop: 16 }}>Tidak Ada Data</Text>
                    </View>
                  )
                }}
              />
            </View>}
        </ScrollView>
        <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
          <AdMobBanner
            adSize={'banner'}
            adUnitID={AdMob}
            testDevices={[AdMobBanner.simulatorId]}
            onAdFailedToLoad={error => console.error(error, 'err admob')}
          />
        </View>
      </>
      {/* choose filter */}
      <Modalize
        // modalHeight={380}
        adjustToContentHeight={true}
        modalStyle={styles.modalstyle}
        handleStyle={styles.handlestyle}
        closeOnOverlayTap={true}
        ref={modalFilter}
        HeaderComponent={(
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 16, marginTop: 30 }}>
            <Text style={styles.titlemodal}>Pilih Kota</Text>
            <TouchableOpacity onPress={() => { modalFilter.current.close() }}>
              <Icons name={'x'} size={24} color={Colors.gray3} style={{ height: 30 }} />
            </TouchableOpacity>
          </View>
        )}>
        <View style={styles.containermodal}>
          <View style={styles.cardSearch}>
            <Icons name='search' color={Colors.gray4} size={20} />
            <TextInput value={cari} onChangeText={(txt) => { setCari(txt), getFilter(txt) }} placeholder='Cari Nama Kota' placeholderTextColor={Colors.placeholder} style={styles.txtSearch} />
          </View>
          <FlatList
            data={dataFilter}
            renderItem={renderViewFilter}
            keyExtractor={item => item.id}
            keyboardAvoidingBehavior="height"
            ListEmptyComponent={() => {
              return (
                <View style={{ alignItems: 'center', justifyContent: 'center', width: screenWidth * 1, height: screenHeight * 0.7 }}>
                  <Image source={require('../../../Assets/images/empty.png')} style={{ width: 220, height: 144 }} />
                  <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.gray1, marginTop: 16 }}>Tidak Ada Data</Text>
                </View>
              )
            }}
          />
        </View>
      </Modalize>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  wrapFilter: {
    marginRight: 8,
    borderWidth: 1,
    borderColor: Colors.gray5,
    backgroundColor: Colors.container,
    justifyContent: 'center',
    height: 30,
    borderRadius: 100,
    flex: 1,
    marginVertical: 8,
    paddingHorizontal: 12,
  },
  wrapFilterActive: {
    marginRight: 8,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    height: 30,
    borderRadius: 100,
    flex: 1,
    marginVertical: 8,
    paddingHorizontal: 12,
  },
  // modal
  modalstyle: { borderTopLeftRadius: 20, borderTopRightRadius: 20, zIndex: 5 },
  handlestyle: { backgroundColor: Colors.gray3, marginTop: 30, width: 40 },
  containermodal: { paddingHorizontal: 16, height: 300 },
  titlemodal: { fontSize: 16, fontFamily: 'Roboto-Bold', lineHeight: 21, color: Colors.gray1, textAlign: 'center' },
  cardFilter: { flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: Colors.container, paddingVertical: 8 },
  txtFilter: { fontFamily: 'Roboto-Regular', fontSize: 12, color: Colors.black },
  cardSearch: { borderColor: Colors.gray5, borderRadius: 30, borderWidth: 1, paddingHorizontal: 12, flexDirection: 'row', alignItems: 'center', height: 45, marginVertical: 8 },
  txtSearch: { flex: 1, fontFamily: 'Roboto-Regular', fontSize: 12, color: Colors.black, marginLeft: 8 },
});
