import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  ImageBackground,
  StatusBar,
  ScrollView,
  Alert,
  Modal,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import React, { useState } from 'react';
import Icons from 'react-native-vector-icons/Ionicons';
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';
import OneSignal from 'react-native-onesignal';
import { screenHeight, screenWidth } from '../../../Variable';
import Colors from '../../../Styles/Colors';
import BtnPrimary from '../../Components/BtnPrimary';
import BtnOutline from '../../Components/BtnOutline';
import { Api, Key } from '../../../Screens/Api';
import { Input } from '@rneui/themed';
import SpinnerLoading from '../../Components/SpinnerLoading';

const index = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [pass, setPass] = useState('');
  const [confirmPass, setConfirmPass] = useState('');
  const [isFocused, setIsFocused] = useState(false);
  const [isFocused1, setIsFocused1] = useState(false);
  const [isFocused2, setIsFocused2] = useState(false);
  const [isFocused3, setIsFocused3] = useState(false);
  const [showError, setShowError] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);

  // save data login
  const saveDataLogin = async (token, role) => {
    console.log('tokee', token);
    try {
      if (token != null) {
        await AsyncStorage.setItem('user_token', token)
      }
    } catch (error) {
      console.log(error)
    }
  }

  //on register
  const onRegister = () => {
    if (pass !== confirmPass) {
      setShowError(true)
      setTimeout(() => {
        setShowError(false)
      }, 1200);
    } else {
      setLoading(true)
      //set data
      let data = {
        'name': username,
        'email': email,
        'password': pass,
        'password_confirmation': confirmPass
      }
      console.log('dataa', data)

      // axios post
      axios.post(`${Api}/register`, data, {
        headers: {
          'Accept': 'application/json',
          'key': Key,
        }
      }).then((res) => {
        console.log('respon register=>', res.data)
        setLoading(false)
        if (res.data.message == 'Email sudah terdaftar!') {
          alert(res.data.message);
        } else {
          setModalVisible(true)
        }
      }).catch((err) => {
        console.log('error register=>', err)
        setLoading(false)

      })
    }
  };

  // Login Google
  const onLoginGoogle = async () => {
    // function
    try {
      await GoogleSignin.configure({
        offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
        webClientId: "417002044033-5o7tu51e3qt4vofkacacul2u6nf0hpft.apps.googleusercontent.com",
        androidClientId: '417002044033-0aiu8j57sep6cv1murhrhego4nirk1g5.apps.googleusercontent.com'
      });

      await GoogleSignin.hasPlayServices();
      setLoading(true)
      const infoUser = await GoogleSignin.signIn();
      const dataLogin = {
        name: infoUser.user.name,
        email: infoUser.user.email,
      }
      console.log('hasil', dataLogin);
      // await GoogleSignin.signIn().then(result => { 
      //   console.log('result google', result);
      //  });

      // axios post
      axios.post(`${Api}/google`, dataLogin, {
        headers: {
          'Accept': 'application/json',
          'key': Key,
        }
      }).then((res) => {
        console.log('result google', res.data);
        if (res.data.success == true) {
          saveDataLogin(res.data.token)
          OneSignal.setAppId("12b2d23e-304f-4f5e-87e5-8958e25972b7"); //akun wanflora1@gmail.com
          OneSignal.setLogLevel(6, 0);
          OneSignal.setExternalUserId(res.data?.data?.id + '', (results) => {
            // The results will contain push and email success statuses
            console.log('onesignal', results);
          })
          navigation.reset({
            index: 0,
            routes: [{ name: 'Home', params: { navv: 'header' } }],
          })
          setLoading(false)
        } else if (res.data.message == 'Akun tidak ditemukan!') {
          setLoading(false)
          alert('Akun belum terdaftar!');
        } else if (res.data.message == 'Akun belum diaktifkan!') {
          setLoading(false)
          alert('Akun belum diaktifkan!');
        } else if (res.data.message == 'Password tidak sesuai!') {
          setLoading(false)
          setAlertError(true)
          setTimeout(() => {
            setAlertError(false);
          }, 1500);
        }
      }).catch((err) => {
        setLoading(false)
        console.log('err google', err)
      })
      // return;

    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        alert('User cancelled the login flow !');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        alert('Signin in progress');
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        alert('Google play services not available or outdated !');
        // play services not available or outdated
      } else {
        console.log(error, 'err google')
      }
    }
  }

  const validation = () => {
    if (username == '' || email == '' || pass == '' || confirmPass == '') {
      return true
    } else if (pass.length < 8) {
      return true
    } else {
      return false
    }
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      {loading == true ? <SpinnerLoading spinner={true} /> : null}
      {/* <ScrollView showsVerticalScrollIndicator={false}> */}
      <ImageBackground source={require('../../../Assets/images/bg3.png')} style={{ width: screenWidth, height: screenHeight - 100 }}>
        <StatusBar backgroundColor="transparent" translucent barStyle="light-content" />
        <View style={{ height: '20%' }} />
        <View
          style={{
            // flex: 1,
            width: screenWidth - 32,
            backgroundColor: Colors.white,
            height: Dimensions.get('window').height * 0.8,
            alignSelf: 'center',
            borderRadius: 15,
            paddingHorizontal: 12,
            paddingVertical: 12,
            elevation: 4,
            marginBottom: 2
          }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View>
              <Text style={{ color: Colors.black, fontSize: 22, fontWeight: '700', textAlign: 'center', fontFamily: 'Roboto-Bold' }}> Register </Text>
              <Input
                // value={username}
                onChangeText={(text) => setUsername(text)}
                placeholder={'Full Name'}
                onFocus={() => { setIsFocused(true) }}
                onBlur={() => { setIsFocused(false) }}
                leftIcon={
                  <Image
                    source={require('../../../Assets/icons/User.png')}
                    style={{ width: 24, height: 24 }}
                  />
                }
                style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                containerStyle={{
                  height: 48,
                  marginTop: 16,
                  borderWidth: 1,
                  borderRadius: 100,
                  borderColor: isFocused == true ? Colors.primary : Colors.gray,
                }}
              />
              <Input
                // value={email}
                onChangeText={(text) => setEmail(text)}
                placeholder={'Email'}
                keyboardType='email-address'
                autoCapitalize='none'
                onFocus={() => { setIsFocused1(true) }}
                onBlur={() => { setIsFocused1(false) }}
                leftIcon={
                  <Image
                    source={require('../../../Assets/icons/Message.png')}
                    style={{ width: 24, height: 24 }}
                  />
                }
                style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                containerStyle={{
                  height: 48,
                  marginTop: 16,
                  borderWidth: 1,
                  borderRadius: 100,
                  borderColor: isFocused1 == true ? Colors.primary : Colors.gray,
                }}
              />
              <Input
                // value={pass}
                onChangeText={(text) => setPass(text)}
                keyboardType='default'
                placeholder={'Password'}
                onFocus={() => { setIsFocused2(true) }}
                onBlur={() => { setIsFocused2(false) }}
                secureTextEntry={true}
                leftIcon={
                  <Image
                    source={require('../../../Assets/icons/Lock.png')}
                    style={{ width: 24, height: 24 }}
                  />
                }
                style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                containerStyle={{
                  height: 48,
                  marginTop: 16,
                  borderWidth: 1,
                  borderRadius: 100,
                  borderColor: isFocused2 == true ? Colors.primary : Colors.gray,
                }}
              />
              <Input
                // value={confirmPass}
                onChangeText={(text) => setConfirmPass(text)}
                keyboardType='default'
                placeholder={'Konfirmasi Password'}
                onFocus={() => { setIsFocused3(true) }}
                onBlur={() => { setIsFocused3(false) }}
                secureTextEntry={true}
                leftIcon={
                  <Image
                    source={require('../../../Assets/icons/Lock.png')}
                    style={{ width: 24, height: 24 }}
                  />
                }
                style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                containerStyle={{
                  height: 48,
                  marginTop: 16,
                  borderWidth: 1,
                  borderRadius: 100,
                  borderColor: isFocused3 == true ? Colors.primary : Colors.gray,
                }}
              />
              <View style={{ height: 16 }} />
              <View style={{ flexDirection: 'row' }}>
                <Icons name='information-circle-outline' size={16} color={Colors.black} />
                <Text
                  style={{
                    fontWeight: '400',
                    fontSize: 11,
                    color: Colors.placeholder,
                    marginLeft: 8,
                    fontFamily: 'Roboto-Regular'
                  }}>
                  Password setidaknya mengandung 8 karakter
                </Text>
              </View>
              <View style={{ height: 16 }} />
              <BtnPrimary title={'Register'} pressed={() => { onRegister() }} disabled={validation()} />
              <View style={{ height: 16 }} />
              <Text
                style={{ textAlign: 'center', fontSize: 12, color: Colors.black, fontFamily: 'Roboto-Regular' }}>
                Sudah Punya Akun?
              </Text>
              <View style={{ height: 16 }} />
              <BtnOutline title={'Login'} pressed={() => { navigation.navigate('Login') }} />
              <View style={{ height: 16 }} />
              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <View style={{ height: 1, width: '30%', backgroundColor: Colors.container }} />
                <Text style={{ textAlign: 'center', fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black }}>
                  Atau masuk dengan
                </Text>
                <View style={{ height: 1, width: '30%', backgroundColor: Colors.container }} />
              </View>
              <View style={{ height: 16 }} />
              <TouchableOpacity onPress={() => { onLoginGoogle() }} style={{ flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderColor: Colors.container, borderRadius: 15, paddingVertical: 10, paddingHorizontal: 16 }}>
                <Image source={require('../../../Assets/images/google_icon.png')} style={{ width: 20, height: 20 }} />
                <Text style={{ fontSize: 12, fontFamily: 'Roboto-Regular', color: Colors.black, textAlign: 'center', width: '90%' }}>Masuk Menggunakan Google</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      </ImageBackground>
      {/* </ScrollView> */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Image source={require('../../../Assets/images/success.png')} style={{ width: 90, height: 90 }} />
            <Text style={styles.titlemodal}>Berhasil</Text>
            <Text style={styles.descmodal}>Akun berhasil didaftarkan! Silahkan cek email untuk melanjutkan aktivasi</Text>
            {/* button */}
            <View style={styles.containerbtnmodal}>
              <TouchableOpacity onPress={() => { setModalVisible(false), navigation.navigate('Login') }} style={styles.btnmodal}>
                <Text style={styles.textmodal}>Ok</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  // modal
  centeredView: { flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: '#00000070' },
  modalView: { backgroundColor: Colors.white, borderRadius: 15, width: screenWidth * 0.8, shadowColor: "#000", shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.25, shadowRadius: 4, elevation: 5, paddingVertical: 16, paddingHorizontal: 16, alignItems: 'center' },
  titlemodal: { fontSize: 15, fontFamily: 'Roboto-Medium', lineHeight: 21, color: Colors.black, textAlign: 'center', marginTop: 5 },
  descmodal: { fontSize: 13, fontFamily: 'Roboto-Regular', lineHeight: 18, color: Colors.black, textAlign: 'center', marginTop: 5 },
  containerbtnmodal: { flexDirection: 'row', justifyContent: 'space-between', marginTop: 16 },
  btnmodal: { width: '90%', height: 36, borderWidth: 1, borderColor: Colors.primary, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.primary },
  textmodal: { fontSize: 12, fontFamily: 'Roboto-Medium', lineHeight: 18, color: Colors.white },
});
