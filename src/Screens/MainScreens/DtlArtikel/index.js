import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState, useEffect } from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Linking,
  Share,
  TouchableOpacity,
  StatusBar
} from 'react-native';
import { SliderBox } from 'react-native-image-slider-box';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import { screenWidth } from '../../../Variable';
import { Api, Key, AdMob } from '../../../Screens/Api';
import HeaderTitleBack from '../../Components/HeaderTitleBack';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceholder from 'react-native-shimmer-placeholder';
import HTMLView from 'react-native-htmlview';
import { AdMobBanner } from 'react-native-admob';

const dataEvent = [
  require('../../../Assets/images/event.png'),
  require('../../../Assets/images/event.png'),
  require('../../../Assets/images/event.png'),
];

const dataSharing = [
  require('../../../Assets/images/sharing.png'),
  require('../../../Assets/images/sharing.png'),
  require('../../../Assets/images/sharing.png'),
];

const index = ({ navigation, route }) => {
  const [shimmer, setShimmer] = useState(false);
  const [loading, setLoading] = useState(false);
  const [tokens, setTokens] = useState('');
  const [dataDetail, setDataDetail] = useState('');
  const [sliders, setSliders] = useState([]);

  useEffect(() => {
    Linking.addEventListener('url', event => handleDeeplink(event.url));
    // get data login
    async function getDataLogin() {
      try {
        const token = await AsyncStorage.getItem('user_token')
        setTokens(token)
        getDetail(token)
      } catch (e) {
        console.log(e)
      }
    }
    getDataLogin();
  }, []);

  // console.log('routee', route.params);

  async function handleDeeplink(url) {
    // setTypeLink('')
    console.log('url', url);
    try {
      // const url = await Linking.getInitialURL();
      // const url2 = await Linking.addEventListener('url', event => { return event });
      // console.log(url2, 'detailllllllllll', url);
      if (url) {
        let newUrl = `${url}`;

        newUrl = newUrl.slice(newUrl.indexOf('://') + 3);

        if (newUrl.indexOf('www.wanflora.com') >= 0) {
          newUrl = newUrl.slice(newUrl.indexOf('www.wanflora.com/') + 'www.wanflora.com/'.length);
        }

        const [rawUri, param = ""] = newUrl.split('?');
        const [uriType, uriId, uriIv, uriKey] = rawUri.split('/');
        const uri = {
          type: uriType || '',
          param: uriId || '',
          paramKey: uriKey || '',
          paramIv: uriIv || '',
          query: param
            .split('&')
            .reduce((all, d) => {

              const [key, value] = d.split('=');

              all[key] = value;

              return all;
            }, {})
        };

        return;
      } else {
        getDataLogin(null, '');
      }
    } catch (err) {
      console.error('An error occurred on get deeplink', err);
    }
  }

  // on share
  const onShare = async (data, type) => {
    try {
      // const urlLinking = await Linking.getInitialURL(data.id);
      // handleDeeplink(url);
      const result = await Share.share({
        message: `${type == 'sharing' ? 'Sharing' : 'Event'} ${data?.title}, Ayo Cek Sekarang! https://www.wanflora.com/DtlArtikel/${data?.id}/${type}`,
        url: `https://www.wanflora.com/DtlArtikel/${data?.id}/${type}`
      });
      return result
    } catch (error) {
      alert(error.message);
    }
  };

  const getDetail = (token) => {
    setShimmer(true)
    // axios get
    axios.get(route.params?.title == 'Event' ? `${Api}/event/find/${route.params?.id}` : `${Api}/sharing/find/${route.params?.id}`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      console.log('result detail', res.data);
      const slider = res.data.data?.files?.map((a) => {
        return (a.file)
      })
      setSliders(slider)
      setDataDetail(res.data.data)
      setShimmer(false);
    }).catch((err) => {
      setShimmer(false);
      console.log('err detail', err.response)
    })
  }


  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      <StatusBar backgroundColor={Colors.primary} barStyle={'light-content'} />
      {/* header */}
      {/* <HeaderTitleBack title={'Detail' + route.params?.title} onPressBack={() => {route?.params?.notif != undefined ? navigation.push('Notification') : navigation.goBack()}}/> */}
      <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: Colors.primary, paddingHorizontal: 16, width: screenWidth, height: route?.params?.tab !== undefined ? 80 : 60, paddingTop: route?.params?.tab !== undefined ? 26 : 5 }}>
        <TouchableOpacity onPress={() => { route?.params?.notif != undefined ? navigation.push('Notification') : navigation.goBack() }}>
          <AntDesign name='arrowleft' size={26} color={Colors.white} />
        </TouchableOpacity>
        <Text style={[Font.bold16, { color: Colors.white, marginLeft: 20 }]}>Detail {route.params?.title}</Text>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* slider */}
        {shimmer == true ? <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth, height: 210, marginBottom: 8 }} /> :
          <SliderBox
            autoplay
            circleLoop
            images={sliders}
            ImageComponentStyle={{
              height: 210,
              width: screenWidth,
            }}
            paginationBoxStyle={{
              alignSelf: 'center',
            }}
            dotStyle={{
              width: 8,
              height: 8,
              marginHorizontal: -20,
            }}
            dotColor={Colors.primary}
            inactiveDotColor={Colors.container}
            parentWidth={screenWidth}
          />}
        {route.params?.title == 'Event' ? (
          <View style={styles.wrapSlider}>
            <View style={{ flex: 1, justifyContent: 'space-between' }}>
              {shimmer == true ? <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.6, height: 16, borderRadius: 7 }} /> :
                <Text style={Font.reguler16}>{dataDetail?.title}</Text>}
              {shimmer == true ? <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.35, height: 14, borderRadius: 7 }} /> :
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image
                    source={require('../../../Assets/icons/date.png')}
                    style={{ width: 16, height: 16, marginRight: 8 }}
                  />
                  <Text style={[Font.caption, { color: Colors.placeholder }]}>
                    {dataDetail?.start?.date + ' - ' + dataDetail?.end?.date}
                  </Text>
                </View>}
              {shimmer == true ? <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.35, height: 14, borderRadius: 7 }} /> :
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image
                    source={require('../../../Assets/icons/time.png')}
                    style={{ width: 16, height: 16, marginRight: 8 }}
                  />
                  <Text style={[Font.caption, { color: Colors.placeholder }]}>
                    {dataDetail?.start?.time + ' - ' + dataDetail?.end?.time}
                  </Text>
                </View>}
            </View>
            <TouchableOpacity onPress={() => { onShare(dataDetail, 'Event') }}>
              <AntDesign
                name="sharealt"
                size={20}
                color={Colors.black}
              />
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.wrapSlider2}>
            <View style={{ flex: 1, justifyContent: 'space-between' }}>
              {shimmer == true ? <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.7, height: 16, borderRadius: 7 }} /> :
                <Text style={Font.reguler16}>{dataDetail?.title}</Text>}
            </View>

            <TouchableOpacity onPress={() => { onShare(dataDetail, 'sharing') }}>
              <AntDesign
                name="sharealt"
                size={20}
                color={Colors.black}
              />
            </TouchableOpacity>
          </View>
        )}
        <View style={{ height: 8, backgroundColor: Colors.container }} />

        <View style={{ flex: 1, paddingVertical: 10, paddingHorizontal: 16, backgroundColor: Colors.white }}>
          {shimmer == true ? <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.3, height: 16, borderRadius: 7 }} /> :
            <Text style={Font.bold14}>Detail {route.params?.title}</Text>}
          <View style={{ height: 8 }} />
          {shimmer == true ?
            <>
              <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.9, height: 16, borderRadius: 7, marginTop: 8 }} />
              <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.7, height: 16, borderRadius: 7, marginVertical: 5 }} />
              <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.6, height: 16, borderRadius: 7 }} />
            </> :
            <>
              <HTMLView
                value={"<div>" + dataDetail?.desc + "</div>"}
                stylesheet={styles.txt}
                numberOfLines={0}
              />
              {/* <Text style={[Font.reguler12, { color: Colors.placeholder }]}>
               {dataDetail?.desc}
             </Text> */}
            </>}
          <View style={{ height: 30 }} />
        </View>
      </ScrollView>
      {/* admob */}
      <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
        <AdMobBanner
          adSize={'banner'}
          adUnitID={AdMob}
          testDevices={[AdMobBanner.simulatorId]}
          onAdFailedToLoad={error => console.error(error, 'err admob')}
        />
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  wrapSlider: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: Colors.white,
    padding: 16,
    height: 100,
  },
  wrapSlider2: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: Colors.white,
    padding: 16,
    height: 56,
  },
  wrapDsc: {
    flex: 1,
    height: 88,
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.container,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
  },
  txt: { p: { fontFamily: 'Roboto-Regular', fontSize: 12, color: Colors.placeholder } }
});
