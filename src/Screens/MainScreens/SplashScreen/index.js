import {
  StyleSheet,
  View,
  Image,
  Animated,
  ImageBackground,
  StatusBar,
  Text,
} from 'react-native';
import React, {useEffect, useRef} from 'react';
import {screenHeight, screenWidth} from '../../../Variable';
import Colors from '../../../Styles/Colors';

const index = () => {
  const fadeIn = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeIn, {
      toValue: 1,
      duration: 3000,
      useNativeDriver: false,
    }).start();
  }, [fadeIn]);

  return (
    <ImageBackground
      source={require('../../../Assets/images/bg1.png')}
      style={styles.wrap}>
      <StatusBar
        backgroundColor="transparent"
        translucent
        barStyle="light-content"
      />
      <Animated.View style={{opacity: fadeIn}}>
        <View style={{alignItems: 'center'}}>
          <Image
            source={require('../../../Assets/icons/new_logo.png')}
            style={{width: 160, height: 89}}
            resizeMode={'contain'}
          />
          <View style={{height: 14}} />
          <Text
            style={{
              fontWeight: '600',
              color: Colors.white,
              fontSize: 24,
              textAlign: 'center',
              fontFamily: 'Roboto-Bold'
            }}>
            Wan Flora
          </Text>
        </View>
      </Animated.View>
    </ImageBackground>
  );
};

export default index;

const styles = StyleSheet.create({
  wrap: {
    justifyContent: 'center',
    alignItems: 'center',
    width: screenWidth,
    height: screenHeight,
  },
});
