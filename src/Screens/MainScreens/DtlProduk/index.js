import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState, useEffect, useRef } from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Linking,
  Animated,
  Share,
  StatusBar
} from 'react-native';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import { screenWidth, screenHeight } from '../../../Variable';
import BtnOutline from '../../Components/BtnOutline';
import BtnPrimary from '../../Components/BtnPrimary';
import Money from '../../Components/Money';
import { Api, Key, AdMob } from '../../../Screens/Api';
import BeforeLogin from '../../Components/BeforeLogin';
import { SliderBox } from 'react-native-image-slider-box';
import Icon from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceholder from 'react-native-shimmer-placeholder';
import ReadMore from '@fawazahmed/react-native-read-more';
import clip from "text-clipper";
import HTMLView from 'react-native-htmlview';
import { Modalize } from 'react-native-modalize';
import { AdMobBanner } from 'react-native-admob';

const dataBanner = [
  require('../../../Assets/images/tanaman.png'),
  require('../../../Assets/images/tanaman.png'),
  require('../../../Assets/images/tanaman.png'),
];

const scrollX = new Animated.Value(0);
let position = Animated.divide(scrollX, screenWidth);

const index = ({ navigation, route }) => {
  const [shimmer, setShimmer] = useState(true);
  const [loading, setLoading] = useState(false);
  const [tokens, setTokens] = useState('');
  const [dataDetail, setDataDetail] = useState('');
  const [showAlert, setShowAlert] = useState(false);
  const [message, setMessage] = useState('');
  const [showLogin, setShowLogin] = useState(false);
  const modalLink = useRef(null);
  const [mobileNumber, setMobileNumber] = useState('');
  const [whatsAppMsg, setWhatsAppMsg] = useState(
    'Silahkan beli produk kami..',
  );

  useEffect(() => {
    Linking.addEventListener('url', event => handleDeeplink(event.url));
    // get data login
    async function getDataLogin() {
      try {
        const token = await AsyncStorage.getItem('user_token')
        console.log('tokne', token);
        setTokens(token)
        getDetail(token)
      } catch (e) {
        console.log(e)
      }
    }
    getDataLogin();

  }, []);

  // console.log('route', route.params);

  const getDetail = (token) => {
    // axios get
    axios.get(`${Api}/product/find/${route.params?.id}`, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + token,
      }
    }).then((res) => {
      console.log('result detail', res.data);
      setDataDetail(res.data.data)
      setMobileNumber(res.data.data.store.whatsapp)
      setShimmer(false);
    }).catch((err) => {
      setShimmer(false);
      console.log('err detail', err.response)
    })
  }

  const addFavorite = (id) => {
    let data = {
      id_product: id
    }
    // setShimmer(true)
    // axios post
    axios.post(`${Api}/product/favorit`, data, {
      headers: {
        'Accept': 'application/json',
        'key': Key,
        'Authorization': 'Bearer ' + tokens,
      }
    }).then((res) => {
      console.log('result favorite', res.data);
      getDetail(tokens);
      setMessage(res.data.message)
      setShowAlert(true)
      setTimeout(() => {
        setShowAlert(false)
      }, 500);
      setShimmer(false);
    }).catch((err) => {
      setShimmer(false);
      console.log('err favorite', err.response)
    })
  }

  const initiateWhatsApp = () => {
    // Check for perfect 10 digit length
    if (mobileNumber.length < 10) {
      alert('Please insert correct WhatsApp number');
      return;
    }
    // You can change 62 with your country code
    let url =
      'whatsapp://send?text=' +
      whatsAppMsg +
      '&phone=62' + mobileNumber;
    Linking.openURL(url)
      .then((data) => {
        console.log('WhatsApp Opened');
      })
      .catch(() => {
        alert('Make sure Whatsapp installed on your device');
      });
  };

  const openEcommeece = (url) => {
    Linking.openURL(url)
      .then((data) => {
        console.log('link Opened');
      })
      .catch(() => {
        alert('Make sure E-Commerse installed on your device');
      });
  }

  async function handleDeeplink(url) {
    // setTypeLink('')
    console.log('url', url);
    try {
      // const url = await Linking.getInitialURL();
      // const url2 = await Linking.addEventListener('url', event => { return event });
      // console.log(url2, 'detailllllllllll', url);
      if (url) {
        let newUrl = `${url}`;

        newUrl = newUrl.slice(newUrl.indexOf('://') + 3);

        if (newUrl.indexOf('www.wanflora.com') >= 0) {
          newUrl = newUrl.slice(newUrl.indexOf('www.wanflora.com/') + 'www.wanflora.com/'.length);
        }

        const [rawUri, param = ""] = newUrl.split('?');
        const [uriType, uriId, uriIv, uriKey] = rawUri.split('/');
        const uri = {
          type: uriType || '',
          param: uriId || '',
          paramKey: uriKey || '',
          paramIv: uriIv || '',
          query: param
            .split('&')
            .reduce((all, d) => {

              const [key, value] = d.split('=');

              all[key] = value;

              return all;
            }, {})
        };

        return;
      } else {
        getDataLogin(null, '');
      }
    } catch (err) {
      console.error('An error occurred on get deeplink', err);
    }
  }

  // on share
  const onShare = async (id) => {
    try {
      // const urlLinking = await Linking.getInitialURL(data.id);
      // handleDeeplink(url);
      const result = await Share.share({
        message: `Beli ${dataDetail?.title} di ${dataDetail?.store?.name} dengan harga Rp${dataDetail?.price}, Sekarang! https://www.wanflora.com/DtlProduk/${dataDetail?.id}`,
        url: `https://www.wanflora.com/DtlProduk/${dataDetail?.id}`
      });
      return result
    } catch (error) {
      alert(error.message);
    }
  };

  const screenLogin = () => {
    return (
      <View style={{ height: screenHeight * 0.85 }}>
        <BeforeLogin navigation={navigation} />
      </View>
    )
  }

  const renderViewLink = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => { openEcommeece(item.link), modalLink.current.close() }} style={styles.cardLink}>
        <Image source={{ uri: item.file }} style={{ width: 30, height: 30 }} />
        <Text style={styles.txtLink}>{item.platform}</Text>
      </TouchableOpacity>
    )
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StatusBar backgroundColor={Colors.primary} barStyle={'light-content'} />
      {/* header */}
      <View style={{ width: '100%', height: route?.params?.tab !== undefined ? 88 : 65, paddingTop: route?.params?.tab !== undefined ? 34 : 0 ,flexDirection: 'row', alignItems: 'center', paddingHorizontal: 16, justifyContent: 'space-between', backgroundColor: Colors.primary, elevation: 5 }}>
        <TouchableOpacity onPress={() => { route?.params?.notif != undefined ? navigation.push('Notification') : navigation.goBack() }}>
          <AntDesign name='arrowleft' size={26} color={Colors.white} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => { navigation.navigate('SearchProduct', { title: 'Product' }) }}
          style={{
            backgroundColor: Colors.white,
            height: 35,
            borderRadius: 100,
            width: screenWidth - 85,
            paddingHorizontal: 8,
            flexDirection: 'row',
            alignItems: 'center',
            // marginLeft: 20
          }}>
          <Ionicons
            name="search"
            size={18}
            color={Colors.placeholder}
            style={{ marginRight: 11 }}
            onPress={() => { }}
          />
          <Text style={Font.reguler12}>Cari “tanaman hias”</Text>
        </TouchableOpacity>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        {showLogin == true ? screenLogin() :
          <>
            {/* slider */}
            {shimmer == true ?
              <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth, height: 375 }} /> :
              <>
                <FlatList
                  data={dataDetail?.files}
                  keyExtractor={(item, index) => 'key' + index}
                  horizontal
                  pagingEnabled
                  scrollEnabled
                  snapToAlignment="center"
                  scrollEventThrottle={16}
                  decelerationRate={'fast'}
                  showsHorizontalScrollIndicator={false}
                  renderItem={({ item, index }) => {
                    return <View>
                      <Image style={{
                        height: 375,
                        width: screenWidth,
                      }} resizeMode='cover' source={{ uri: item.file }} />
                    </View>;
                  }}
                  onScroll={Animated.event(
                    [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                    { useNativeDriver: false }
                  )}
                />
                <View style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  bottom: screenHeight * 0.5
                }}>
                  {dataDetail?.files?.map((_, i) => {
                    let opacity = position.interpolate({
                      inputRange: [i - 1, i, i + 1],
                      outputRange: [0.3, 1, 0.3],
                      extrapolate: 'clamp',
                    });
                    return (
                      <Animated.View
                        key={i}
                        style={{ opacity, height: 8, width: 8, backgroundColor: Colors.primary, margin: 5, borderRadius: 100 }}
                      />
                    );
                  })}
                </View>
              </>}
            <View style={styles.wrapSlider}>
              <View style={{ flex: 1, justifyContent: 'space-between' }}>
                {shimmer == true ?
                  <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.4, height: 16, borderRadius: 7 }} /> :
                  <Text style={Font.reguler16}>{dataDetail?.title}</Text>}
                <View style={{ height: 8 }} />
                {shimmer == true ? <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.3, height: 16, borderRadius: 7 }} /> :
                  <Money
                    value={dataDetail?.price_discount == '0' ? dataDetail?.price : dataDetail?.price_discount}
                    styling={[Font.bold16, { color: Colors.primary }]}
                  />}
                <View style={{ height: 8 }} />
                {dataDetail?.discount_percent > 0 ?
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View
                      style={{
                        backgroundColor: Colors.error,
                        borderRadius: 100,
                        paddingVertical: 4, paddingHorizontal: 10
                      }}>
                      <Text style={[Font.reguler12, { color: Colors.white }]}>
                        {dataDetail?.discount_percent}% Disc
                      </Text>
                    </View>
                    <Money
                      value={dataDetail?.price}
                      styling={[
                        Font.reguler12,
                        {
                          color: Colors.placeholder,
                          textDecorationLine: 'line-through',
                          marginLeft: 4,
                        },
                      ]}
                    />
                  </View> : null}
              </View>
              <View style={{ flexDirection: 'row' }}>
                {dataDetail?.favorit == 0 ?
                  <TouchableOpacity onPress={() => { tokens !== null ? addFavorite(dataDetail?.id) : setShowLogin(true) }}>
                    <AntDesign
                      name="hearto"
                      size={20}
                      color={Colors.black}
                      style={{ marginRight: 8 }}
                    />
                  </TouchableOpacity> :
                  <TouchableOpacity onPress={() => { addFavorite(dataDetail?.id) }}>
                    <AntDesign
                      name="heart"
                      size={20}
                      color={Colors.primary}
                      style={{ marginRight: 8 }}
                    />
                  </TouchableOpacity>}
                <AntDesign
                  name="sharealt"
                  size={20}
                  color={Colors.black}
                  onPress={() => { onShare() }}
                />
              </View>
            </View>
            <View style={{ height: 8 }} />
            <View style={{ flex: 1, paddingVertical: 8, paddingHorizontal: 16, backgroundColor: Colors.white }}>
              <Text style={Font.bold14}>Deskripsi Produk</Text>
              <View style={{ height: 8 }} />
              <View
                style={{
                  flexDirection: 'row',
                  backgroundColor: Colors.lightPrimary,
                  padding: 8,
                  borderRadius: 7,
                }}>
                <Text style={[Font.reguler12]}>Kategori</Text>
                {shimmer == true ?
                  <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.4, height: 16, borderRadius: 7, marginLeft: 50 }} /> :
                  <Text
                    style={[Font.bold12, { color: Colors.primary, marginLeft: 100 }]}>
                    {dataDetail?.category?.title}
                  </Text>}
              </View>
              <View style={{ height: 8 }} />
              {shimmer == true ?
                <>
                  <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.7, height: 16, borderRadius: 7 }} />
                  <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.6, height: 16, borderRadius: 7, marginTop: 8 }} />
                  <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.4, height: 16, borderRadius: 7, marginTop: 8 }} />
                  <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.4, height: 16, borderRadius: 7, marginTop: 8 }} />
                </> :
                // <ReadMore style={[Font.reguler12, { color: Colors.placeholder }]} seeMoreText={'Baca Selengkapnya'} seeMoreStyle={styles.txtRead} seeLessText={'Sembunyikan'} seeLessStyle={styles.txtRead}>
                //   {/* {item.desc} */}
                //   {clip(
                //     dataDetail?.desc,
                //     dataDetail?.desc?.length,
                //     { html: true, stripTags: true }
                //   )}
                // </ReadMore>
                <HTMLView
                  value={"<div>" + dataDetail?.desc + "</div>"}
                  stylesheet={styles.txt}
                  numberOfLines={0}
                />}
              <View style={{ height: 8 }} />
              {/* admob */}
              <View style={{ alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 8 }}>
                <AdMobBanner
                  adSize={'banner'}
                  adUnitID={AdMob}
                  testDevices={[AdMobBanner.simulatorId]}
                  onAdFailedToLoad={error => console.error(error, 'err admob')}
                />
              </View>
              <View style={styles.wrapDsc}>
                {shimmer == true ? <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: 56, height: 56, borderRadius: 30 }} /> :
                  <Image
                    source={{ uri: dataDetail?.store?.file }}
                    style={{ width: 56, height: 56, borderRadius: 30 }}
                  />}
                <View style={{ flex: 1, marginLeft: 8, justifyContent: 'center' }}>
                  {shimmer == true ?
                    <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.47, height: 16, borderRadius: 7 }} /> :
                    <Text style={Font.bold14}>{dataDetail?.store?.name}</Text>}
                  <View
                    style={{
                      borderBottomColor: Colors.container,
                      borderBottomWidth: 1,
                      marginVertical: 4,
                    }}
                  />
                  {shimmer == true ?
                    <ShimmerPlaceholder LinearGradient={LinearGradient} visible={false} style={{ width: screenWidth * 0.47, height: 16, borderRadius: 7 }} /> :
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Image
                        source={require('../../../Assets/icons/Pin_fill.png')}
                        style={{ width: 24, height: 24 }}
                      />
                      <Text style={Font.reguler12}>{dataDetail?.store?.city?.city_name}, {dataDetail?.store?.province?.province_name}</Text>
                    </View>}
                </View>
                {shimmer == true ? null :
                  <View style={{ width: '22%', marginLeft: 8 }}>
                    <BtnOutline title={'Cek Toko'} pressed={() => { navigation.navigate('DtlToko', { id: dataDetail?.id_store }) }} />
                  </View>}
              </View>
            </View>
          </>}
      </ScrollView>
      {showLogin == true ? null :
        <View
          style={{
            flexDirection: 'row',
            height: 65,
            backgroundColor: Colors.white,
            elevation: 4,
            alignItems: 'center',
            paddingHorizontal: 16,
          }}>
          <View style={{ width: 126 }}>
            <BtnOutline title={'Link E-Commerce'} radius={true} pressed={() => { modalLink.current.open() }} />
          </View>
          <View style={{ marginLeft: 8, flex: 1 }}>
            <BtnPrimary
              title={'Pesan via Whatsapp'}
              pressed={() => { tokens !== null ? initiateWhatsApp() : setShowLogin(true) }}
              iconLeft={
                <Image
                  source={require('../../../Assets/icons/whatsapp.png')}
                  style={{ width: 20, height: 20, marginRight: 8 }}
                />
              }
              radius={true}
            />
          </View>
        </View>}
      {showAlert == true ?
        <View style={{ backgroundColor: Colors.white, elevation: 5, borderRadius: 30, paddingVertical: 10, position: 'absolute', bottom: '10%', width: screenWidth * 0.6, alignSelf: 'center', alignItems: 'center' }}>
          <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 13, color: Colors.black }}>{message}</Text>
        </View> : null}
      {/* choose link */}
      <Modalize
        modalHeight={350}
        // adjustToContentHeight={true}
        modalStyle={styles.modalstyle}
        handleStyle={styles.handlestyle}
        closeOnOverlayTap={true}
        ref={modalLink}
        HeaderComponent={(
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 16, marginTop: 30 }}>
            <Text style={styles.titlemodal}>Link E-Commerce</Text>
            <TouchableOpacity onPress={() => { modalLink.current.close() }}>
              <Icon name={'x'} size={24} color={Colors.gray3} style={{ height: 30 }} />
            </TouchableOpacity>
          </View>
        )}>
        <View style={styles.containermodal}>
          <FlatList
            data={dataDetail?.ecommerce}
            renderItem={renderViewLink}
            keyExtractor={item => item.id}
            ListEmptyComponent={() => {
              return (
                <View style={{ alignItems: 'center', justifyContent: 'center', width: screenWidth * 1, height: screenHeight * 0.7 }}>
                  <Image source={require('../../../Assets/images/empty.png')} style={{ width: 220, height: 144 }} />
                  <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.gray1, marginTop: 16 }}>Tidak Ada Data</Text>
                </View>
              )
            }}
          />
        </View>
      </Modalize>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  wrapSlider: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.white,
    padding: 16,
  },
  wrapDsc: {
    flex: 1,
    // height: 88,
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.container,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  txtRead: { fontFamily: 'Roboto-Regular', fontSize: 12, color: Colors.placeholder },
  cardLink: { flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: Colors.container, paddingVertical: 8 },
  txtLink: { fontFamily: 'Roboto-Regular', fontSize: 12, color: Colors.black, marginLeft: 10, },
  txt: { p: { fontFamily: 'Roboto-Regular', fontSize: 12, color: Colors.placeholder } },
  // modal
  modalstyle: { borderTopLeftRadius: 20, borderTopRightRadius: 20, zIndex: 5 },
  handlestyle: { backgroundColor: Colors.gray3, marginTop: 30, width: 40 },
  containermodal: { paddingHorizontal: 16 },
  titlemodal: { fontSize: 16, fontFamily: 'Roboto-Bold', lineHeight: 21, color: Colors.gray1, textAlign: 'center' },
});
