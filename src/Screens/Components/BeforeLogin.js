import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    Image,
} from 'react-native';
import { screenHeight, screenWidth } from '../../Variable';
import Colors from '../../Styles/Colors';
import BtnPrimary from '../Components/BtnPrimary';
import BtnOutline from '../Components/BtnOutline';

const BeforeLogin = ({ navigation }) => {
    console.log('naa', navigation);
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Image source={require('../../Assets/images/before_login.png')} style={{ width: 200, height: 150 }} />
                <View style={{ height: 32 }} />
                <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, color: Colors.black }}>Login untuk membuka semua fitur</Text>
                <View style={{ height: 20 }} />
                <View style={{ width: screenWidth * 0.8 }}>
                    <BtnPrimary
                        title={'Register'}
                        pressed={() => {navigation.navigate('Register')}}
                    />
                </View>
                <View style={{ width: screenWidth * 0.82, marginLeft: 4, marginTop: 16 }}>
                    <BtnOutline
                        title={'Login'}
                        pressed={() => {navigation.navigate('Login')}}
                    />
                </View>
            </View>
        </SafeAreaView>
    );
};

export default BeforeLogin;

const styles = StyleSheet.create({});
