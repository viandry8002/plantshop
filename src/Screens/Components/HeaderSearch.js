// import library
import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Font from '../../Styles/Fonts';
import { Badge } from '@rneui/themed';
import Octicons from 'react-native-vector-icons/Octicons';
import { screenWidth, screenHeight } from '../../Variable';
// import styles
import Colors from '../../Styles/Colors';

// main function
const HeaderSearch = ({navigation, onPress, notif }) => {
    // main return
    return (
        <View style={{ backgroundColor: Colors.primary, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 14, height: 85, paddingTop: screenHeight * 0.035 }}>
            <TouchableOpacity
                onPress={onPress}
                style={{
                    backgroundColor: Colors.white,
                    height: 35,
                    borderRadius: 100,
                    width: screenWidth - 90,
                    paddingHorizontal: 8,
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                <Ionicons
                    name="search"
                    size={18}
                    color={Colors.placeholder}
                    style={{ marginRight: 11 }}
                    onPress={() => { }}
                />
                <Text style={Font.reguler12}>Cari “tanaman hias”</Text>
            </TouchableOpacity>
            <View style={{ marginRight: 16 }}>
                <Octicons
                    name="bell-fill"
                    size={20}
                    color={Colors.white}
                    // style={{marginLeft: 16}}
                    onPress={() => navigation.navigate('Notification')}
                />
                <Badge
                    value={3}
                    containerStyle={{ position: 'absolute', top: -9, left: 10 }}
                    badgeStyle={{
                        backgroundColor: Colors.error,
                        borderColor: Colors.white,
                        borderWidth: 1,
                    }}
                />
            </View>
        </View>
    )
}

export default HeaderSearch;

// styles
const styles = StyleSheet.create({
  
});