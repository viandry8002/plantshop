// import library
import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import ButtonHeaderBack from '../ButtonHeaderBack';
// import styles
import colors from '../../../Styles/Colors';

// main function
const HeaderTitleBack = ({ title, onPressBack, typeIconLeft, iconLeft, onPressIcon, btnLeft, OnPressBtn }) => {
  // main return
  return (
    <View style={styles.header}>
      <View style={{ flexDirection: 'row', width: onPressBack === undefined ? '85%' : '70%' }}>
        {onPressBack === undefined ? null :
          <ButtonHeaderBack onPress={onPressBack} />
        }
        <Text style={styles.titleStack}>{title}</Text>
      </View>
      {iconLeft !== undefined ?
        <TouchableOpacity onPress={onPressIcon}>
          {typeIconLeft === 'FontAwesome5' ? (
            <FontAwesome5 name={iconLeft} size={24} style={{ color: colors.white, marginTop: 27 }} />
          ) : typeIconLeft === 'FontAwesome' ? (
            <FontAwesome name={iconLeft} size={24} style={{ color: colors.white, marginTop: 27 }} />
          ) : typeIconLeft === 'Ionicons' ? (
            <Ionicons name={iconLeft} size={24} style={{ color: colors.white, marginTop: 27 }} />
          ) : typeIconLeft === 'Feather' ? (
            <Feather name={iconLeft} size={24} style={{ color: colors.white, marginTop: 27 }} />
          ) : typeIconLeft === 'Entypo' ? (
            <Entypo name={iconLeft} size={24} style={{ color: colors.white, marginTop: 27 }} />
          ) : typeIconLeft === 'AntDesign' ? (
            <AntDesign name={iconLeft} size={24} style={{ color: colors.white, marginTop: 27 }} />
          ) : typeIconLeft === 'MaterialCommunityIcons' ? (
            <MaterialCommunityIcons name={iconLeft} size={24} style={{ color: colors.white, marginTop: 27 }} />
          ) : null}
        </TouchableOpacity>
        : null}
      {btnLeft !== undefined ?
        <TouchableOpacity onPress={OnPressBtn}>
          <Text style={styles.textLeft}>{btnLeft}</Text>
        </TouchableOpacity> : null}
    </View>
  )
}

export default HeaderTitleBack;

// styles
const styles = StyleSheet.create({
  // header
  header: { width: '100%', height: 83, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 16, justifyContent: 'space-between', backgroundColor: colors.primary, elevation: 5 },
  titleStack: { fontFamily: 'Roboto-Medium', color: colors.white, width: '100%', marginTop: 27, marginLeft: 20, fontSize: 16, textAlignVertical: 'center'},
  textLeft: { fontFamily: 'Roboto-Medium', color: colors.white, marginTop: 27, marginLeft: 20, fontSize: 14, textAlignVertical: 'center' },
});