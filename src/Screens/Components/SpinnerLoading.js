// import library
import React from 'react';
import { StyleSheet } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
// import styles
import colors from '../../Styles/Colors';

// main function
const SpinnerLoading = ({ spinner }) => {
  // main return
  return (
    <Spinner
      visible={spinner}
      textContent={'Harap tunggu...'}
      textStyle={styles.text}
    />

  )
}

export default SpinnerLoading;

// styles
const styles = StyleSheet.create({
  text: { color: colors.white, fontSize: 12, fontFamily: 'Roboto-Medium' },
});
