import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

const Horizontal = ({margin, coloring}) => {
  return (
    <View
      style={{
        borderBottomColor: coloring,
        borderBottomWidth: 1,
        marginVertical: margin,
      }}
    />
  );
};

export default Horizontal;

const styles = StyleSheet.create({});
