import {StyleSheet, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import Colors from '../../Styles/Colors';

const BtnPrimary = ({title, pressed, iconLeft, radius, disabled}) => {
  return (
    <TouchableOpacity
      style={[
        {
          backgroundColor: disabled == true ? Colors.gray4 : Colors.primary ,
          borderWidth: 1,
          borderColor: disabled == true ? Colors.gray4 : Colors.primary,
          height: 45,
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
        },
        radius === true ? {borderRadius: 100} : {borderRadius: 15},
      ]}
      onPress={pressed}
      disabled={disabled}>
      {iconLeft}
      <Text style={{color: Colors.white, fontWeight: '600', fontSize: 12, fontFamily: 'Roboto-Bold'}}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default BtnPrimary;

const styles = StyleSheet.create({});
