// import library
import React from 'react';
import { Image, StyleSheet, TouchableOpacity } from 'react-native';
import Icons from 'react-native-vector-icons/Feather';
// import styles
import colors from '../../../Styles/Colors';

// main function
const ButtonHeaderBack = ({ onPress }) => {
  // main return
  return (
    <TouchableOpacity onPress={onPress} style={{ marginTop: 30 }}>
      <Icons name={'arrow-left'} size={28} color={colors.white} />
    </TouchableOpacity>
  )
}

export default ButtonHeaderBack;
