import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import Colors from '../../Styles/Colors';

const BtnOutline = ({title, pressed, radius, disabled}) => {
  return (
    <TouchableOpacity
      style={[
        {
          borderWidth: 1,
          borderColor: disabled == true ? Colors.gray4 : Colors.primary,
          // paddingVertical: 12,
          height: 45,
          alignItems: 'center',
          justifyContent: 'center',
          // marginRight: 8,
        },
        radius === true ? {borderRadius: 100} : {borderRadius: 15},
      ]}
      onPress={pressed}
      disabled={disabled}>
      <Text style={{color: disabled == true ? Colors.gray4 : Colors.primary, fontWeight: '600', fontSize: 12, fontFamily: 'Roboto-Bold'}}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default BtnOutline;

const styles = StyleSheet.create({});
