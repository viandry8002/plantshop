import React from 'react';
import {StyleSheet} from 'react-native';
import Colors from './Colors';

const Font = StyleSheet.create({
  caption: {
    fontSize: 11,
    color: Colors.black,
    fontWeight: '400',
    fontFamily: 'Roboto-Regular',
  },
  bold12: {
    fontSize: 12,
    color: Colors.black,
    fontWeight: '500',
    fontFamily: 'Roboto-Medium',
  },
  reguler12: {
    fontSize: 12,
    color: Colors.black,
    fontWeight: '400',
    fontFamily: 'Roboto-Regular',
  },
  bold14: {
    fontSize: 14,
    color: Colors.black,
    fontWeight: '600',
    fontFamily: 'Roboto-Bold',
  },
  reguler14: {
    fontSize: 14,
    color: Colors.black,
    fontWeight: '400',
    fontFamily: 'Roboto-Regular',
  },
  reguler16: {
    fontSize: 16,
    color: Colors.black,
    fontWeight: '400',
    fontFamily: 'Roboto-Regular',
  },
  bold16: {
    fontSize: 16,
    color: Colors.black,
    fontWeight: '600',
    fontFamily: 'Roboto-Bold',
  },
  bold24: {
    fontSize: 24,
    color: Colors.black,
    fontWeight: '600',
    fontFamily: 'Roboto-Bold',
  },
});

export default Font;
