const Colors = {
  primary: '#006E49',
  gray: '#F4F3F9',
  gray1: '#333333',
  gray2: '#4F4F4F',
  gray3: '#828282',
  gray4: '#BDBDBD',
  gray5: '#E0E0E0',
  lightPrimary: '#EDF5F2',
  placeholder: '#79747E',
  error: '#EF3A3A',
  white: '#FFFFFF',
  black: '#000000',
  container: '#F2F2F2',
};

export default Colors;
